﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWB_Controls.BusinessObjects
{
    public class Characteristic
    {
        public int ID { get; set; }
        public string ClassificationID { get; set; }
        public string ConnectionNote { get; set; }
        public string CharacteristicUniqueID { get; set; }
        public string ObjectID { get; set; }
        public string CharacteristicValueID { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public List<CharacteristicValues> PossibleValues { get; set; }
        public FunctionLocation FunctionLoc { get; set; }
        public bool IsReadOnly { get; set; }
        public bool isModified { get; set; }
        public string ZNumChar { get; set; }
        public string ZNumDec { get; set; }
        public string DataType { get; set; }
        public int ItemNumber { get; set; }
        public bool IsNegative { get; set; }

        private CharacteristicValues _selectedCharvalue;
        public CharacteristicValues SelectedCharvalue
        {
            get
            {
                return _selectedCharvalue;
            }
            set
            {
                this.isModified = true;
                _selectedCharvalue = value;
                //Value = _selectedCharvalue.Value;                
            }
        }

        public Characteristic() { }
        public Characteristic(FunctionLocation fl)
        {
            FunctionLoc = fl;
            PossibleValues = new List<CharacteristicValues>();
        }
    }
}
