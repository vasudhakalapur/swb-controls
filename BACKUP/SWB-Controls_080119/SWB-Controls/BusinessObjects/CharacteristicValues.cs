﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWB_Controls.BusinessObjects
{
    public class CharacteristicValues
    {
        public string ID { get; set; }
        public string Value { get; set; }
    }
}
