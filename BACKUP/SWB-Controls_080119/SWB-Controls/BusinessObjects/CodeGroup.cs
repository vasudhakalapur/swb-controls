﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using SWB_Controls.Extensions;

namespace SWB_Controls.BusinessObjects
{

    public class CodeGroup
    {
        public static IEnumerable<IGrouping<string, CodeGroup>> CodesByCatalogAndCodeGrp;
        
        public static void parseCodeGroups(String xml)
        {
            try
            {
                XDocument xd = XDocument.Parse(xml);
                var codes = xd.Element("ctCodeGroup");
                CodesByCatalogAndCodeGrp = codes.Elements("cgData").Select(c => new CodeGroup()
                    {
                        ID = c.Attribute("ID").Value,
                        Code = c.Attribute("Code").Value,
                        CodeGrp = c.Attribute("CodeGrp").Value,
                        Catalog = c.Attribute("Catalog").Value,
                        Desc = c.Attribute("Desc").Value,
                        ValidFromDt = c.Attribute("ValidFromDt").Value,
                    }
                    ).GroupBy(c => String.Format("{0}-{1}", c.Catalog, c.CodeGrp)).ToList();

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Fehler aufgetreten beim parsen der Codegroups " + e);
            }
     
        }

        public string ID { get; set; }
        public string Code { get; set; }
        public string CodeGrp { get; set; }
        public string Catalog { get; set; }
        public string Desc { get; set; }
        public string ValidFromDt { get; set; }
        public MPoint MPoint { get; set; }

        internal static void initCodeGroups(AgentryClientSDK.IAgentryControlViewModelCollectionDisplay vm)
        {
            if (CodesByCatalogAndCodeGrp == null && vm != null && vm.Any())
            {
                var collection = vm.First().Ancestor.CollectionByName("ZCodeGroups");
                if (collection == null || collection.DescendantCount < 1) return;

                var xml = collection.Descendant(0).PropertyValueByName("CodeGroupXMLContent");

                if (string.IsNullOrWhiteSpace(xml)) return;

                parseCodeGroups(xml);
            }
        }

        public bool IsSelected
        {
            get
            {
                return MPoint.selectedCode == this;
            }
            set
            {
                if (value)
                {
                    MPoint.selectedCode = this;
                }
            }
        }

        internal CodeGroup CopyForMPoint(MPoint mPoint)
        {
            return new CodeGroup()
            {
                ID = this.ID,
                Code = this.Code,
                CodeGrp = this.CodeGrp,
                Catalog = this.Catalog,
                Desc = this.Desc,
                ValidFromDt = this.ValidFromDt,
                MPoint = mPoint
            };
        }
    }
}
