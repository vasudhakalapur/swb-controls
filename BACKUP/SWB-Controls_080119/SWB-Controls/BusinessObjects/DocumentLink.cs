﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWB_Controls.BusinessObjects
{
    public class DocumentLink
    {
        public int Index { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
        public string RefObject { get; set; }
        public bool IsLocal { get; set; }
    }
}
