﻿using SWB_Controls.ListControls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWB_Controls.BusinessObjects
{
    public class NotificationDateComparer_Ascending : IComparer
    {
        public int Compare(object x, object y)
        {
            Notification x1 = x as Notification;
            Notification y1 = y as Notification;

            return x1.StartAsDate.CompareTo(y1.StartAsDate);
        }
    }
    public class NotificationDateComparer_Descending : IComparer
    {
        public int Compare(object x, object y)
        {
            Notification x1 = x as Notification;
            Notification y1 = y as Notification;

            return y1.StartAsDate.CompareTo(x1.StartAsDate);
        }
    }
    public class NotificationComparer : IComparer
    {
        public int Compare(object x, object y)
        {
            Notification x1 = x as Notification;
            Notification y1 = y as Notification;         
           
            //KAPA order by Prio
            if (string.IsNullOrWhiteSpace(x1.Priority) && !string.IsNullOrWhiteSpace(y1.Priority))
            {
                return 1;
            }
            if (string.IsNullOrWhiteSpace(y1.Priority) && !string.IsNullOrWhiteSpace(x1.Priority))
            {
                return -1;
            }
            if (!string.IsNullOrWhiteSpace(x1.Priority) && !string.IsNullOrWhiteSpace(y1.Priority))
            {
                return x1.Priority.CompareTo(y1.Priority);
            }

            //Rest order by Workorder-No
            return x1.WorkOrderNumber.CompareTo(y1.WorkOrderNumber);
        }
    }
    public class Notification
    {
        //Data Fields
        public int Index { get; set; }
        public string WorkOrderNumber { get; set; }
        public string NotificationNumber { get; set; }
        public string Priority { get; set; }
        public DateTime StartAsDate { get; set; }
        public string CreatedDate { get { return StartAsDate != DateTime.MinValue ? StartAsDate.ToString("dd.MM.yyyy") : String.Empty; } }
        public string Description { get; set; }
        public string DescriptionLong { get; set; }
        public string Techobj { get; set; }
        public string TechobjLong { get; set; }
        public string NotifLocal { get; set; }
        public bool NotifIsClosed { get; set; }
        private NotificationListControl OpenUIControl;
        private NotificationHistoryListControl notificationHistoryListControl;

        public AgentryClientSDK.IAgentryControlViewModelCollectionDisplay context { get; set; }

        public Notification(NotificationListControl list)
        {
            OpenUIControl = list;
        }

        public Notification(NotificationHistoryListControl notificationHistoryListControl)
        {
            // TODO: Complete member initialization
            this.notificationHistoryListControl = notificationHistoryListControl;
        }
        public Notification()
        {

        }
    }
}
