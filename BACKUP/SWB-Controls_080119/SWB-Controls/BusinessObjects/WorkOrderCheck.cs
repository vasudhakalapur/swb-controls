﻿﻿using AgentryClientSDK;
using SWB_Controls.ListControls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using SWB_Controls.Extensions;

namespace SWB_Controls.BusinessObjects
{
    public class AlphanumComparator : IComparer
    {
        private enum ChunkType { Alphanumeric, Numeric };
        private bool InChunk(char ch, char otherCh)
        {
            ChunkType type = ChunkType.Alphanumeric;

            if (char.IsDigit(otherCh))
            {
                type = ChunkType.Numeric;
            }

            if((type == ChunkType.Alphanumeric && char.IsDigit(ch)) || (type == ChunkType.Numeric && !char.IsDigit(ch)))
            {
                return false;
            }
            return true;
        }
        public AlphanumComparator() { }

        public int Compare(object x, object y)
        {
            String s1 = x as string;
            String s2 = y as string;
            if (s1 == null || s2 == null) { return 0; }

            int thisMarker = 0, thisNumericChunk = 0;
            int thatMarker = 0, thatNumericChunk = 0;

            while ((thisMarker < s1.Length) && (thatMarker < s2.Length))
            {
                if (thisMarker >= s1.Length) { return -1; }
                else if (thatMarker >= s2.Length) { return 1; }

                char thisCh = s1[thisMarker];
                char thatCh = s2[thatMarker];

                StringBuilder thisChunk = new StringBuilder();
                StringBuilder thatChunk = new StringBuilder();
                while ((thisMarker < s1.Length) && (thisChunk.Length == 0 || InChunk(thisCh, thisChunk[0])))
                {
                    thisChunk.Append(thisCh);
                    thisMarker++;
                    if (thisMarker < s1.Length) { thisCh = s1[thisMarker]; }
                }
                while ((thatMarker < s2.Length) && (thatChunk.Length == 0 || InChunk(thatCh, thatChunk[0])))
                {
                    thatChunk.Append(thatCh);
                    thatMarker++;
                    if (thatMarker < s2.Length) { thatCh = s2[thatMarker]; }
                }

                int result = 0;
                //If both chunks contain numeric characters, sort them numerically
                if (char.IsDigit(thisChunk[0]) && char.IsDigit(thatChunk[0]))
                {
                    thisNumericChunk = Convert.ToInt32(thisChunk.ToString());
                    thatNumericChunk = Convert.ToInt32(thatChunk.ToString());

                    if (thisNumericChunk < thatNumericChunk) { result = -1; }
                    if (thisNumericChunk > thatNumericChunk) { result = 1; }
                }
                else if (char.IsDigit(thisChunk[0]) && !char.IsDigit(thatChunk[0]))
                {
                    return 1; //Ensure the non-numeric sorts before numeric
                }
                else if (!char.IsDigit(thisChunk[0]) && char.IsDigit(thatChunk[0]))
                {
                    return -1; //Ensure the non-numeric sorts before numeric
                }
                else
                {
                    result = thisChunk.ToString().CompareTo(thatChunk.ToString());
                }

                if (result != 0) { return result; }
            }
            return 0;
        }
    }
    public class WorkOrderCheckDateComparer_Ascending : IComparer
    {
        public int Compare(object x, object y)
        {
            WorkOrderCheck x1 = x as WorkOrderCheck;
            WorkOrderCheck y1 = y as WorkOrderCheck;

            return x1.StartAsDate.CompareTo(y1.StartAsDate);
        }
    }
    public class WorkOrderCheckDateComparer_Descending : IComparer
    {
        public int Compare(object x, object y)
        {
            WorkOrderCheck x1 = x as WorkOrderCheck;
            WorkOrderCheck y1 = y as WorkOrderCheck;

            return y1.StartAsDate.CompareTo(x1.StartAsDate);
        }
    }
    public class WorkOrderCheck : INotifyPropertyChanged
    {
        // Data Fields
        public IAgentryData agentrydata { get; set; }
        public int AddressID { get; set; }
        public int Index { get; set; }
        public string Wonr { get; set; }
        public string Status { get; set; }
        public DateTime StartAsDate { get; set; }
        public string Start { get { return StartAsDate != DateTime.MinValue && StartAsDate != DateTime.MaxValue ? StartAsDate.ToString("dd.MM HH:mm") : String.Empty; } }
        public string Description { get; set; }
        public string DescriptionLong { get; set; }
        public string Techobj { get; set; }
        public string TechobjLong { get; set; }
        public string ActiveOperationNum { get; set; }
        public bool isKapaWO { get; set; }
        public string PlantSection { get; set; }
        public string OrderType { get; set; }
        public string HouseNumber { get; set; }
        public string Street { get; set; }
        private WorkOrderCheckListControl OpenUIControl;

        public AgentryClientSDK.IAgentryControlViewModelCollectionDisplay context { get; set; }

        public WorkOrderCheck(WorkOrderCheckListControl list)
        {
            OpenUIControl = list;
        }
        public WorkOrderCheck()
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void firePropertChangedEvent(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(property));
            }
        }


        internal void refreshStatus()
        {
            var status = agentrydata.PropertyValueByName("ZStatusFlow");
            if (string.IsNullOrEmpty(status))
            {
                status = "RCVD";
            }
            Status = status;
            firePropertChangedEvent("Status");
        }
    }
}
