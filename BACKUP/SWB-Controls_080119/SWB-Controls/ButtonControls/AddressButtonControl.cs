﻿using SWB_Controls.BusinessObjects;
using SWB_Controls.TextBoxControls;
using SWB_Controls.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace SWB_Controls.ButtonControls
{
    public class AddressButtonControl: BaseAgentryCustomControl
    {
        public AgentryClientSDK.IAgentryControlViewModelStringDisplay context { get; set; }        
        
        public List<Address> Addresses {get; set;}
        public static Popup popupControl { get; set; }
        public static bool? IsAddressSelected { get; set; }

        static AddressButtonControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(AddressButtonControl), new FrameworkPropertyMetadata(typeof(AddressButtonControl)));
            PureVirtualHandler.RegisterHandler();
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            var btn = GetTemplateChild("PART_Button") as Button;
            btn.Click += btn_Click;
            var btnAbort = GetTemplateChild("PART_btnAbort") as Button;
            btnAbort.Click += btnAbort_Click;
            Addresses = new List<Address>();
            IsAddressSelected = false;

            popupControl = GetTemplateChild("PART_Popup") as Popup;
            if (ConfigurationHelper.GetConfigSettings["NotificationAddress"] != null)
            {
                if (ConfigurationHelper.GetConfigSettings["NotificationAddress"].Value.IndexOf(";") > 0)
                {
                    foreach (var str in ConfigurationHelper.GetConfigSettings["NotificationAddress"].Value.Split(';'))
                    {
                        Addresses.Add(new Address { Description = str.Replace(",", " ").Replace("|", ", ").Trim() });
                    }
                }
                else
                {
                    Addresses.Add(new Address { Description = ConfigurationHelper.GetConfigSettings["NotificationAddress"].Value.Replace(",", " ").Replace("|", ", ").Trim() });
                }
            }
           
        }

        private void btnAbort_Click(object sender, RoutedEventArgs e)
        {
            popupControl.IsOpen = false;
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            var itemsControl = GetTemplateChild("PART_ItemsControl") as ItemsControl;
            itemsControl.ItemsSource = (Addresses.Count > 5) ? Addresses.Skip(Addresses.Count - 5).Take(5).ToList() : Addresses;
            popupControl.IsOpen = true;
        }
        public ICommand ClickCommand
        {
            get
            {
                return new CommandHandler(clickHyperlink, true);
            }
        }
        private void clickHyperlink(object target)
        {
            var arrStr = target.ToString().Split(',');
            if (arrStr[0].Length > 0)
            {
                CityTextControl.txtControl.Text = arrStr[0].Trim();
                CityTextControl.context.StringValue = arrStr[0].Trim();
            }
            if (arrStr[1].Length > 0)
            {
                StreetTextControl.txtControl.Text = arrStr[1].Trim();
                StreetTextControl.context.StringValue = arrStr[1].Trim();
            }
            if (arrStr[2].Length > 0)
            {
                HouseNumTextControl.txtControl.Text = arrStr[2].Trim();
                HouseNumTextControl.context.StringValue = arrStr[2].Trim();
            }
            IsAddressSelected = true;
            popupControl.IsOpen = false;
        }

    }
}
