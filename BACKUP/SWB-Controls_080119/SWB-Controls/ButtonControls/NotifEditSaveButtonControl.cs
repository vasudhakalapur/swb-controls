﻿using SWB_Controls.Utils;
using SWB_Controls.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using SWB_Controls.TextBoxControls;

namespace SWB_Controls.ButtonControls
{
    public class NotifEditSaveButtonControl : BaseAgentryCustomControl
    {
        public AgentryClientSDK.IAgentryControlViewModelStringDisplay context { get; set; }
        static NotifEditSaveButtonControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(NotifEditSaveButtonControl), new FrameworkPropertyMetadata(typeof(NotifEditSaveButtonControl)));
            PureVirtualHandler.RegisterHandler();
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            var btn = GetTemplateChild("PART_Button") as Button;
            btn.Click += btn_Click;
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            context = DataContext as AgentryClientSDK.IAgentryControlViewModelStringDisplay;            
            var key = "NotificationAddress";
            var address = string.Empty;
            if ((CityTextControl.CityTextControlValue != null && CityTextControl.CityTextControlValue.Length > 0) || (StreetTextControl.StreetTextControlValue != null && StreetTextControl.StreetTextControlValue.Length > 0) || (HouseNumTextControl.HouseNumTextControlValue != null && HouseNumTextControl.HouseNumTextControlValue.Length > 0))
            {
                address = String.Format("{0}|{1}|{2}", CityTextControl.CityTextControlValue.Replace("|", " ").Replace(";", " "), StreetTextControl.StreetTextControlValue.Replace("|", " ").Replace(";", " "), HouseNumTextControl.HouseNumTextControlValue.Replace("|", " ").Replace(";", " "));
            }
            if (address.Length > 0)
            {
                var config = ConfigurationHelper.GetConfig;
                var settings = config.AppSettings.Settings;
                if (settings[key] == null)
                {
                    settings.Add(key, address);
                }
                else
                {
                    if (!settings[key].Value.Contains(address))
                        settings[key].Value = String.Format("{0};{1}", settings[key].Value, address);
                }
                ConfigurationHelper.SaveConfig(config);
            }
            context.ExecuteAgentryAction("ZNotificationEdit");
        }
    }
}
