﻿using SWB_Controls.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace SWB_Controls.DatePickerControls
{
    public class DatePickerControl : BaseAgentryCustomControl
    {
        public AgentryClientSDK.IAgentryControlViewModelStringDisplay context { get; set; }

        static DatePickerControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(DatePickerControl), new FrameworkPropertyMetadata(typeof(DatePickerControl)));           
            PureVirtualHandler.RegisterHandler();
        }
        public void SetAgentryValue()
        {
            context = DataContext as AgentryClientSDK.IAgentryControlViewModelStringDisplay;
            var datePicker = GetTemplateChild("PART_Date") as DatePicker;
            //datePicker.Loaded -= datePicker_Loaded;
            //datePicker.Loaded += datePicker_Loaded;

            //Hide the future dates
            datePicker.DisplayDateEnd = DateTime.Today;
            var createdDate = context.GetAgentryString("ZCreatedDate");
           if (!string.IsNullOrEmpty(createdDate))
            {
                datePicker.Text = createdDate;
            }
            if (datePicker.SelectedDate != null)
                context.StringValue = datePicker.SelectedDate.Value.ToString("MM/dd/yyyy");

            datePicker.SelectedDateChanged += datePicker_SelectedDateChanged;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("de-DE");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("de-DE");
        }

        private void datePicker_Loaded(object sender, RoutedEventArgs e)
        {
            var dp = sender as DatePicker;
            if (dp == null) return;

            var tb = DataGridHelper.GetVisualChild<DatePickerTextBox>(dp);
            if (tb == null) return;

            var wm = tb.Template.FindName("PART_Watermark", tb) as ContentControl;
            if (wm == null) return;
            wm.Content = "Bitte Datum auswählen";
        }

        private void datePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            var datePicker = sender as DatePicker;
            DateTime? date = datePicker.SelectedDate;
            context.StringValue = (date != null) ? date.Value.ToString("MM/dd/yyyy") : "";
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            SetAgentryValue();
        }
    }
}
