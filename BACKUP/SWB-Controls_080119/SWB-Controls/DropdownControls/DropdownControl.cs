﻿using AgentryClientSDK;
using SWB_Controls.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SWB_Controls.DropdownControls
{
    public class DropdownControl : BaseAgentryCustomControl
    {
        public IAgentryControlViewModelCollectionDisplay context { get; set; }
        static DropdownControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(DropdownControl), new FrameworkPropertyMetadata(typeof(DropdownControl)));
            PureVirtualHandler.RegisterHandler();
        }
        public void SetAgentryValue()
        {
            context = DataContext as IAgentryControlViewModelCollectionDisplay;
            ComboBox cmb = GetTemplateChild("PART_Combo") as ComboBox;


            cmb.ItemsSource = context.ToList();//DropDown.GetItemSourceForRange(0, 13, 0);
            cmb.SelectedIndex = 0;
            cmb.SelectionChanged += cmb_SelectionChanged;
        }
        private void cmb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var cmb = sender as ComboBox;
            //context.SelectedItem = (IAgentryData) cmb.SelectedItem;
        }
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            SetAgentryValue();
        }
    }
}
