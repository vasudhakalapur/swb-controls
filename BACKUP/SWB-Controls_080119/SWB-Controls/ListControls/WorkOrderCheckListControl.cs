﻿using SWB_Controls.BusinessObjects;
using SWB_Controls.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Timers;
using AgentryClientSDK;
using System.Windows.Markup;
using SWB_Controls.Utils;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Threading;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Collections;
using System.Windows.Media;
using System.Windows.Controls.Primitives;

namespace SWB_Controls.ListControls
{
    public class WorkOrderCheckListControl : BaseAgentryCustomControl
    {
        static int MAX_WO_DESCRIPTION_LENGTH = 34;
        static int MAX_OP_DESCRIPTION_LENGTH = 32;

        static string selectedWONR = "";
        static Dictionary<string, ListSortDirection> dictSort = new Dictionary<string, ListSortDirection>();
        public static AgentryClientSDK.IAgentryData woContext;
        static WorkOrderCheckListControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(WorkOrderCheckListControl), new FrameworkPropertyMetadata(typeof(WorkOrderCheckListControl)));
            PureVirtualHandler.RegisterHandler();
        }
        public bool m_Loaded = false;
        public WorkOrderCheck currentWO;
        public CollectionViewSource cvs;
        private System.Timers.Timer refreshTimer;
        private String lastRefreshValue;
        public string _filter;
        public string Filter
        {
            get
            {
                return _filter;
            }
            set
            {
                _filter = value;
                cvs.View.Refresh();
                //_cview.FilterText = _filter;
                // UpdateCurrentPage();
                //lblTotalCount.Content = String.Format("Of {0}", _cview.PageCount);
            }
        }

        private bool ignoreSelectionChanges = false; //this will be set to true during intialization of grid to  avoid saving wrong selection.
        private WorkOrderCheck _selectedItem;
        public WorkOrderCheck SelectedWO
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                _selectedItem = value;

                if (ignoreSelectionChanges) return;

                if (_selectedItem != null)
                {
                    ((AgentryClientSDK.IAgentryControlViewModelCollectionDisplay)DataContext).SelectItem(value.Index);
                    woContext = (AgentryClientSDK.IAgentryData)(_selectedItem.context.SingleOrDefault(x => x.PropertyValueByName("WONum") + x.PropertyValueByName("ZActiveOperationNum") == selectedWONR));
                    selectedWONR = _selectedItem.Wonr + _selectedItem.ActiveOperationNum;
                    System.Diagnostics.Debug.WriteLine("storing selectedWONR to" + selectedWONR);
                }
            }
        }
        //private PagingCollectionView _cview;
        //private TextBox txtCurrentPage;
        //private Label lblTotalCount;
        private ObservableCollection<WorkOrderCheck> workorders;
        private EnhancedDataGrid grid;

        //public Visibility ShowBtnPrevious
        //{
        //    get { return _cview.ShowPrevButton; }
        //}

        //public Visibility ShowBtnNext
        //{
        //    get { return _cview.ShowNextButton; }
        //}

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            //Button btnPrevious = GetTemplateChild("PART_BtnPrevious") as Button;
            //btnPrevious.Click += OnPreviousClicked;


            //Button btnNext = GetTemplateChild("PART_BtnNext") as Button;
            //btnNext.Click += OnNextClicked;

            grid = GetTemplateChild("PART_GridWOCheckList") as EnhancedDataGrid;
            if (grid != null)
            {
                grid.Loaded += grid_Loaded;        
                grid.Sorting += grid_Sorting;
                grid.Sorted += grid_Sorted;
                grid.PreviewKeyDown += grid_PreviewKeyDown;
                grid.DataContext = this;

                Style rowStyle = (Style) grid.Resources["rowStyle"];
                rowStyle.Setters.Add(new EventSetter(DataGridRow.MouseDoubleClickEvent, new MouseButtonEventHandler(grid_MouseDoubleClick)));
                grid.RowStyle = rowStyle;
            }

            FrameworkElement filter = GetTemplateChild("PART_Filter") as DockPanel;
            if (filter != null)
            {
                filter.DataContext = this;
            }

            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("de-DE");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("de-DE");

            //txtCurrentPage = GetTemplateChild("PART_TxtCurrentPage") as TextBox;
            //lblTotalCount = GetTemplateChild("PART_LblTotalCount") as Label;
        }
        void grid_Sorting(object sender, DataGridSortingEventArgs e)
        {
            DataGridColumn column = e.Column;

            if (column.SortMemberPath == "StartAsDate")
            {
                e.Handled = true;
                var direction = (column.SortDirection != ListSortDirection.Ascending) ? ListSortDirection.Ascending : ListSortDirection.Descending;
                column.SortDirection = direction;
                foreach (var item in (ObservableCollection<WorkOrderCheck>)cvs.Source)
                {
                    if (direction == ListSortDirection.Ascending)
                    {
                        if (item.StartAsDate == DateTime.MinValue)
                            item.StartAsDate = DateTime.MaxValue;
                    }
                    else
                    {
                        if (item.StartAsDate == DateTime.MaxValue)
                            item.StartAsDate = DateTime.MinValue;
                    }
                }
                var lcv = cvs.View as ListCollectionView;
                IComparer comparer = (direction == ListSortDirection.Ascending) ? (IComparer)new WorkOrderCheckDateComparer_Ascending() : (IComparer)new WorkOrderCheckDateComparer_Descending();
                lcv.CustomSort = comparer;
            }

        }

        private void grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //DependencyObject source = VisualTreeHelper.GetParent((DependencyObject)e.OriginalSource);
            //if (source.GetType() == typeof(ContentPresenter))
            //{
                 SelectNavigation();
            //}
        }
        private void grid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                SelectNavigation();
            }
        }
        private void SelectNavigation()
        {
            MessageBoxResult result = MessageBox.Show(String.Format("Wurde die Überprüfung {0} {1} durchgeführt?", ((WorkOrderCheck)grid.SelectedItem).Street, ((WorkOrderCheck)grid.SelectedItem).HouseNumber), "Prüfung", MessageBoxButton.YesNoCancel, MessageBoxImage.Information);
            if (result == MessageBoxResult.Yes)
            {
                ((AgentryClientSDK.IAgentryControlViewModelCollectionDisplay)DataContext).ExecuteAgentryAction("ZNavigateToMeasuringPointsScreen");
            }
            else if (result == MessageBoxResult.No)
            {
                ((AgentryClientSDK.IAgentryControlViewModelCollectionDisplay)DataContext).ExecuteAgentryAction("ZNavigateToTimeRecordingScreen");
            }
        }
        //private void OnPreviousClicked(object sender, RoutedEventArgs e)
        //{
        //    _cview.MoveToPreviousPage();
        //    UpdateCurrentPage();
        //}

        //private void OnNextClicked(object sender, RoutedEventArgs e)
        //{
        //    _cview.MoveToNextPage();
        //    UpdateCurrentPage();
        //}

        //private void UpdateCurrentPage()
        //{
        //    txtCurrentPage.Text = _cview.CurrentPage.ToString();
        //    firePropertyChanged("ShowPrevButton");
        //}       

        private void grid_Sorted(object sender, ValueEventArgs<DataGridColumn> e)
        {
            var col = (DataGridColumn)e.Value;
            dictSort.Clear();
            dictSort.Add(col.SortMemberPath, (ListSortDirection)col.SortDirection);
        }

        private void applySortDescriptions(DataGrid dataGrid)
        {
            dataGrid.Items.SortDescriptions.Clear();
            foreach (DataGridColumn c in dataGrid.Columns)
            {
                if (dictSort.ContainsKey(c.SortMemberPath))
                {
                    dataGrid.Items.SortDescriptions.Add(new SortDescription(c.SortMemberPath, (ListSortDirection)dictSort[c.SortMemberPath]));
                    c.SortDirection = (ListSortDirection)dictSort[c.SortMemberPath];
                }
                else
                {
                    c.SortDirection = null;
                }
            }
            dataGrid.Items.Refresh();
        }

        private void grid_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                using (new WaitCursor())
                {
                    System.Diagnostics.Debug.WriteLine("HIS Grid loaded triggered");
                    ignoreSelectionChanges = true;
                    workorders = new ObservableCollection<WorkOrderCheck>();
                    AgentryClientSDK.IAgentryControlViewModelCollectionDisplay vm = DataContext as AgentryClientSDK.IAgentryControlViewModelCollectionDisplay;

                    vm.PropertyChanged += dc_PropertyChanged;
                    string counter = ((AgentryClientSDK.IAgentryControlViewModelCollectionDisplay)DataContext).Count() > 0 ? ((AgentryClientSDK.IAgentryControlViewModelCollectionDisplay)DataContext).First().Ancestor.PropertyValueByName("ZOpenUIRefreshRequiredForWorkOrderByCheck") : string.Empty;
                    lastRefreshValue = counter;
                    int index = 0;

                    CodeGroup.initCodeGroups(vm);
                    foreach (AgentryClientSDK.IAgentryData item in vm)
                    {
                        var wo =
                        new WorkOrderCheck
                        {
                            context = vm,
                            agentrydata = item,
                            Wonr = item.PropertyValueByName("WONum"),
                            isKapaWO = item.PropertyValueByName("ZOperationUserStatus") == "KAPA",
                            ActiveOperationNum = item.PropertyValueByName("ZActiveOperationNum"),
                            PlantSection = item.PropertyValueByName("ZPlantSection"),
                            Techobj = "",
                            TechobjLong = "",
                            Index = index++
                        };

                        //Set Status based on created Labor entries
                        wo.Status = item.PropertyValueByName("ZStatusFlow");
                        if (string.IsNullOrEmpty(wo.Status))
                        {
                            wo.Status = "RCVD";
                        }

                        var activeOperation = findActiveOperation(item, wo);
                        var funcLoc = findFuncLoc(item, item.PropertyValueByName("FuncLoc"));
                        if (funcLoc != null)
                        {
                            var equipLoc = findFuncLoc(item, item.PropertyValueByName("EquipmentID"));
                            wo.Techobj = BuildTechObj(item, funcLoc, wo, equipLoc);
                        }
                        else
                        {
                            funcLoc = findFuncLoc(item, item.PropertyValueByName("EquipmentID"));
                            if (funcLoc != null)
                            {
                                wo.Techobj = BuildTechObj(item, funcLoc, wo);
                            }
                        }

                        if (activeOperation != null && !wo.isKapaWO)
                        {
                            //Add Start-Date only for Workorders that are not KAPA (ie FIX)
                            wo.StartAsDate = activeOperation.PropertyValueAsDateTimeByName("ZFixAppointment");
                        }

                        wo.Description = AssembleDescription(item, activeOperation, wo);

                        workorders.Add(wo);
                    }

                    grid = GetTemplateChild("PART_GridWOCheckList") as EnhancedDataGrid;

                    var count = 0;
                    var comp = new AlphanumComparator();
                    workorders.Sort((lhs, rhs) => comp.Compare(lhs.TechobjLong, rhs.TechobjLong));
                    var source = workorders;
                    foreach (var item in source)
                    {
                        if (item.StartAsDate == DateTime.MinValue)
                            item.StartAsDate = DateTime.MaxValue;
                        item.AddressID = count;
                        count++;
                    }
                    cvs = new CollectionViewSource();
                    cvs.Source = source;
                    cvs.Filter += cvs_Filter;

                    cvs.SortDescriptions.Add(new SortDescription("StartAsDate", ListSortDirection.Descending));

                    ListCollectionView lcw = cvs.View as ListCollectionView;
                    lcw.CustomSort = new WorkOrderCheckDateComparer_Ascending();

                    grid.ItemsSource = cvs.View;

                    //UpdateCurrentPage();
                    //lblTotalCount.Content = String.Format("Of {0}", _cview.PageCount);
                    var lblCount = GetTemplateChild("PART_LblCount") as Label;
                    if (lblCount != null)
                    {
                        lblCount.Content = String.Format("{0} Aufträgen", grid.Items.Count);
                    }

                    if (dictSort.Keys.Count() > 0)
                    {
                        applySortDescriptions(grid);
                    }

                    if (selectedWONR != "")
                    {
                        var item = workorders.SingleOrDefault(x => x.Wonr + x.ActiveOperationNum == selectedWONR);
                        if (item != null)
                        {
                            System.Diagnostics.Debug.WriteLine("Restore of selectedWONR from" + selectedWONR);
                            grid.SelectedItem = item;
                        }
                        else
                        {
                            System.Diagnostics.Debug.WriteLine("Could not find selectedWONR from" + selectedWONR + "selecting first row instead");
                            grid.SelectedIndex = 0;
                        }
                    }
                    else
                    {
                        grid.SelectedIndex = 0;
                        System.Diagnostics.Debug.WriteLine("No selectedWONR stored - selecting first row instead");
                    }

                    ignoreSelectionChanges = false;
                    if (grid.Items.Count > 0)
                    {
                        SelectedWO = (WorkOrderCheck)grid.SelectedItem;
                        grid.UpdateLayout();
                        grid.ScrollIntoView(grid.SelectedIndex);
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Grid Load Exception : " + ex.Message);
            }
        }

        private void cvs_Filter(object sender, FilterEventArgs e)
        {
            WorkOrderCheck t = e.Item as WorkOrderCheck;
            if (t != null)
            // If filter is turned on, filter completed items.
            {
                if (string.IsNullOrWhiteSpace(_filter)) { e.Accepted = true; return; }

                string fs = _filter.Trim().ToLower();

                if (t.Description.ToLower().Contains(fs) ||
                    t.Start.ToString().ToLower().Contains(fs) ||
                    t.Wonr.ToLower().Contains(fs) ||
                    t.Techobj.ToLower().Contains(fs) ||
                    t.Status.ToLower().Contains(fs)
                    )
                    e.Accepted = true;
                else
                    e.Accepted = false;
            }
        }

        void dc_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            string counter = ((AgentryClientSDK.IAgentryControlViewModelCollectionDisplay)DataContext).First().Ancestor.PropertyValueByName("ZOpenUIRefreshRequiredForWorkOrderByCheck");
             if (lastRefreshValue != counter)
             {
                 lastRefreshValue = counter;
                 if (refreshTimer == null)
                 {
                     refreshTimer = new System.Timers.Timer(1000);
                     refreshTimer.Elapsed += t_Elapsed;

                     refreshTimer.Start();
                 }
             }
             else if (grid.SelectedItem != null)
             {
                 //Refresh this item.
                 WorkOrderCheck orderCheck = grid.SelectedItem as WorkOrderCheck;
                 orderCheck.refreshStatus();
             }
        }
        void t_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                refreshTimer.Enabled = false;
                refreshTimer.Stop();
                refreshTimer = null;
                IAgentryControlViewModelCollectionDisplay dc = null;
                this.Dispatcher.Invoke(new Action(() => dc = this.DataContext as IAgentryControlViewModelCollectionDisplay));
                this.Dispatcher.Invoke(new Action(() => { grid_Loaded(null, new RoutedEventArgs()); }));
            }
            catch
            {
                //TODO 
                //Do nothing for now.
            }
        }
        private string BuildTechObj(AgentryClientSDK.IAgentryData wo, AgentryClientSDK.IAgentryData funcLoc, WorkOrderCheck woObject, AgentryClientSDK.IAgentryData equipLoc)
        {
            string Street = funcLoc.PropertyValueByName("ZStreet");
            string HouseNo = funcLoc.PropertyValueByName("ZHouseNum");
            string PostCode = funcLoc.PropertyValueByName("ZPostCode");
            string City = funcLoc.PropertyValueByName("ZCity");
            woObject.HouseNumber = HouseNo;
            woObject.Street = Street;

            if (Street.Length == 0 && HouseNo.Length == 0 && equipLoc != null)
                return BuildTechObj(wo, equipLoc, woObject);
            else
            {
                string shortVersion = TechnicalObject.buildShortAddressDesc(PostCode, City, Street);
                woObject.TechobjLong = TechnicalObject.buildAddressDesc(PostCode, City, Street, HouseNo);
                return shortVersion;
            }

        }

        private string BuildTechObj(AgentryClientSDK.IAgentryData wo, AgentryClientSDK.IAgentryData funcLoc, WorkOrderCheck woObject)
        {
            string Street = funcLoc.PropertyValueByName("ZStreet");
            string HouseNo = funcLoc.PropertyValueByName("ZHouseNum");
            string PostCode = funcLoc.PropertyValueByName("ZPostCode");
            string City = funcLoc.PropertyValueByName("ZCity");
            woObject.HouseNumber = HouseNo;
            woObject.Street = Street;

            string shortVersion = TechnicalObject.buildShortAddressDesc(PostCode, City, Street);
            woObject.TechobjLong = TechnicalObject.buildAddressDesc(PostCode, City, Street, HouseNo);
            return shortVersion;

        }

        private AgentryClientSDK.IAgentryData findFuncLoc(AgentryClientSDK.IAgentryData item, string FuncLoc)
        {
            for (int i = 0; i < item.DescendantCount; i++)
            {
                var descendant = item.Descendant(i);
                if (descendant.InternalName == "ObjectList")
                {
                    if (descendant.DescendantCount > 0)

                        for (int j = 0; j < descendant.DescendantCount; j++)
                        {
                            var funcLoc = descendant.Descendant(j);
                            if (funcLoc.PropertyValueByName("ZTechnicalObject") == FuncLoc)
                            {
                                return funcLoc;
                            }
                        }
                }
            }
            return null;
        }

        private string AssembleDescription(AgentryClientSDK.IAgentryData wo, AgentryClientSDK.IAgentryData activeOperation, WorkOrderCheck woObject)
        {
            string WODescription = wo.PropertyValueByName("Description");
            string OpDescription = activeOperation != null ? activeOperation.PropertyValueByName("Description") : "";
            string Separator = " | ";
            woObject.DescriptionLong = WODescription + Separator + OpDescription;

            //Case 1 : Both strings longer than MAX:
            if (WODescription.Length > MAX_WO_DESCRIPTION_LENGTH && OpDescription.Length > MAX_OP_DESCRIPTION_LENGTH)
            {
                return WODescription.Substring(0, MAX_WO_DESCRIPTION_LENGTH) + Separator + OpDescription.Substring(0, MAX_OP_DESCRIPTION_LENGTH);
            }
            //Case 2: WO string longer than MAX:
            if (WODescription.Length > MAX_WO_DESCRIPTION_LENGTH)
            {
                int maxLength = MAX_WO_DESCRIPTION_LENGTH + MAX_OP_DESCRIPTION_LENGTH - OpDescription.Length;
                int usedLength = maxLength > WODescription.Length ? WODescription.Length : maxLength;
                return WODescription.Substring(0, usedLength) + Separator + OpDescription;
            }
            //Case 3: Op description longer than MAX:
            if (OpDescription.Length > MAX_OP_DESCRIPTION_LENGTH)
            {
                int maxLength = MAX_OP_DESCRIPTION_LENGTH + MAX_WO_DESCRIPTION_LENGTH - WODescription.Length;
                int usedLength = maxLength > OpDescription.Length ? OpDescription.Length : maxLength;
                return WODescription + Separator + OpDescription.Substring(0, usedLength);
            }
            //Case 4: Both not longer than MAX:
            return OpDescription;

        }

        private AgentryClientSDK.IAgentryData findActiveOperation(AgentryClientSDK.IAgentryData item, WorkOrderCheck wo)
        {
            for (int i = 0; i < item.DescendantCount; i++)
            {
                var descendant = item.Descendant(i);
                if (descendant.InternalName == "Operations")
                {
                    for (int j = 0; j < descendant.DescendantCount; j++)
                    {
                        var desc2 = descendant.Descendant(j);
                        if (desc2.PropertyValueByName("OperationNum") == wo.ActiveOperationNum) //Found Operation that is the current one.
                        {
                            return desc2;
                        }
                    }
                }
            }
            return null;
        }
    }
}
