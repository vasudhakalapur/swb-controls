﻿using SWB_Controls.BusinessObjects;
using SWB_Controls.Extensions;
using SWB_Controls.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;

namespace SWB_Controls.ReportControls
{
    public class CharacteristicReportControl : BaseAgentryCustomControl
    {
        static CharacteristicReportControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CharacteristicReportControl), new FrameworkPropertyMetadata(typeof(CharacteristicReportControl)));
            injectDictionary();
            PureVirtualHandler.RegisterHandler();
        }

        private static void injectDictionary()
        {
            var ResName = "SWB-Controls;component/Resources/Theme.xaml";
            var uri = new Uri(ResName, UriKind.Relative);
            var streamResourceInfo = Application.GetResourceStream(uri);

            using (var resStream = streamResourceInfo.Stream)
            {
                ResourceDictionary myResDic = (ResourceDictionary)XamlReader.Load(resStream);
                Application.Current.Resources.MergedDictionaries.Add(myResDic);
            }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            var tree = GetTemplateChild("PART_TreeView") as ItemsControl;
            if (tree != null)
            {
               tree.Loaded += tree_Loaded;                
            }
        }


        private void tree_Loaded(object sender, RoutedEventArgs e)
        {
            var src = GetItemSource();
            var tree = GetTemplateChild("PART_TreeView") as ItemsControl;
            tree.ItemsSource = src;
        }

        private List<CharacteristicReport> GetItemSource()
        {
            List<CharacteristicReport> listItems = new List<CharacteristicReport>();
            AgentryClientSDK.IAgentryControlViewModelCollectionDisplay vm = DataContext as AgentryClientSDK.IAgentryControlViewModelCollectionDisplay;
            CodeGroup.initCodeGroups(vm);
            foreach (AgentryClientSDK.IAgentryData item in vm)
            { 
                var objDesc = item.PropertyValueByName("ZTechnicalObjectDesc");
                var classificationCollection = item.CollectionByName("Classifications");
                if (classificationCollection != null && classificationCollection.DescendantCount > 0)
                {
                    for (int i = 0; i < classificationCollection.DescendantCount; i++)
                    {
                        ObservableCollection<Characteristic> c = new ObservableCollection<Characteristic>();
                        var crName = classificationCollection.Descendant(i).PropertyValueByName("Name");
                        var characteristicCollection = classificationCollection.Descendant(i).CollectionByName("Characteristics");
                        for (int j = 0; j < characteristicCollection.DescendantCount; j++)
                        {
                            var cName = characteristicCollection.Descendant(j).PropertyValueByName("Description");
                            var valueCollection = characteristicCollection.Descendant(j).CollectionByName("CharacteristicValues");
                            var itemNumber = Convert.ToInt32(characteristicCollection.Descendant(j).PropertyValueByName("ItemNumber"));
                            var listValues = new List<CharacteristicValues>();
                            if (valueCollection != null && valueCollection.DescendantCount > 0)
                            {
                                if (valueCollection.DescendantCount == 1)
                                    c.Add(new Characteristic { Name = cName, Value = valueCollection.Descendant(0).PropertyValueByName("Value") });
                                else
                                {
                                    for (int k = 0; k < valueCollection.DescendantCount; k++)
                                    {
                                        var isLocalValue = valueCollection.Descendant(k).PropertyValueAsBool("ZIsLocalValue");
                                        if(isLocalValue)
                                        {
                                            c.Add(new Characteristic { Name = cName, Value = valueCollection.Descendant(k).PropertyValueByName("Value"), ItemNumber = itemNumber });
                                            break;
                                        }
                                    }
                                }
                                
                            }                            
                        }
                        listItems.Add(new CharacteristicReport(this) { TechnicalPointDesc = objDesc, ClassificationName = crName, Charateristics = c.OrderBy(x => x.ItemNumber).ToList() });
                    }
                }
            }
            return listItems;
        }
    }
}
