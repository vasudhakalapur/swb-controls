﻿using SWB_Controls.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SWB_Controls.TextBoxControls
{
    /// <summary>
    /// Interaktionslogik für AutoCompleteTextBox.xaml
    /// </summary>
    public partial class AutoCompleteTextBox : Grid
    {
        #region Members
        private ObservableCollection<AutoCompleteEntry> autoCompletionList;
        private System.Timers.Timer keypressTimer;
        private delegate void TextChangedCallback();
        public bool insertText;
        private int delayTime;
        private int searchThreshold;
        public bool IsNumeric = true;

        public string Text
        {
            get { return textBox.Text; }
            set
            {
                insertText = true;
                textBox.Text = value;
            }
        }

        public int DelayTime
        {
            get { return delayTime; }
            set { delayTime = value; }
        }

        public int Threshold
        {
            get { return searchThreshold; }
            set { searchThreshold = value; }
        }

        #endregion

        public AutoCompleteTextBox()
        {   
            InitializeComponent();       

            autoCompletionList = new ObservableCollection<AutoCompleteEntry>();
            searchThreshold = 2;

            keypressTimer = new System.Timers.Timer();
            keypressTimer.Elapsed += keypressTimer_Elapsed;

            comboBox.SelectionChanged += comboBox_SelectionChanged;
            textBox.TextChanged += textBox_TextChanged;
        }

        public void AddItem(AutoCompleteEntry entry)
        {
            autoCompletionList.Add(entry);
        }

        public void RemoveAllItems()
        {
            autoCompletionList.Clear();
        }

        void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (insertText == true) insertText = false;
            else
            {
                if (delayTime > 0)
                {
                    keypressTimer.Interval = delayTime;
                    keypressTimer.Start();
                }
                else TextChanged();
            }
        }

        private void TextChanged()
        {
            try
            {
                comboBox.Items.Clear();
                if(textBox.Text.Length >= searchThreshold)
                {
                    foreach(AutoCompleteEntry entry in autoCompletionList)
                    {
                        foreach(string word in entry.KeywordStrings)
                        {
                            if(word.StartsWith(textBox.Text, StringComparison.CurrentCultureIgnoreCase))
                            {
                                ComboBoxItem cbItem = new ComboBoxItem();
                                cbItem.Content = entry.ToString();
                                comboBox.Items.Add(cbItem);                                
                                break;
                            }
                        }
                    }
                    comboBox.Visibility = System.Windows.Visibility.Visible;
                    comboBox.IsDropDownOpen = comboBox.HasItems;
                }
                else
                {
                    comboBox.Visibility = System.Windows.Visibility.Collapsed;
                    comboBox.IsDropDownOpen = false;
                }
            }
            catch { }
        }

        void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboBox.SelectedItem != null)
            {
                insertText = true;
                ComboBoxItem cbItem = (ComboBoxItem)comboBox.SelectedItem;
                textBox.Text = cbItem.Content.ToString();
            }
        }

        void keypressTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            keypressTimer.Stop();
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new TextChangedCallback(this.TextChanged));
        }

        private void textBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (IsNumeric)
            {
               e.Handled = StringHelper.IsNumericTextAllowed(e.Text);
            }
        }
    }
}
