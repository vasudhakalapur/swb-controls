﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWB_Controls.Utils
{
    public static class AgentryUtils
    {
        public static void logData(AgentryClientSDK.IAgentryData item)
        {
            System.Diagnostics.Debug.WriteLine(String.Format("Name: \"{0}\" Type = {1}", item.DisplayName, item.DataType.ToString()));
            /*
             * Enumerate all descendants
             */
            System.Diagnostics.Debug.Indent();
            for (int ix = 0; ix < item.DescendantCount; ++ix)
            {
                AgentryClientSDK.IAgentryData subitem = item.Descendant(ix);
                if (subitem.DataType == AgentryClientSDK.AgentryDataType.Property)
                {
                    AgentryClientSDK.IAgentryProperty prop = (AgentryClientSDK.IAgentryProperty)subitem;
                    System.Diagnostics.Debug.WriteLine(String.Format("Name: \"{0}\" ({1}) Type = {2}, Value = \"{3}\"", subitem.DisplayName, subitem.InternalName, subitem.DataType.ToString(), prop.ToString()));
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine(String.Format("Name: \"{0}\" ({1}) Type = {2}", subitem.DisplayName, subitem.InternalName, subitem.DataType.ToString()));
                }
            }
            System.Diagnostics.Debug.Unindent();

            /*
             * Enumerate all properties
             */
            System.Diagnostics.Debug.Indent();
            foreach (AgentryClientSDK.IAgentryProperty prop in item.Properties())
            {
                System.Diagnostics.Debug.WriteLine(String.Format("Name: \"{0}\" ({1}) Type = {2}, Value: \"{3}\" ({4})", prop.DisplayName, prop.InternalName, prop.DataType.ToString(), prop.ToString(), prop.ToUInt()));
            }
            System.Diagnostics.Debug.Unindent();
        }
    }
}
