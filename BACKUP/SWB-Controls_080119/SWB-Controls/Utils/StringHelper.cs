﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SWB_Controls.Utils
{
    public class StringHelper
    {
        public static string BuildTime(string time)
        {
            if (time.Length > 0)
            {
                //var split = time.Split('.');
                //var hours = split[0];
                //var mins = Convert.ToInt32(split[1]) * 6;
                var newtime = TimeSpan.FromMinutes(double.Parse(time, CultureInfo.InvariantCulture));
                var hours = (newtime.Days > 0) ? (int)newtime.TotalHours : newtime.Hours;
                var mins = newtime.Minutes;
                return String.Format("{0} h {1} min", hours, (mins.ToString().Length == 1) ? String.Format("0{0}", mins) : mins.ToString().Substring(0, 2));
            }
            return string.Empty;
        }
        public static bool IsNumericTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]+");
            return regex.IsMatch(text);
        }
        public static bool IsNumericCommaTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9,]+");
            return regex.IsMatch(text);
        }
        public static bool IsNumericDecimalTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9.]+");
            return regex.IsMatch(text);
        }      
       
        public static bool IsValueInRange(string txt, bool isLower, decimal val)
        {
            if (txt.Length == 0)
                return false;
            if (isLower)
            {
                return Convert.ToDecimal(txt) >= val;
            }
            else
            {
                return Convert.ToDecimal(txt) <= val;
            }
        }
        public static bool IsValueWithinRange(string txt, decimal lowerValue, decimal upperValue)
        {
            if (txt.Length == 0)
                return false;
            return IsValueInRange(txt, true, lowerValue) && IsValueInRange(txt, false, upperValue);
        }
    }
}
