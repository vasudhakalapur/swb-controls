﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using AgentryClientSDK;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows;

namespace SWB_Controls
{
    public class BaseAgentryCustomControl : Control, ICustomAgentryControl, INotifyPropertyChanged
    {
        public Dictionary<string, string> extensionKeys = new Dictionary<String, String>();

        public void SetExtensionString(string key, string value)
        {
            key = key.Trim();
            if (value != null) 
                value = value.Trim();
            if (extensionKeys.ContainsKey(key))
            {
                extensionKeys.Remove(key);
            }
            extensionKeys.Add(key, value);
        }

        public BaseAgentryCustomControl()
        {
            Dispatcher.UnhandledException += Dispatcher_UnhandledException;
        }

        void Dispatcher_UnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show("Inner Exception : " + e.Exception.InnerException + "\r\n\n Message : " + e.Exception.Message + "\r\n\n StackTrace : " + e.Exception.StackTrace);
        }

        public bool ClientDisplaysLabel
        {
            get { return true; }
        }

        public bool ClientDisplaysValidationError
        {
            get { return true; }
        }

        public string GetExtensionString(string key)
        {
            if (!extensionKeys.ContainsKey(key))
            {
                Debug.WriteLine("Agentry requestde non-existing key " + key);
                return String.Empty;
            }
            Debug.WriteLine("Agentry requested key " + key + " Value returned " + extensionKeys[key]);
            return extensionKeys[key] != null? extensionKeys[key] : "";
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void firePropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
