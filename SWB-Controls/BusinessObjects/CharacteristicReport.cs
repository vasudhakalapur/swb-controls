﻿using SWB_Controls.ReportControls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWB_Controls.BusinessObjects
{
    public class CharacteristicReport
    {
        public string TechnicalPointDesc { get; set; }
        public string ClassificationName { get; set; }
        public List<Characteristic> Charateristics { get; set; }
        private CharacteristicReportControl OpenUIControl;
        private ListControls.CharValueListControl cValueListControl;

        public CharacteristicReport(CharacteristicReportControl list)
        {
            OpenUIControl = list;
            this.Charateristics = new List<Characteristic>();
        }

        public CharacteristicReport(ListControls.CharValueListControl cValueListControl)
        {
            // TODO: Complete member initialization
            this.cValueListControl = cValueListControl;
        }
    }
}
