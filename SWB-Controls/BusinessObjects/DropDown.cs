﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWB_Controls.BusinessObjects
{
    public class DropDown
    {
        public static List<string> GetItemSourceForRange(int minValue, int maxValue, int incrementor, bool IsPadding)
        {
            var list = new List<string>();
            int counter = minValue + 1;
            list.Add((IsPadding) ? minValue.ToString("D2") : minValue.ToString());
            while (counter != maxValue)
            {
                if (incrementor > 0)
                {
                    if (counter % incrementor == 0)
                        list.Add((IsPadding) ? counter.ToString("D2") : counter.ToString());
                }
                else
                {
                    list.Add((IsPadding) ? counter.ToString("D2") : counter.ToString());
                }
                counter++;
            }
            return list;
        }
    }
}
