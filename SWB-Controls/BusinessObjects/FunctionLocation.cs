﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWB_Controls.BusinessObjects
{
    public class FunctionLocation
    {
        public string TechObjDesc { get; set; }
        public string ParentDescription { get; set; }
        public string Street { get; set; }
        public string HouseNo { get; set; }
        public string SortField { get; set; }
        public string Description { get { return buildObjDesc(TechObjDesc, Street, HouseNo, SortField, ParentDescription); } }
        public string DescriptionLong { get; set; }
        public string FullDescription { get { return Description + "-" + DescriptionLong; } }
        public string ID { get; set; }
        public string characteristicID { get; set; }
        public String ObjectID { get; set; }
        public String ObjectType { get; set; }
        public List<Characteristic> characteristics = new List<Characteristic>();

        public static string buildFunctionLocDesc(string TechnicalPointDesc, string ClassificationName)
        {
            return String.Format("{0} - {1}", TechnicalPointDesc, ClassificationName);              
        }

        public static string buildObjDesc(string TechObjDesc, string Street, string HouseNo, string sortField, string parentDescription)
        {
            var sortfield = (sortField.Length > 0) ? String.Format("| {0}", sortField) : string.Empty;
            var address = (Street.Length > 0 || HouseNo.Length > 0) ? String.Format("| {0} {1}", Street, HouseNo) : string.Empty;
            return String.Format("{0} {1} {2} {3}", TechObjDesc, address, sortfield, parentDescription);
        }
    }
}
