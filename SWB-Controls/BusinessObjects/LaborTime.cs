﻿using SWB_Controls.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWB_Controls.BusinessObjects
{
    public class LaborTimeDateComparer_Ascending : IComparer
    {
        public int Compare(object x, object y)
        {
            LaborTime x1 = x as LaborTime;
            LaborTime y1 = y as LaborTime;

            return x1.TimeRecordingDate.CompareTo(y1.TimeRecordingDate);
        }
    }
    public class LaborTimeDateComparer_Descending : IComparer
    {
        public int Compare(object x, object y)
        {
            LaborTime x1 = x as LaborTime;
            LaborTime y1 = y as LaborTime;

            return y1.TimeRecordingDate.CompareTo(x1.TimeRecordingDate);
        }
    }
    public class LaborTime
    {
        public int Index { get; set; }
        public string ID { get; set; }
        public DateTime TimeRecordingDate { get; set; }
        System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("de-DE"); 
        public string Day
        {
            get { return culture.DateTimeFormat.GetDayName(TimeRecordingDate.DayOfWeek); }
        }
        public string Date
        {
            get { return TimeRecordingDate.ToString("dd.MM.yyyy"); }
        }
        private string _woTime;
        public string WorkTime
        {
            get
            {
                return StringHelper.BuildTime(_woTime);
            }
            set { _woTime = value; }
        }
        private string _otherTime;
        public string OtherTime
        {
            get
            {
                return StringHelper.BuildTime(_otherTime);
            }
            set { _otherTime = value; }
        }
        private string _sumTime;
        public string Summary
        {
            get
            {
                return StringHelper.BuildTime(_sumTime);
            }
            set { _sumTime = value; }
        }       
    }
}
