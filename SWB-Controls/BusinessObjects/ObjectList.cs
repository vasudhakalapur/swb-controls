﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SWB_Controls.BusinessObjects
{
    public class ObjectList
    {
        public int Index { get; set; }
        public string TechObjDesc { get; set; }
        public string Street { get; set; }
        public string HouseNo { get; set; }
        public string SortField { get; set; }
        public string Description { get { return buildObjDesc(TechObjDesc, Street, HouseNo, SortField); } }
        public string Type { get; set; }
        public string TypeID { get; set; }
        public string ParentDescription { get; set; }
        public string ParentID { get; set; }
        public int NotifCount { get; set; }
        public bool isPreviousReading { get; set; }
        public Visibility FlagVisible { get { return isPreviousReading ? Visibility.Visible : Visibility.Collapsed; } }
        public string SortNum { get; set; }

        public static string buildObjDesc(string TechObjDesc, string Street, string HouseNo, string sortField)
        {
            var sortfield = (sortField.Length > 0) ? String.Format("| {0}", sortField) : string.Empty;
            var address = (Street.Length > 0 || HouseNo.Length > 0) ? String.Format("| {0} {1}", Street, HouseNo) : string.Empty;
            return String.Format("{0} {1} {2}", TechObjDesc,address, sortfield);
        }

    }
}
