﻿using SWB_Controls.ListControls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SWB_Controls.BusinessObjects
{
    public class WorkOrderDateComparer_Ascending : IComparer
    {
        public int Compare(object x, object y)
        {
            WorkOrder x1 = x as WorkOrder;
            WorkOrder y1 = y as WorkOrder;

            return x1.StartAsDate.CompareTo(y1.StartAsDate);
        }
    }
    public class WorkOrderDateComparer_Descending : IComparer
    {
        public int Compare(object x, object y)
        {
            WorkOrder x1 = x as WorkOrder;
            WorkOrder y1 = y as WorkOrder;

            return y1.StartAsDate.CompareTo(x1.StartAsDate);
        }
    }
    public class WorkOrderComparer : IComparer
    {

        public int Compare(object x, object y)
        {
            WorkOrder x1 = x as WorkOrder;
            WorkOrder y1 = y as WorkOrder;
            //FIX vor KAPA
            {
                if (x1.isKapaWO && !y1.isKapaWO)
                {
                    return 1;
                }
                if (y1.isKapaWO && !x1.isKapaWO)
                {
                    return -1;
                }
            }
            //FIX order by Date
            {
                if (!x1.isKapaWO && !y1.isKapaWO)
                {
                    return x1.StartAsDate.CompareTo(y1.StartAsDate);
                }
            }
            //KAPA order by Prio
            if (x1.PriorityUI > 0 && y1.PriorityUI < 1)
            {
                return 1;
            }
            if (x1.PriorityUI < 1 && y1.PriorityUI > 0)
            {
                return -1;
            }
            if (x1.PriorityUI > 0 && y1.PriorityUI > 0)
            {
                return x1.Priority.CompareTo(y1.Priority);
            }

            //Rest order by Workorder-No
            return x1.Wonr.CompareTo(y1.Wonr);
        }
    }

    public class WorkOrder
    {
        // Data Fields

        public int Index { get; set; }
        public string Wonr { get; set; }
        public string Status { get; set; }
        public DateTime StartAsDate { get; set; }
        public string Start { get { return StartAsDate != DateTime.MinValue ? StartAsDate.ToString("dd.MM HH:mm") : String.Empty; } }
        public string Description { get; set; }
        public string DescriptionLong { get; set; }
        public string Techobj { get; set; }
        public string TechobjLong { get; set; }
        public string Arbeitsplatz { get; set; }
        public string ActiveOperationNum { get; set; }
        public bool isKapaWO { get; set; }
        public string DueAsDate { get; set; }
        public string DueDate { get { return (isKapaWO) ? string.IsNullOrEmpty(DueAsDate) ? string.Empty : DateTime.ParseExact(DueAsDate, "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString("dd.MM.yyyy") : string.Empty; } }
        public int Priority { get; set; }
        public int PriorityUI
        {
            get { return Priority != null ? Priority : 0; }
            set
            {
                if (value != null)
                {
                    Priority = value;
                    OpenUIControl.SetExtensionString("ZCustomPriority", value.ToString());
                    context.ExecuteAgentryAction("ZOpenUIChangeWOPriority");
                }
                else
                { System.Console.Beep(); }
            }
        }
        private WorkOrderListControl OpenUIControl;

        public AgentryClientSDK.IAgentryControlViewModelCollectionDisplay context { get; set; }

        public WorkOrder(WorkOrderListControl list)
        {
            OpenUIControl = list;
            Priority = 0;
        }

    }
}
