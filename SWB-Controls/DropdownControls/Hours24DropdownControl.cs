﻿using SWB_Controls.BusinessObjects;
using SWB_Controls.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SWB_Controls.DropdownControls
{
    public class Hours24DropdownControl : BaseAgentryCustomControl
    {
          public AgentryClientSDK.IAgentryControlViewModelStringDisplay context { get; set; }
          static Hours24DropdownControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Hours24DropdownControl), new FrameworkPropertyMetadata(typeof(Hours24DropdownControl)));
            PureVirtualHandler.RegisterHandler();
        }
        public void SetAgentryValue()
        {
            context = DataContext as AgentryClientSDK.IAgentryControlViewModelStringDisplay;
            ComboBox cmb = GetTemplateChild("PART_Hours") as ComboBox;

            //GetItemSourceForRange arguments
            //minValue = 0 (first value / lowest value)
            //maxValue = 24 (last value / max value adding 1)
            //incrementor = 0 (0 means no incrementor condition for list)
            //2 digit padding e.g 0 will be 00 if set to true
            cmb.ItemsSource = DropDown.GetItemSourceForRange(0, 24, 0, true);
            var onsiteHours = context.GetAgentryString("ZOnsiteHours");
            var createdHours = context.GetAgentryString("ZCreatedHours");
            if (!string.IsNullOrEmpty(onsiteHours))
                cmb.SelectedItem = onsiteHours;
            else if (!string.IsNullOrEmpty(createdHours))
                cmb.SelectedItem = createdHours;
            else
                cmb.SelectedIndex = 0;

            if(cmb.SelectedItem == null)
                cmb.SelectedIndex = 0;

            context.StringValue = cmb.SelectedItem.ToString();
            cmb.SelectionChanged += cmb_SelectionChanged;
        }
        private void cmb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var cmb = sender as ComboBox;
            context.StringValue = cmb.SelectedItem.ToString();
        }
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            SetAgentryValue();
        }
    }
}
