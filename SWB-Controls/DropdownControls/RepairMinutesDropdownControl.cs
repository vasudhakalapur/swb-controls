﻿using SWB_Controls.BusinessObjects;
using SWB_Controls.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using SWB_Controls.TextBoxControls;


namespace SWB_Controls.DropdownControls
{
    public class RepairMinutesDropdownControl : BaseAgentryCustomControl
    {
        public AgentryClientSDK.IAgentryControlViewModelStringDisplay context { get; set; }
        public static TextBox txtMinutes { get; set; }
        private string _messageText;
        public string MessageText { get { return _messageText; } set { _messageText = value; firePropertyChanged("MessageText"); } }
        static RepairMinutesDropdownControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(RepairMinutesDropdownControl), new FrameworkPropertyMetadata(typeof(RepairMinutesDropdownControl)));           
            PureVirtualHandler.RegisterHandler();
        }       

        public void SetAgentryValue()
        {
            context = DataContext as AgentryClientSDK.IAgentryControlViewModelStringDisplay;
            txtMinutes = GetTemplateChild("PART_Minutes") as TextBox;
            txtMinutes.DataContext = this;
            //GetItemSourceForRange arguments
            //minValue = 0 (first value / lowest value)
            //maxValue = 55 (last value / max value adding 1)
            //incrementor = 6 (jumping value and listing multiples of incrementor within the range)
            MessageText = "0";
            txtMinutes.GotFocus += txtMinutes_GotFocus;
            txtMinutes.LostFocus += txtMinutes_LostFocus;
            txtMinutes.PreviewTextInput += txtMinutes_PreviewTextInput;
            txtMinutes.TextChanged += txtMinutes_TextChanged;
            DataObject.AddPastingHandler(txtMinutes, OnPaste);
        }

        void txtMinutes_GotFocus(object sender, RoutedEventArgs e)
        {
            MessageText = string.Empty;
        }

        void txtMinutes_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txtMinutes.Text.Length == 0)
                MessageText = "0";
        }

        private void OnPaste(object sender, DataObjectPastingEventArgs e)
        {
            if (e.SourceDataObject.GetDataPresent(typeof(String)))
            {
                String text = (String)e.DataObject.GetData(typeof(String));
                if (StringHelper.IsNumericTextAllowed(text))
                    e.CancelCommand();
                else
                {
                    if (Convert.ToInt32(text) < 0 || Convert.ToInt32(text) > 59)
                        e.CancelCommand();
                }
            }
            else
                e.CancelCommand();
        }
        void txtMinutes_TextChanged(object sender, RoutedEventArgs e)
        {
            var textbox = sender as TextBox;
            if (textbox.Text.Length > 0)
            {
                context.StringValue = textbox.Text;
                if (Convert.ToInt32(textbox.Text) == 0 && RepairHoursDropdownControl.cmbHours.SelectedIndex == 0)
                    RepairTextControl40Chars.textbox.IsEnabled = false;
                else
                    RepairTextControl40Chars.textbox.IsEnabled = true;
            }
        }

        void txtMinutes_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = StringHelper.IsNumericTextAllowed(e.Text);
            var textbox = (sender as TextBox);
            if (!e.Handled)
            {
                if (textbox.CaretIndex > 0)
                {
                    if (textbox.Text[0] == '6' || textbox.Text[0] == '7' || textbox.Text[0] == '8' || textbox.Text[0] == '9')
                        e.Handled = true;
                }
                else
                {
                    if (((textbox.SelectedText.Length == 1 && textbox.Text.Length > 1) || (textbox.SelectedText.Length == 0 && textbox.Text.Length >= 1)) && Convert.ToInt32(e.Text) > 5)
                    {
                        e.Handled = true;
                    }
                }
            }
        }
       
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            SetAgentryValue();
        }
    }
}
