﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Configuration;
using System.IO;
using SWB_Controls.Utils;

namespace SWB_Controls.ListControls
{
    public class EnhancedDataGrid : DataGrid
    {
        private bool inWidthChange = false;
        private bool updatingColumnInfo = false;
        private string gridName = "";
        public static readonly DependencyProperty ColumnInfoProperty = DependencyProperty.Register("ColumnInfo", typeof(ObservableCollection<ColumnInfo>), typeof(EnhancedDataGrid), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, ColumnInfoChangedCallback));
        private static void ColumnInfoChangedCallback(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var grid = (EnhancedDataGrid)obj;
            if (!grid.updatingColumnInfo) { grid.ColumnInfoChanged(); }
        }

        public event EventHandler<ValueEventArgs<DataGridColumn>> Sorted;

        protected override void OnSorting(DataGridSortingEventArgs eventArgs)
        {
            base.OnSorting(eventArgs);
            if (Sorted == null) return;
            var column = eventArgs.Column;
            Sorted(this, new ValueEventArgs<DataGridColumn>(column));
        }
        protected override void OnInitialized(EventArgs e)
        {
            gridName = this.Name;
            EventHandler sortDirectionChangedHandler = (sender, x) => UpdateColumnInfo();
            EventHandler widthPropertyChangedHandler = (sender, x) => inWidthChange = true;
            var sortDirectionPropertyDescriptor = DependencyPropertyDescriptor.FromProperty(DataGridColumn.SortDirectionProperty, typeof(DataGridColumn));
            var widthPropertyDescriptor = DependencyPropertyDescriptor.FromProperty(DataGridColumn.WidthProperty, typeof(DataGridColumn));
            Loaded += (sender, x) =>
                {
                    foreach (var column in Columns)
                    {
                        sortDirectionPropertyDescriptor.AddValueChanged(column, sortDirectionChangedHandler);
                        widthPropertyDescriptor.AddValueChanged(column, widthPropertyChangedHandler);
                        var widthKey = gridName + "_" + column.Header + "_width";
                        var displayIndexKey = gridName + "_" + column.Header + "_index";
                        if (ConfigurationHelper.GetConfigSettings[widthKey] != null)
                        {
                            column.Width = new DataGridLength(Convert.ToDouble(ConfigurationHelper.GetConfigSettings[widthKey].Value), column.Width.UnitType);
                        }

                        if (ConfigurationHelper.GetConfigSettings[displayIndexKey] != null)
                        {
                            column.DisplayIndex = Convert.ToInt32(ConfigurationHelper.GetConfigSettings[displayIndexKey].Value);
                        }
                    }
                };
            Unloaded += (sender, x) =>
             {
                 foreach (var column in Columns)
                 {
                     sortDirectionPropertyDescriptor.RemoveValueChanged(column, sortDirectionChangedHandler);
                     widthPropertyDescriptor.RemoveValueChanged(column, widthPropertyChangedHandler);
                 }
             };
            base.OnInitialized(e);
        }

        private void UpdateColumnInfo()
        {           
            updatingColumnInfo = true;
            ColumnInfo = new ObservableCollection<ColumnInfo>(Columns.Select((x) => new ColumnInfo(x)));
            ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
            var config = ConfigurationHelper.GetConfig;
            var settings = config.AppSettings.Settings;
            foreach (var col in ColumnInfo)
            {
                var widthKey = gridName + "_" + col.Header + "_width";
                var displayIndexKey = gridName + "_" + col.Header + "_index";
                if (settings[widthKey] == null)
                {
                    settings.Add(widthKey, col.WidthValue.ToString());
                }
                else
                {
                    settings[widthKey].Value = col.WidthValue.ToString();
                }

                if (settings[displayIndexKey] == null)
                {
                    settings.Add(displayIndexKey, col.DisplayIndex.ToString());
                }
                else
                {
                    settings[displayIndexKey].Value = col.DisplayIndex.ToString();
                }
                ConfigurationHelper.SaveConfig(config);
            }
            updatingColumnInfo = false;
        }

        public ObservableCollection<ColumnInfo> ColumnInfo
        {
            get { return (ObservableCollection<ColumnInfo>)GetValue(ColumnInfoProperty); }
            set { SetValue(ColumnInfoProperty, value); }
        }
        protected override void OnColumnReordered(DataGridColumnEventArgs e)
        {
            UpdateColumnInfo();
            base.OnColumnReordered(e);
        }
        protected override void OnPreviewMouseLeftButtonUp(System.Windows.Input.MouseButtonEventArgs e)
        {
            if (inWidthChange)
            {
                inWidthChange = false;
                UpdateColumnInfo();
            }
            base.OnPreviewMouseLeftButtonUp(e);
        }
        private void ColumnInfoChanged()
        {
            Items.SortDescriptions.Clear();
            foreach (var col in ColumnInfo)
            {
                var realColumn = Columns.Where((x) => col.Header.Equals(x.Header)).FirstOrDefault();
                if (realColumn == null) { continue; }
                col.Apply(realColumn, Columns.Count, Items.SortDescriptions);
            }
        }
    }
    public struct ColumnInfo
    {
        public object Header;
        public string PropertyPath;
        public ListSortDirection? SortDirection;
        public int DisplayIndex;
        public double WidthValue;
        public DataGridLengthUnitType WidthType;
        public ColumnInfo(DataGridColumn col)
            : this()
        {
            Header = col.Header;
            PropertyPath = FindBoundProperty(col);
            WidthValue = col.Width.DisplayValue;
            WidthType = col.Width.UnitType;
            SortDirection = col.SortDirection;
            DisplayIndex = col.DisplayIndex;
        }

        public void Apply(DataGridColumn col, int gridColumnCount, SortDescriptionCollection sortDescriptions)
        {
            col.Width = new DataGridLength(WidthValue, WidthType);
            col.SortDirection = SortDirection;
            if (SortDirection != null)
            {
                sortDescriptions.Add(new SortDescription(PropertyPath, SortDirection.Value));
            }
            if (col.DisplayIndex != DisplayIndex)
            {
                var maxIndex = (gridColumnCount == 0) ? 0 : gridColumnCount - 1;
                col.DisplayIndex = (DisplayIndex <= maxIndex) ? DisplayIndex : maxIndex;
            }
        }

        public string FindBoundProperty(DataGridColumn col)
        {
            DataGridBoundColumn boundColumn = col as DataGridBoundColumn;
            string boundPropertyName = "";
            Binding binding = default(Binding);
            if (col.DependencyObjectType.Name == "DataGridTextColumn" && boundColumn.Binding !=null)
            {
                binding = boundColumn.Binding as Binding;
                boundPropertyName = binding.Path.Path;
            }
            if (col.DependencyObjectType.Name == "DataGridTemplateColumn")
            {
                binding = col.ClipboardContentBinding as Binding;
                if (binding != null)
                    boundPropertyName = binding.Path.Path;
            }
            return boundPropertyName;
        }
    }
    public class ValueEventArgs<T> : EventArgs
    {
        public T Value { get; set; }
        public ValueEventArgs(T value)
        {
            Value = value;
        }
    }
}
