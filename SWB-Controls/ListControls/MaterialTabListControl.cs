﻿using AgentryClientSDK;
using SWB_Controls.BusinessObjects;
using SWB_Controls.Extensions;
using SWB_Controls.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;

namespace SWB_Controls.ListControls
{
    public class MaterialTabListControl : BaseAgentryCustomControl
    {
         public CollectionViewSource cvs;
        public static DataGrid grid { get; set; }
        private System.Timers.Timer refreshTimer;
        private Material _selectedItem;
        public Material SelectedMaterial
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                _selectedItem = value;
                if (value != null)
                {
                    ((AgentryClientSDK.IAgentryControlViewModelCollectionDisplay)DataContext).SelectItem(value.Index);
                }
            }
        }
        static MaterialTabListControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(MaterialTabListControl), new FrameworkPropertyMetadata(typeof(MaterialTabListControl)));
            PureVirtualHandler.RegisterHandler();
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            grid = GetTemplateChild("PART_GridMaterialList") as DataGrid;
            if (grid != null)
            {
                grid.Loaded -= grid_Loaded;
                grid.Loaded += grid_Loaded;
                grid.DataContext = this;
            }
        }

        private void grid_Loaded(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Material Grid loaded triggered");
            List<Material> materials = new List<Material>();
            AgentryClientSDK.IAgentryControlViewModelCollectionDisplay vm = DataContext as AgentryClientSDK.IAgentryControlViewModelCollectionDisplay;
            vm.PropertyChanged += dc_PropertyChanged;
            int index = 0;
            foreach (AgentryClientSDK.IAgentryData item in vm)
            {
                materials.Add(new Material
                              {                                
                                  Number = item.PropertyValueByName("ItemNum"),
                                  Quantity = item.PropertyValueByName("Quantity"),
                                  UOM = item.PropertyValueByName("UOM"),
                                  Description = item.PropertyValueByName("Description"),
                                  Category = (item.PropertyValueByName("StockType") == "W") ? "X" : "",
                                  Index = index++
                              });               

            }

            cvs = new CollectionViewSource();
            cvs.Source = materials;
            grid.ItemsSource = cvs.View;
            grid.Items.MoveCurrentToFirst();
            grid.SelectedIndex = 0;
            SelectedMaterial = (Material)grid.SelectedItem;
        }

        private void dc_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (refreshTimer == null)
            {
                refreshTimer = new System.Timers.Timer(100);
                refreshTimer.Elapsed += t_Elapsed;
                refreshTimer.Start();
            }
        }
        void t_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                IAgentryControlViewModelCollectionDisplay dc = null;
                this.Dispatcher.Invoke(new Action(() => dc = this.DataContext as IAgentryControlViewModelCollectionDisplay));
                this.Dispatcher.Invoke(new Action(() => { grid_Loaded(null, new RoutedEventArgs()); }));
                refreshTimer.Stop();
                refreshTimer = null;
            }
            catch
            {
                //TODO 
                //Do nothing for now.
            }
        }
    }
}
