﻿using AgentryClientSDK;
using SWB_Controls.BusinessObjects;
using SWB_Controls.Extensions;
using SWB_Controls.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;

namespace SWB_Controls.ListControls
{
    public class ObjectListControl : BaseAgentryCustomControl
    {
        public CollectionViewSource cvs;
        public static DataGrid grid { get; set; }
        private System.Timers.Timer refreshTimer;
        static string selectedObject = "";
        private bool ignoreSelectionChanges = false; //this will be set to true during intialization of grid to  avoid saving wrong selection.
        public ObjectList _selectedItem;
        public ObjectList SelectedObj
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                _selectedItem = value;
                if (ignoreSelectionChanges) return;
                if (_selectedItem != null)
                {
                    selectedObject = _selectedItem.TypeID + _selectedItem.ParentID;
                    ((AgentryClientSDK.IAgentryControlViewModelCollectionDisplay)DataContext).SelectItem(value.Index);
                }
            }
        }
        static ObjectListControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ObjectListControl), new FrameworkPropertyMetadata(typeof(ObjectListControl)));
            injectDictionary();
            PureVirtualHandler.RegisterHandler();
        }

        private static void injectDictionary()
        {
            var ResName = "SWB-Controls;component/Resources/Theme.xaml";
            var uri = new Uri(ResName, UriKind.Relative);

            var streamResourceInfo = Application.GetResourceStream(uri);

            using (var resStream = streamResourceInfo.Stream)
            {
                ResourceDictionary myResDic = (ResourceDictionary)XamlReader.Load(resStream);
                Application.Current.Resources.MergedDictionaries.Add(myResDic);
            }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            grid = GetTemplateChild("PART_GridObjectList") as DataGrid;
            if (grid != null)
            {
                grid.Loaded -= grid_Loaded;
                grid.Loaded += grid_Loaded;
                grid.DataContext = this;
            }
        }

        private void grid_Loaded(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Object Grid loaded triggered");
            ignoreSelectionChanges = true;
            List<ObjectList> objects = new List<ObjectList>();
            AgentryClientSDK.IAgentryControlViewModelCollectionDisplay vm = DataContext as AgentryClientSDK.IAgentryControlViewModelCollectionDisplay;
            string woNum = vm.GetAgentryString("WONum");
            vm.PropertyChanged += dc_PropertyChanged;
            //CodeGroup.initCodeGroups(vm);
            int index = 0;
            foreach (AgentryClientSDK.IAgentryData item in vm)
            {

                objects.Add(new ObjectList
                              {
                                  TechObjDesc = item.PropertyValueByName("ZTechnicalObjectDesc"),
                                  Street = item.PropertyValueByName("ZStreet"),
                                  HouseNo = item.PropertyValueByName("ZHouseNum"),
                                  SortField = item.PropertyValueByName("ZSortField"),
                                  Type = item.PropertyValueByName("ZType"),
                                  TypeID = item.PropertyValueByName("ZTechnicalObject"),
                                  ParentDescription = item.PropertyValueByName("ZParentTechnicalObjectDesc"),
                                  ParentID = item.PropertyValueByName("ZParentTechnicalObject"),
                                  NotifCount = GetNotificationCount(item, item.PropertyValueByName("ZTechnicalObject"), item.PropertyValueByName("ZType")) + GetHistoryNotificationCount(item, item.PropertyValueByName("ZTechnicalObject"), item.PropertyValueByName("ZType")),
                                  isPreviousReading = GetPreviousReading(item, woNum),
                                  SortNum = item.PropertyValueByName("ZSortNum"),
                                  Index = index++
                              });
            }

            DataGrid grid = GetTemplateChild("PART_GridObjectList") as DataGrid;
            cvs = new CollectionViewSource();
            cvs.Source = objects;

            cvs.SortDescriptions.Add(new System.ComponentModel.SortDescription("SortNum", System.ComponentModel.ListSortDirection.Descending));

            grid.ItemsSource = cvs.View;
            if (!String.IsNullOrEmpty(selectedObject))
            {
                var item = objects.SingleOrDefault(x => x.TypeID + x.ParentID == selectedObject);
                if (item != null)
                {
                    System.Diagnostics.Debug.WriteLine("Restore of selected object from " + selectedObject);
                    grid.SelectedItem = item;
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("Could not find selected object from" + selectedObject + " selecting first row instead");
                    grid.SelectedIndex = 0;
                }
            }
            else
            {
                grid.Items.MoveCurrentToFirst();
                grid.SelectedIndex = 0;
            }
            ignoreSelectionChanges = false;
            SelectedObj = (ObjectList)grid.SelectedItem;
            grid.UpdateLayout();
            grid.ScrollIntoView(grid.SelectedIndex);      
        }

        private void dc_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (refreshTimer == null)
            {
                refreshTimer = new System.Timers.Timer(100);
                refreshTimer.Elapsed += t_Elapsed;
                refreshTimer.Start();
            }
        }
        void t_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                IAgentryControlViewModelCollectionDisplay dc = null;
                this.Dispatcher.Invoke(new Action(() => dc = this.DataContext as IAgentryControlViewModelCollectionDisplay));
                this.Dispatcher.Invoke(new Action(() => { grid_Loaded(null, new RoutedEventArgs()); }));
                refreshTimer.Stop();
                refreshTimer = null;
            }
            catch
            {
                //TODO 
                //Do nothing for now.
            }
        }

        private int GetNotificationCount(AgentryClientSDK.IAgentryData item, string obj, string type)
        {
            var count = 0;
            if (item != null)
            {
                for (int i = 0; i < item.DescendantCount; i++)
                {
                    var descendant = item.Descendant(i);
                    if (descendant.InternalName == "Notifications")
                    {
                        if (descendant.DescendantCount > 0)
                        {
                            for (int j = 0; j < descendant.DescendantCount; j++)
                            {
                                var funcLoc = descendant.Descendant(j);
                                if (type == "TP")
                                {
                                    if (funcLoc.PropertyValueByName("FuncLoc") == obj)
                                        count++;

                                }
                                //removing notification that have equipment set
                                //else if (type == "EQ")
                                //{
                                //    if (funcLoc.PropertyValueByName("EquipmentID") == obj)
                                //        count++;
                                //}
                            }
                        }
                    }
                }
            }
            return count;
        }

        private int GetHistoryNotificationCount(AgentryClientSDK.IAgentryData item, string obj, string type)
        {
            var count = 0;
            for (int i = 0; i < item.DescendantCount; i++)
            {
                var descendant = item.Descendant(i);
                if (descendant.InternalName == "ZHistoryNotifications")
                {
                    if (descendant.DescendantCount > 0)
                    {
                        for (int j = 0; j < descendant.DescendantCount; j++)
                        {
                            var funcLoc = descendant.Descendant(j);
                            if (type == "TP")
                            {
                                if (funcLoc.PropertyValueByName("FuncLoc") == obj)
                                    count++;

                            }
                        }
                    }
                }
            }
            return count;
        }

        private bool GetPreviousReading(AgentryClientSDK.IAgentryData item, string wonum)
        {
            for (int i = 0; i < item.DescendantCount; i++)
            {
                var descendant = item.Descendant(i);
                if (descendant.InternalName == "MeasuringPoints")
                {
                    if (descendant.DescendantCount > 0)
                    {
                        for (int j = 0; j < descendant.DescendantCount; j++)
                        {
                            var desc2 = descendant.Descendant(j);
                            if (desc2.PropertyValueAsBool("ZIsRead"))
                            {
                                return true;
                            }
                            var histDocsCollection = desc2.CollectionByName("ZHistoryMeasurementDocuments");
                            for (int k = 0; k < histDocsCollection.DescendantCount; k++)
                            {
                                 var doc = histDocsCollection.Descendant(k);
                                 if (doc.PropertyValueByName("ZWONum") == wonum)
                                 {
                                     return true;
                                 }

                            }
                        }
                    }
                }
            }
            return false;
        }
    }
}
