﻿using AgentryClientSDK;
using SWB_Controls.BusinessObjects;
using SWB_Controls.Extensions;
using SWB_Controls.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;

namespace SWB_Controls.ListControls
{
    public class OperationTabListControl : BaseAgentryCustomControl
    {
          public CollectionViewSource cvs;
        public static DataGrid grid { get; set; }
        private System.Timers.Timer refreshTimer;
        public Operation _selectedItem;
        public Operation SelectedOpr
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                _selectedItem = value;
                if (value != null)
                {
                    ((AgentryClientSDK.IAgentryControlViewModelCollectionDisplay)DataContext).SelectItem(value.Index);
                }
            }
        }
        static OperationTabListControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(OperationTabListControl), new FrameworkPropertyMetadata(typeof(OperationTabListControl)));
            PureVirtualHandler.RegisterHandler();
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            grid = GetTemplateChild("PART_GridOperationList") as DataGrid;
            if (grid != null)
            {
                grid.Loaded -= grid_Loaded;
                grid.Loaded += grid_Loaded;
                grid.DataContext = this;
            }
        }

        private void grid_Loaded(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Operation Grid loaded triggered");
            List<Operation> operations = new List<Operation>();
            AgentryClientSDK.IAgentryControlViewModelCollectionDisplay vm = DataContext as AgentryClientSDK.IAgentryControlViewModelCollectionDisplay;
            vm.PropertyChanged += dc_PropertyChanged;
            int index = 0;
            foreach (AgentryClientSDK.IAgentryData item in vm)
            {
                operations.Add(new Operation
                              {                                
                                  Number = item.PropertyValueByName("OperationNum"),
                                  WorkCenter = item.PropertyValueByName("WorkCenter"),
                                  Description = item.PropertyValueByName("Description"),
                                  TelNum = item.PropertyValueByName("ZTelephoneNum"),
                                  PersName = (item.PropertyValueByName("ControlKey") == "LVEX") ? item.PropertyValueByName("ZVendorName") : item.PropertyValueByName("ZSplitPersonName"),
                                  IsPersSupervisor = (item.PropertyValueByName("ZHeaderPersonNumber") == item.PropertyValueByName("ZSplitPersonNumber")),
                                  Index = index++
                              });               

            }

            cvs = new CollectionViewSource();
            cvs.Source = operations;
            grid.ItemsSource = cvs.View;
            grid.Items.MoveCurrentToFirst();
            grid.SelectedIndex = 0;
            SelectedOpr = (Operation)grid.SelectedItem;  
        }

        private void dc_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (refreshTimer == null)
            {
                refreshTimer = new System.Timers.Timer(100);
                refreshTimer.Elapsed += t_Elapsed;
                refreshTimer.Start();
            }
        }
        void t_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                IAgentryControlViewModelCollectionDisplay dc = null;
                this.Dispatcher.Invoke(new Action(() => dc = this.DataContext as IAgentryControlViewModelCollectionDisplay));
                this.Dispatcher.Invoke(new Action(() => { grid_Loaded(null, new RoutedEventArgs()); }));
                refreshTimer.Stop();
                refreshTimer = null;
            }
            catch
            {
                //TODO 
                //Do nothing for now.
            }
        }
    }
}
