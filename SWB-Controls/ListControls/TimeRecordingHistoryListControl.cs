﻿using SWB_Controls.BusinessObjects;
using SWB_Controls.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Timers;
using AgentryClientSDK;
using System.Windows.Markup;
using SWB_Controls.Utils;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Threading;
using System.Collections;

namespace SWB_Controls.ListControls
{
    public class TimeRecordingHistoryListControl : BaseAgentryCustomControl
    {
        static TimeRecordingHistoryListControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TimeRecordingHistoryListControl), new FrameworkPropertyMetadata(typeof(TimeRecordingHistoryListControl)));
            injectDictionary();
            PureVirtualHandler.RegisterHandler();
        }
        private static void injectDictionary()
        {
            var ResName = "SWB-Controls;component/Resources/Theme.xaml";
            var uri = new Uri(ResName, UriKind.Relative);
            var streamResourceInfo = Application.GetResourceStream(uri);

            using (var resStream = streamResourceInfo.Stream)
            {
                ResourceDictionary myResDic = (ResourceDictionary)XamlReader.Load(resStream);
                Application.Current.Resources.MergedDictionaries.Add(myResDic);
            }
        }
        static string selectedID = "";
        private System.Timers.Timer refreshTimer;
        static Dictionary<string, ListSortDirection> dictSort = new Dictionary<string, ListSortDirection>();
        public CollectionViewSource cvs;
        private bool ignoreSelectionChanges = false; //this will be set to true during intialization of grid to  avoid saving wrong selection.
        private LaborTime _selectedItem;
        public LaborTime SelectedRecord
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                _selectedItem = value;

                if (ignoreSelectionChanges) return;

                if (_selectedItem != null)
                {
                    ((AgentryClientSDK.IAgentryControlViewModelCollectionDisplay)DataContext).SelectItem(value.Index);
                    selectedID = _selectedItem.ID;
                    System.Diagnostics.Debug.WriteLine("storing selectedID to " + selectedID);
                }
            }
        }

        private ObservableCollection<LaborTime> records;
        private DataGrid grid;

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            grid = GetTemplateChild("PART_GridDailyOverviewList") as DataGrid;
            if (grid != null)
            {
                grid.Loaded += grid_Loaded;
                grid.Sorting += grid_Sorting;
                grid.DataContext = this;
            }
        }

        void grid_Sorting(object sender, DataGridSortingEventArgs e)
        {
            DataGridColumn column = e.Column;            

            if (column.SortMemberPath == "TimeRecordingDate")
            {
                e.Handled = true;
                var direction = (column.SortDirection != ListSortDirection.Ascending) ? ListSortDirection.Ascending : ListSortDirection.Descending;
                column.SortDirection = direction;

                var lcv = cvs.View as ListCollectionView;
                IComparer comparer = (direction == ListSortDirection.Ascending) ? (IComparer)new LaborTimeDateComparer_Ascending() : (IComparer)new LaborTimeDateComparer_Descending();
                lcv.CustomSort = comparer;
            }

        }

        //private void grid_Sorted(object sender, ValueEventArgs<DataGridColumn> e)
        //{
        //    var col = (DataGridColumn)e.Value;
        //    dictSort.Clear();
        //    dictSort.Add(col.SortMemberPath, (ListSortDirection)col.SortDirection);
        //}

        //private void applySortDescriptions(DataGrid dataGrid)
        //{
        //    dataGrid.Items.SortDescriptions.Clear();
        //    foreach (DataGridColumn c in dataGrid.Columns)
        //    {
        //        if (dictSort.ContainsKey(c.SortMemberPath))
        //        {
        //            dataGrid.Items.SortDescriptions.Add(new SortDescription(c.SortMemberPath, (ListSortDirection)dictSort[c.SortMemberPath]));
        //            c.SortDirection = (ListSortDirection)dictSort[c.SortMemberPath];
        //        }
        //        else
        //        {
        //            c.SortDirection = null;
        //        }
        //    }
        //    dataGrid.Items.Refresh();
        //}
        private void grid_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("Time Recording History Grid load triggered");
                ignoreSelectionChanges = true;
                records = new ObservableCollection<LaborTime>();
                AgentryClientSDK.IAgentryControlViewModelCollectionDisplay vm = DataContext as AgentryClientSDK.IAgentryControlViewModelCollectionDisplay;

                vm.PropertyChanged += dc_PropertyChanged;
                int index = 0;
                foreach (AgentryClientSDK.IAgentryData item in vm)
                {
                    var record =
                    new LaborTime
                    {
                        WorkTime = item.PropertyValueByName("WoTimes"),
                        OtherTime = item.PropertyValueByName("OtherTimes"),
                        Summary = item.PropertyValueByName("SumOfTimes"),
                        TimeRecordingDate = item.PropertyValueAsDateTimeByName("TimeRecordingDate"),
                        ID = item.PropertyValueByName("Key"),
                        Index = index++
                    };
                    records.Add(record);
                }

                cvs = new CollectionViewSource();
                cvs.Source = records;

                ListCollectionView lcv = (ListCollectionView)cvs.View;
                lcv.CustomSort = new LaborTimeDateComparer_Descending();

                grid.ItemsSource = cvs.View;

                //if (dictSort.Keys.Count() > 0)
                //{
                //    applySortDescriptions(grid);
                //}

                if (selectedID != "")
                {
                    var item = records.SingleOrDefault(x => x.ID == selectedID);
                    if (item != null)
                    {
                        System.Diagnostics.Debug.WriteLine("Restore of selectedID from " + selectedID);
                        grid.SelectedItem = item;
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("Could not find selectedID from " + selectedID + " selecting first row instead");
                        grid.SelectedIndex = 0;
                    }
                }
                else
                {
                    grid.SelectedIndex = 0;
                    System.Diagnostics.Debug.WriteLine("No selectedID stored - selecting first row instead");
                }

                ignoreSelectionChanges = false;
                SelectedRecord = (LaborTime)grid.SelectedItem;
                grid.UpdateLayout();
                grid.ScrollIntoView(grid.SelectedIndex);
                //System.Diagnostics.Debug.WriteLine(StringHelper.BuildTime("1990"));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Grid Load Exception : " + ex.Message);
            }
        }

        private void dc_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (refreshTimer == null)
            {
                refreshTimer = new System.Timers.Timer(100);
                refreshTimer.Elapsed += t_Elapsed;
                refreshTimer.Start();
            }
        }
        void t_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                IAgentryControlViewModelCollectionDisplay dc = null;
                this.Dispatcher.Invoke(new Action(() => dc = this.DataContext as IAgentryControlViewModelCollectionDisplay));
                this.Dispatcher.Invoke(new Action(() => { grid_Loaded(null, new RoutedEventArgs()); }));
                refreshTimer.Stop();
                refreshTimer = null;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Timer Exception : " + ex.Message);
            }
        }
    }
}
