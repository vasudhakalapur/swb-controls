﻿<ResourceDictionary 
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"    
    xmlns:local="clr-namespace:SWB_Controls.ListControls"
    xmlns:BusinessObjects="clr-namespace:SWB_Controls.BusinessObjects"
>

    <Style x:Key="RadioButtonListBoxStyle" TargetType="{x:Type ListBox}">
        <Setter Property="BorderBrush" Value="Transparent"/>
        <Setter Property="KeyboardNavigation.DirectionalNavigation" Value="Cycle" />
        <Setter Property="ItemContainerStyle">
            <Setter.Value>
                <Style TargetType="{x:Type ListBoxItem}">
                    <Setter Property="Margin" Value="2,2,2,0" />
                    <Setter Property="Template">
                        <Setter.Value>
                            <ControlTemplate>
                                <RadioButton VerticalAlignment="Center" x:Name="PART_Radio"
                                                  IsChecked="{Binding Path=IsSelected,RelativeSource={RelativeSource FindAncestor, AncestorType={x:Type ListBoxItem}}}">
                                    <RadioButton.Content>
                                        <TextBlock Text="{Binding Path=Desc}" />
                                    </RadioButton.Content>
                                </RadioButton>
                            </ControlTemplate>
                        </Setter.Value>
                    </Setter>
                </Style>
            </Setter.Value>
        </Setter>
    </Style>

    <DataTemplate x:Key="NumericTemplate">
        <DockPanel LastChildFill="False">
            <TextBox DockPanel.Dock="Left" Text="{Binding Path=Reading,Mode=TwoWay,UpdateSourceTrigger=PropertyChanged}" Width="100" x:Name="PART_Textbox" Margin="3,10,0,10"/>
            <TextBlock DockPanel.Dock="Left" Text="{Binding Path=UOM}" Margin="3,10,0,10"/>
            <Button DockPanel.Dock="Right" Visibility="{Binding Path=deleteButtonVisible}" Command="{Binding DeleteMeasDocCommand, RelativeSource={RelativeSource AncestorType={x:Type local:MPointListControl}}}"
                                                                       CommandParameter="{Binding}" Style="{StaticResource {x:Static ToolBar.ButtonStyleKey}}" Padding="0">              
                <Image Width="24" Height="24" VerticalAlignment="Center" Source="/SWB-Controls;Component/Resources/delete.gif"/>
            </Button>
            <Image DockPanel.Dock="Right" Visibility="{Binding Path=FlagVisible}" Width="24" Height="24" VerticalAlignment="Center" Source="/SWB-Controls;Component/Resources/finalflag.gif"  />

        </DockPanel>
    </DataTemplate>

    <DataTemplate x:Key="CodegroupTemplate">
        <DockPanel LastChildFill="False">
            <ListBox DockPanel.Dock="Left" ItemsSource="{Binding Codes}"
                 SelectedItem="{Binding selectedCode,Mode=TwoWay,UpdateSourceTrigger=PropertyChanged}"
              DisplayMemberPath="Desc"
                 Style="{StaticResource RadioButtonListBoxStyle}" Background="Transparent" >
            </ListBox>
            <Button DockPanel.Dock="Right" Visibility="{Binding Path=deleteButtonVisible}" Command="{Binding DeleteMeasDocCommand, RelativeSource={RelativeSource AncestorType={x:Type local:MPointListControl}}}"
                                                                       CommandParameter="{Binding}" Style="{StaticResource {x:Static ToolBar.ButtonStyleKey}}" Padding="0">               
                <Image Width="24" Height="24" VerticalAlignment="Center" Source="/SWB-Controls;Component/Resources/delete.gif" />
            </Button>
            <Image DockPanel.Dock="Right"  Visibility="{Binding Path=FlagVisible}" Width="24" Height="24" VerticalAlignment="Center" Source="/SWB-Controls;Component/Resources/finalflag.gif">
                <Image.ToolTip>
                    <ToolTip Content="{Binding Path=LastWOFlagReading}"></ToolTip>
                </Image.ToolTip>
            </Image>

        </DockPanel>
    </DataTemplate>

    <DataTemplate x:Key="DropdownTemplate">
        <DockPanel LastChildFill="False">
            <ComboBox DockPanel.Dock="Left" Margin="3,10,0,10" MinWidth="100" x:Name="PART_Combo" ItemsSource="{Binding Path=Codes}" DisplayMemberPath="Desc" SelectedItem="{Binding selectedCode,Mode=TwoWay,UpdateSourceTrigger=PropertyChanged}" />
            <Button DockPanel.Dock="Right" Visibility="{Binding Path=deleteButtonVisible}" Command="{Binding DeleteMeasDocCommand, RelativeSource={RelativeSource AncestorType={x:Type local:MPointListControl}}}"
                                                                       CommandParameter="{Binding}" Style="{StaticResource {x:Static ToolBar.ButtonStyleKey}}" Padding="0">               
                <Image Width="24" Height="24" VerticalAlignment="Center" Source="/SWB-Controls;Component/Resources/delete.gif"  />
            </Button>
            <Image DockPanel.Dock="Right" Visibility="{Binding Path=FlagVisible}" Width="24" Height="24" VerticalAlignment="Center" Source="/SWB-Controls;Component/Resources/finalflag.gif"  />

        </DockPanel>
    </DataTemplate>



    <Style TargetType="local:MPointListControl">
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="local:MPointListControl">
                    <Border Background="{TemplateBinding Background}"
                     BorderBrush="{TemplateBinding BorderBrush}"
                     BorderThickness="{TemplateBinding BorderThickness}">
                        <Grid  x:Name="PART_BaseGrid">
                            <Grid.RowDefinitions>
                                <RowDefinition Height="40" />
                                <RowDefinition Height="*" />
                                <RowDefinition Height="40" />
                            </Grid.RowDefinitions>
                            <StackPanel Grid.Row="0" Orientation="Horizontal" Margin="5" Background="Gray" x:Name="PART_Filter">
                                <Label Foreground="White">Filter:</Label>
                                <ComboBox x:Name="PART_Combo" Width="300" ItemsSource="{Binding TechnicalObjects,Mode=TwoWay,UpdateSourceTrigger=PropertyChanged}" SelectedItem="{Binding selectedTO, Mode=TwoWay, UpdateSourceTrigger=Default}" DisplayMemberPath="Description" SelectedValuePath="ObjectID" />
                                <Button x:Name="PART_btnResetFilter" Margin="10,0">
                                    <Button.Style>
                                        <Style TargetType="Button">
                                            <Style.Triggers>
                                                <DataTrigger Binding="{Binding ElementName=PART_Combo, Path=SelectedItem}" Value="{x:Null}">
                                                    <Setter Property="Visibility" Value="Collapsed"></Setter>
                                                </DataTrigger>
                                            </Style.Triggers>
                                        </Style>
                                    </Button.Style>
                                    Alle zeigen
                                </Button>
                            </StackPanel>
                            <DataGrid x:Name="PART_Grid" VirtualizingPanel.IsVirtualizingWhenGrouping="True" VirtualizingPanel.IsVirtualizing="True" AutoGenerateColumns="False" Grid.Row="1" FontSize="16" VerticalAlignment="Top" CanUserAddRows="False" Background="#FFF" AlternationCount="2">
                                <DataGrid.Resources>

                                    <Style TargetType="{x:Type DataGridRow}">
                                        <!--<Setter Property="MinHeight" Value="40"></Setter>-->
                                        <Style.Triggers>
                                            <Trigger Property="ItemsControl.AlternationIndex" Value="0">
                                                <Setter Property="Background" Value="#CCC"></Setter>
                                                <Setter Property="Foreground" Value="#000"/>
                                            </Trigger>
                                            <Trigger Property="ItemsControl.AlternationIndex" Value="1">
                                                <Setter Property="Background" Value="#EEE"></Setter>
                                                <Setter Property="Foreground" Value="#000"/>
                                            </Trigger>

                                        </Style.Triggers>
                                    </Style>
                                    <SolidColorBrush x:Key="{x:Static SystemColors.HighlightBrushKey}"  Color="Yellow"/>
                                    <SolidColorBrush x:Key="{x:Static SystemColors.HighlightTextBrushKey}"  Color="Black"/>
                                </DataGrid.Resources>
                                <DataGrid.Columns>
                                    <DataGridTemplateColumn Header="Bezugsobjekt" Width="250" IsReadOnly="True">
                                        <DataGridTemplateColumn.CellTemplate>
                                            <DataTemplate>
                                                <TextBlock Text="{Binding TechnicalObject.Description}"  FontSize="16">
                                                    <TextBlock.ToolTip>
                                                        <TextBlock Text="{Binding TechnicalObject.DescriptionLong}" />
                                                    </TextBlock.ToolTip>
                                                    </TextBlock>
                                            </DataTemplate>
                                        </DataGridTemplateColumn.CellTemplate>
                                    </DataGridTemplateColumn>
                                    <DataGridTemplateColumn Header="Meßpunkt" Width="300" IsReadOnly="True">
                                        <DataGridTemplateColumn.CellTemplate>
                                            <DataTemplate>
                                                <TextBlock Text="{Binding Description}"  FontSize="16">
                                                    <TextBlock.ToolTip>
                                                        <TextBlock Text="{Binding Description}" />
                                                    </TextBlock.ToolTip>
                                                    </TextBlock>
                                            </DataTemplate>
                                        </DataGridTemplateColumn.CellTemplate>
                                    </DataGridTemplateColumn>
                                    <DataGridTemplateColumn Header="Meßwert" IsReadOnly="False" Width="180">
                                        <DataGridTemplateColumn.CellTemplateSelector>
                                            <local:MPointInputTemplateSelector
                                                 NumericTemplate="{StaticResource NumericTemplate}"
                                                 CodegroupTemplate="{StaticResource CodegroupTemplate}" 
                                                 DropdownTemplate="{StaticResource DropdownTemplate}" >
                                            </local:MPointInputTemplateSelector>
                                        </DataGridTemplateColumn.CellTemplateSelector>
                                    </DataGridTemplateColumn>
                                    <DataGridTemplateColumn Header="" Width="60">
                                        <DataGridTemplateColumn.CellTemplate>
                                            <DataTemplate>
                                                <StackPanel Orientation="Horizontal">
                                                    <TextBlock Visibility="{Binding Path=addDocumentOrShortTextButtonVisible}" Width="30">
                                                            <Hyperlink Command="{Binding NavigateToDocumentCommand, RelativeSource={RelativeSource AncestorType={x:Type local:MPointListControl}}}"
                                                                       CommandParameter="{Binding}" TextDecorations="{x:Null}">
                                                                <Canvas>
                                                                <Image Source="/SWB-Controls;Component/Resources/adddocument.gif" Width="20" Height="20" Visibility="{Binding AddDocumentVisible}" Margin="0" />
                                                                <Image Source="/SWB-Controls;Component/Resources/adddocumentexists.gif" Width="20" Height="20" Visibility="{Binding AddDocumentExistsVisible}" Margin="0"/>
                                                                </Canvas>
                                                                </Hyperlink>
                                                    </TextBlock>
                                                    <TextBlock Visibility="{Binding Path=addDocumentOrShortTextButtonVisible}" Width="30">
                                                            <Hyperlink Command="{Binding NavigateToAddShortTextCommand, RelativeSource={RelativeSource AncestorType={x:Type local:MPointListControl}}}"
                                                                       CommandParameter="{Binding}" TextDecorations="{x:Null}">
                                                                <Canvas>
                                                                <Image Source="/SWB-Controls;Component/Resources/text_new.gif" Width="20" Height="20" Visibility="{Binding AddShorttextVisible}"  Margin="0"  />
                                                                <Image Source="/SWB-Controls;Component/Resources/text_new_alt.gif" Width="20" Height="20" Visibility="{Binding AddShorttextExistsVisible}"  Margin="0" >
                                                                    <Image.ToolTip>
                                                                        <TextBlock Text="{Binding ShorttextAndNotes}" />
                                                                    </Image.ToolTip>
                                                                    </Image>
                                                                 </Canvas>
                                                                </Hyperlink>
                                                        </TextBlock>

                                                </StackPanel>
                                            </DataTemplate>
                                        </DataGridTemplateColumn.CellTemplate>
                                    </DataGridTemplateColumn>
                                    <DataGridTemplateColumn  Header="Letzter Meßwert" Width="180" >
                                        <DataGridTemplateColumn.CellTemplate >
                                            <DataTemplate>
                                                <TextBlock Text="{Binding FormattedPrevReading}"  FontSize="16">
                                                    <TextBlock.ToolTip>
                                                        <TextBlock Text="{Binding FormattedPrevReading}" />
                                                    </TextBlock.ToolTip>
                                                    </TextBlock>
                                            </DataTemplate>
                                        </DataGridTemplateColumn.CellTemplate>
                                    </DataGridTemplateColumn>
                                </DataGrid.Columns>
                                <DataGrid.GroupStyle>
                                    <GroupStyle>
                                        <GroupStyle.HeaderTemplate>
                                            <DataTemplate>
                                                <StackPanel Background="BlanchedAlmond">
                                                    <TextBlock Text="{Binding Path=Name}" Margin="10,3" FontSize="18" FontStyle="Normal" TextDecorations="Underline" FontWeight="Bold" />
                                                </StackPanel>
                                            </DataTemplate>
                                        </GroupStyle.HeaderTemplate>
                                    </GroupStyle>
                                </DataGrid.GroupStyle>
                            </DataGrid>
                            <StackPanel Grid.Row="2" Background="Gray" Margin="5">
                                <Button HorizontalAlignment="Right" Margin="5" Command="{Binding SaveCommand}">Speichern</Button>
                            </StackPanel>
                        </Grid>
                    </Border>

                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>
</ResourceDictionary>
