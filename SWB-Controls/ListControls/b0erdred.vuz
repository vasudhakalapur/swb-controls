﻿using SWB_Controls.BusinessObjects;
using SWB_Controls.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Timers;
using AgentryClientSDK;
using System.Windows.Markup;
using SWB_Controls.Utils;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Threading;

namespace SWB_Controls.ListControls
{
    public class WorkOrderListControl : BaseAgentryCustomControl
    {
        static int MAX_WO_DESCRIPTION_LENGTH = 34;
        static int MAX_OP_DESCRIPTION_LENGTH = 32;
      
        static string selectedWONR = "";
        public static Dictionary<string, ListSortDirection> dictSort = new Dictionary<string, ListSortDirection>();

        static WorkOrderListControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(WorkOrderListControl), new FrameworkPropertyMetadata(typeof(WorkOrderListControl)));
            injectDictionary();
            PureVirtualHandler.RegisterHandler();
        }

        private static void injectDictionary()
        {
            var ResName = "SWB-Controls;component/Resources/Theme.xaml";
            var uri = new Uri(ResName, UriKind.Relative);
            var streamResourceInfo = Application.GetResourceStream(uri);

            using (var resStream = streamResourceInfo.Stream)
            {
                ResourceDictionary myResDic = (ResourceDictionary)XamlReader.Load(resStream);
                Application.Current.Resources.MergedDictionaries.Add(myResDic);
            }
        }

        private System.Timers.Timer refreshTimer;
        private String lastRefreshValue;
        public WorkOrder currentWO;
        public CollectionViewSource cvs;
        public string _filter;
        public string Filter
        {
            get

            {
                return _filter;
            }
            set
            {
                _filter = value;
                cvs.View.Refresh();
            }
        }

        private bool ignoreSelectionChanges = false; //this will be set to true during intialization of grid to  avoid saving wrong selection.
        private WorkOrder _selectedItem;
        public WorkOrder SelectedWO
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                _selectedItem = value;

                if (ignoreSelectionChanges) return;

                if (_selectedItem != null)
                {
                    ((AgentryClientSDK.IAgentryControlViewModelCollectionDisplay)DataContext).SelectItem(value.Index);

                    selectedWONR = _selectedItem.Wonr + _selectedItem.ActiveOperationNum;
                    System.Diagnostics.Debug.WriteLine("storing selectedWONR to" + selectedWONR);
                }
            }
        }

        #region unused stuff
        private void dgTitlesPendingListofTitles_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender != null)
            {
                DataGrid grid = sender as DataGrid;
                if (grid != null && grid.SelectedItems != null && grid.SelectedItems.Count == 1)
                {
                    DataGridRow dgr = grid.ItemContainerGenerator.ContainerFromItem(grid.SelectedItem) as DataGridRow;
                }
            }
        }
        void grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender != null)
            {
                DataGrid dgr = sender as DataGrid;
                currentWO = (WorkOrder)dgr.SelectedItem;
                currentWO.context.ExecuteAgentryAction("WorkOrderView");
            }
        }
        #endregion


		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();

            EnhancedDataGrid grid = GetTemplateChild("WorkOrderList_Grid") as EnhancedDataGrid;
            if (grid != null)
            {
                grid.Loaded += grid_Loaded;
                grid.Sorted += grid_Sorted;
                grid.DataContext = this;
            }

            FrameworkElement filter = GetTemplateChild("PART_Filter") as StackPanel;
            if (filter != null)
            {
                filter.DataContext = this;
            }      
		}

        private void grid_Sorted(object sender, ValueEventArgs<DataGridColumn> e)
        {
            var col = (DataGridColumn)e.Value;
            dictSort.Clear();
            dictSort.Add(col.SortMemberPath, (ListSortDirection)col.SortDirection);
        }   

        private void applySortDescriptions(DataGrid dataGrid){
            dataGrid.Items.SortDescriptions.Clear();
            foreach(DataGridColumn c in dataGrid.Columns){
                if(dictSort.ContainsKey(c.SortMemberPath)){
                    dataGrid.Items.SortDescriptions.Add(new SortDescription(c.SortMemberPath, (ListSortDirection)dictSort[c.SortMemberPath]));                    
                    c.SortDirection = (ListSortDirection)dictSort[c.SortMemberPath];
                }else{
                    c.SortDirection = null;
                }
            }
            dataGrid.Items.Refresh();
        }
        void grid_Loaded(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Grid loaded triggered");
            ignoreSelectionChanges = true;
            List<WorkOrder> workorders = new List<WorkOrder>();
            AgentryClientSDK.IAgentryControlViewModelCollectionDisplay vm = DataContext as AgentryClientSDK.IAgentryControlViewModelCollectionDisplay;
            string counter = vm.GetAgentryString("RefreshCounter");  
            vm.PropertyChanged += dc_PropertyChanged;
            CodeGroup.initCodeGroups(vm);

            int index = 0;
            foreach (AgentryClientSDK.IAgentryData item in vm)
            {
                var wo = 
                new WorkOrder(this) { 
                    context = vm,
                    Wonr = item.PropertyValueByName("WONum"),
                    Arbeitsplatz = item.PropertyValueByName("WorkCenter"),
                    isKapaWO = item.PropertyValueByName("ZOperationUserStatus") == "KAPA",
                    ActiveOperationNum = item.PropertyValueByName("ZActiveOperationNum"),
                    Priority = item.PropertyValueByName("ZCustomPriority"),
                    Techobj = "",
                    TechobjLong = "",
                    Index = index++
                };

                //Set Status based on created Labor entries
                wo.Status = item.PropertyValueByName("Status");
                var laborCollection = item.CollectionByName("Labor");

                int highestStatus = 0; // 1 = Time recorded, 2 = Completed, 3 = Aborted
                for (int i = 0; i < laborCollection.DescendantCount; i++)
                {
                    var labor = laborCollection.Descendant(i);
                    var statusOfLabor = labor.PropertyValueByName("ZStatus");
                    switch (statusOfLabor)
                    {
                        case "01" :
                            if (highestStatus < 1) highestStatus = 1;
                            break;
                        case "02":
                            if (highestStatus < 2) highestStatus = 2;
                            break;
                        case "03":
                            if (highestStatus < 3) highestStatus = 3;
                            break;
                        default:
                            break;
                    }
                }
                if (highestStatus == 1)
                {
                    wo.Status = "InProgress";
                }
                if (highestStatus == 2)
                {
                    wo.Status = "Completed";
                }
                if (highestStatus == 3)
                {
                    wo.Status = "Aborted";
                }


                var activeOperation = findActiveOperation(item, wo);
                var funcLoc = findFuncLoc(item, item.PropertyValueByName("FuncLoc"));
                if (funcLoc != null)
                {
                    var equipLoc = findFuncLoc(item, item.PropertyValueByName("EquipmentID"));
                    wo.Techobj = BuildTechObj(item, funcLoc, wo, equipLoc);
                }
                else
                {
                   funcLoc = findFuncLoc(item, item.PropertyValueByName("EquipmentID"));
                    if (funcLoc != null)
                    {
                        wo.Techobj = BuildTechObj(item, funcLoc, wo);
                    }
                }

                if (activeOperation != null && !wo.isKapaWO)
                {
                    //Add Start-Date only for Workorders that are not KAPA (ie FIX)
                    wo.StartAsDate = activeOperation.PropertyValueAsDateTimeByName("ZFixAppointment");
                }

                wo.Description = AssembleDescription(item, activeOperation, wo);

                workorders.Add(wo);
            }

            EnhancedDataGrid grid = GetTemplateChild("WorkOrderList_Grid") as EnhancedDataGrid;

            cvs = new CollectionViewSource();

            if (dictSort.Keys.Count() == 0)
            {
            //{
            //    List<string> list = new List<string>(dictSort.Keys);
            //    foreach (var item in list)
            //    {
            //        cvs.SortDescriptions.Add(new SortDescription(item, (ListSortDirection)dictSort[item]));
            //    }
            //}
            //else
            //{
                cvs.SortDescriptions.Add(new SortDescription("Start", ListSortDirection.Descending));
                cvs.SortDescriptions.Add(new SortDescription("Priority", ListSortDirection.Ascending));
            }

            cvs.Filter += cvs_Filter;
            cvs.Source = workorders;

            if (dictSort.Keys.Count() == 0)
            {
                ListCollectionView lcw = cvs.View as ListCollectionView;
                lcw.CustomSort = new WorkOrderComparer();
            }
            
            grid.ItemsSource = cvs.View;
            if (dictSort.Keys.Count() > 0)
            {
                applySortDescriptions(grid);
            }
            if (selectedWONR != "")
            {
                var item = workorders.SingleOrDefault(x => x.Wonr + x.ActiveOperationNum == selectedWONR);
                if (item != null)
                {
                    System.Diagnostics.Debug.WriteLine("Restore of selectedWONR from" + selectedWONR);
                    grid.SelectedItem = item;
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("Could not find selectedWONR from" + selectedWONR + "selecting first row instead");
                    grid.SelectedIndex = 0;
                }
            }
            else
            {
                grid.SelectedIndex = 0;
                System.Diagnostics.Debug.WriteLine("No  selectedWONR stored - selecting first row instead");
            }
            ignoreSelectionChanges = false;
            SelectedWO = (WorkOrder)grid.SelectedItem;            
            grid.ScrollIntoView(grid.SelectedItem);
           
        }
      
        void t_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                IAgentryControlViewModelCollectionDisplay dc = null;
                this.Dispatcher.Invoke(new Action(() => dc = this.DataContext as IAgentryControlViewModelCollectionDisplay));
                
                string counter = dc.GetAgentryString("RefreshCounter");
                if (lastRefreshValue != counter)
                {
                    lastRefreshValue = counter;
                    this.Dispatcher.Invoke(new Action(() => { grid_Loaded(null, new RoutedEventArgs()); }));                  
                }
                refreshTimer.Stop();
                refreshTimer = null;
            }
            catch
            {
                //TODO 
                //Do nothing for now.
            }
        }

        void dc_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (refreshTimer == null)
            {
                refreshTimer = new System.Timers.Timer(50);
                refreshTimer.Elapsed += t_Elapsed;
                refreshTimer.Start();
            }
        }
        private string BuildTechObj(AgentryClientSDK.IAgentryData wo, AgentryClientSDK.IAgentryData funcLoc, WorkOrder woObject, AgentryClientSDK.IAgentryData equipLoc)
        {
            string TechObjDesc = funcLoc.PropertyValueByName("ZTechnicalObjectDesc");
            string Street = funcLoc.PropertyValueByName("ZStreet");
            string HouseNo = funcLoc.PropertyValueByName("ZHouseNum");

            if (Street.Length == 0 && HouseNo.Length == 0)
                return BuildTechObj(wo, equipLoc, woObject);
            else
            {
                string longVersion;
                string shortVersion = TechnicalObject.buildTechObjDesc(TechObjDesc, Street, HouseNo, out longVersion);
                woObject.TechobjLong = longVersion;
                return shortVersion;
            }

        }

        private string BuildTechObj(AgentryClientSDK.IAgentryData wo, AgentryClientSDK.IAgentryData funcLoc, WorkOrder woObject)
        {
            string TechObjDesc = funcLoc.PropertyValueByName("ZParentTechnicalObjectDesc");
            string Street = funcLoc.PropertyValueByName("ZStreet");
            string HouseNo = funcLoc.PropertyValueByName("ZHouseNum");

            string longVersion;
            string shortVersion =  TechnicalObject.buildTechObjDesc(TechObjDesc, Street, HouseNo, out longVersion);
            woObject.TechobjLong = longVersion;
            return shortVersion;
                
        }

        private AgentryClientSDK.IAgentryData findFuncLoc(AgentryClientSDK.IAgentryData item, string FuncLoc)
        {
            for (int i = 0; i < item.DescendantCount; i++)
            {
                var descendant = item.Descendant(i);
                if (descendant.InternalName == "ObjectList")
                {
                    if (descendant.DescendantCount > 0)

                        for (int j = 0; j < descendant.DescendantCount; j++)
                        {
                            var funcLoc = descendant.Descendant(j);
                            if (funcLoc.PropertyValueByName("ZTechnicalObject") == FuncLoc)
                            {
                                return funcLoc;
                            }
                        }
                }
            }
            return null;
        }

        private string AssembleDescription(AgentryClientSDK.IAgentryData wo, AgentryClientSDK.IAgentryData activeOperation, WorkOrder woObject)
        {
            string WODescription = wo.PropertyValueByName("Description");
            string OpDescription = activeOperation != null ?  activeOperation.PropertyValueByName("Description") : "";
            string Separator = " | ";
            woObject.DescriptionLong = WODescription + Separator + OpDescription;

            //Case 1 : Both strings longer than MAX:
            if (WODescription.Length > MAX_WO_DESCRIPTION_LENGTH && OpDescription.Length > MAX_OP_DESCRIPTION_LENGTH)
            {
                return WODescription.Substring(0, MAX_WO_DESCRIPTION_LENGTH) + Separator + OpDescription.Substring(0, MAX_OP_DESCRIPTION_LENGTH);
            }
            //Case 2: WO string longer than MAX:
            if (WODescription.Length > MAX_WO_DESCRIPTION_LENGTH)
            {
                int maxLength = MAX_WO_DESCRIPTION_LENGTH + MAX_OP_DESCRIPTION_LENGTH - OpDescription.Length;
                int usedLength = maxLength > WODescription.Length ? WODescription.Length :  maxLength;
                return WODescription.Substring(0, usedLength) + Separator + OpDescription;
            }
            //Case 3: Op description longer than MAX:
            if (OpDescription.Length > MAX_OP_DESCRIPTION_LENGTH)
            {
                int maxLength = MAX_OP_DESCRIPTION_LENGTH + MAX_WO_DESCRIPTION_LENGTH - WODescription.Length;
                int usedLength = maxLength > OpDescription.Length ? OpDescription.Length : maxLength;
                return WODescription + Separator + OpDescription.Substring(0, usedLength);
            }
            //Case 4: Both not longer than MAX:
            return WODescription + Separator + OpDescription;
           
        }

        private AgentryClientSDK.IAgentryData findActiveOperation(AgentryClientSDK.IAgentryData item, WorkOrder wo)
        {
            for (int i = 0; i < item.DescendantCount; i++)
            {
                var descendant = item.Descendant(i);
                if (descendant.InternalName == "Operations")
                {
                    for (int j = 0; j < descendant.DescendantCount; j++)
                    {
                        var desc2 = descendant.Descendant(j);
                        if (desc2.PropertyValueByName("OperationNum") == wo.ActiveOperationNum) //Found Operation that is the current one.
                        {
                            return desc2;
                        }
                    }
                }
            }
            return null;
        }

        void cvs_Filter(object sender, FilterEventArgs e)
        {
            WorkOrder t = e.Item as WorkOrder;
            if (t != null)
            // If filter is turned on, filter completed items.
            {
                if (string.IsNullOrWhiteSpace(_filter)) { e.Accepted = true; return;}

                string fs = _filter.Trim().ToLower();

                if (t.Description.ToLower().Contains(fs) ||
                    t.Start.ToString().ToLower().Contains(fs) ||
                    t.Wonr.ToLower().Contains(fs) ||
                    t.Arbeitsplatz.ToLower().Contains(fs) ||
                    t.Techobj.ToLower().Contains(fs) ||
                    t.Status.ToLower().Contains(fs) 
                    )
                    e.Accepted = true;
                else
                    e.Accepted = false;
            }
        }


    }
}
