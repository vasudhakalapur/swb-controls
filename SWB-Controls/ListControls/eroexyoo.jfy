﻿using SWB_Controls.BusinessObjects;
using SWB_Controls.Extensions;
using SWB_Controls.TextBoxControls;
using SWB_Controls.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace SWB_Controls.ListControls
{
    public class CharValueListControl : BaseAgentryCustomControl
    {
        public AgentryClientSDK.IAgentryControlViewModelCollectionDisplay context { get; set; }
        public DataGrid dataGrid { get; set; }
        public DataGridCell dataGridCell { get; set; }
        public CollectionViewSource cvs;
        public bool isDirty = false;
        public int currentTextLength = 0;
        public bool hasUnsavedChanged
        {
            set { this.SetExtensionString("HasUnsavedChanges", ((value) ? "true" : "")); }
        }
        public string _filter;
        public string Filter
        {
            get
            {
                return _filter;
            }
            set
            {
                _filter = value;
                cvs.View.Refresh();
            }
        }
        private FunctionLocation _selectedFL;
        public FunctionLocation selectedFL
        {
            get
            {
                return _selectedFL;
            }
            set
            {
                _selectedFL = value;
                cvs.View.Refresh();
            }
        }

        public List<FunctionLocation> FunctionLocations { get; set; }
        //public List<CharacteristicValues> ComboValues { get; set; }
        public string SelectedCharValue { get; set; }

        static CharValueListControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CharValueListControl), new FrameworkPropertyMetadata(typeof(CharValueListControl)));
        }

        public override void OnApplyTemplate()
        {

            base.OnApplyTemplate();

            var baseGrid = GetTemplateChild("PART_BaseGrid") as Grid;
            if (baseGrid != null)
            {
                baseGrid.DataContext = this;
            }
            DataGrid grid = GetTemplateChild("PART_Grid") as DataGrid;
            if (grid != null)
            {
                grid.Loaded += grid_Loaded;
            }
        }
        void grid_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                FunctionLocations = new List<FunctionLocation>();

                context = DataContext as AgentryClientSDK.IAgentryControlViewModelCollectionDisplay;
                var index = 0;
                foreach (AgentryClientSDK.IAgentryData item in context)
                {
                    var classificationCollection = item.CollectionByName("Classifications");
                    var objID = item.PropertyValueByName("ObjectID");
                    var objDesc = item.PropertyValueByName("ZTechnicalObjectDesc");
                    if (classificationCollection != null && classificationCollection.DescendantCount > 0)
                    {
                        for (int i = 0; i < classificationCollection.DescendantCount; i++)
                        {
                            FunctionLocation obj = new FunctionLocation()
                            {
                                ObjectID = objID
                            };
                            var crName = classificationCollection.Descendant(i).PropertyValueByName("Name");
                            var crID = classificationCollection.Descendant(i).PropertyValueByName("ID");
                            var characteristicCollection = classificationCollection.Descendant(i).CollectionByName("Characteristics");
                            obj.ID = crID;
                            obj.Description = FunctionLocation.buildFunctionLocDesc(objDesc, crName);
                            for (int j = 0; j < characteristicCollection.DescendantCount; j++)
                            {
                                Characteristic c = new Characteristic(obj);
                                c.ObjectID = objID;
                                c.ClassificationID = crID;
                                c.CharacteristicUniqueID = characteristicCollection.Descendant(j).PropertyValueByName("ID");
                                c.Name = characteristicCollection.Descendant(j).PropertyValueByName("Description");
                                c.IsReadOnly = characteristicCollection.Descendant(j).PropertyValueAsBool("ZIsReadOnly");
                                c.ZNumChar = characteristicCollection.Descendant(j).PropertyValueByName("ZNumChar");
                                c.ZNumDec = characteristicCollection.Descendant(j).PropertyValueByName("ZNumDec");
                                c.DataType = characteristicCollection.Descendant(j).PropertyValueByName("DataType");
                                c.ItemNumber = Convert.ToInt32(characteristicCollection.Descendant(j).PropertyValueByName("ItemNumber"));
                                var valueCollection = characteristicCollection.Descendant(j).CollectionByName("CharacteristicValues");
                                var possibleValues = characteristicCollection.Descendant(j).CollectionByName("ZPossibleCharacteristicValues");
                                if (valueCollection != null && valueCollection.DescendantCount > 0)
                                {
                                    if (valueCollection.DescendantCount == 1)
                                    {
                                        c.Value = valueCollection.Descendant(0).PropertyValueByName("Value");
                                        c.CharacteristicValueID = valueCollection.Descendant(0).PropertyValueByName("ID");
                                    }
                                    else
                                    {
                                        for (int k = 0; k < valueCollection.DescendantCount; k++)
                                        {
                                            var isLocalValue = valueCollection.Descendant(k).PropertyValueAsBool("ZIsLocalValue");
                                            if (isLocalValue)
                                            {
                                                c.Value = valueCollection.Descendant(k).PropertyValueByName("Value");
                                                c.CharacteristicValueID = valueCollection.Descendant(k).PropertyValueByName("ID");
                                                break;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    c.Value = string.Empty;
                                    c.CharacteristicValueID = string.Empty;
                                }
                                if (possibleValues != null && possibleValues.DescendantCount > 0)
                                {
                                    for (int l = 0; l < possibleValues.DescendantCount; l++)
                                    {
                                        var id = possibleValues.Descendant(l).PropertyValueByName("ID");
                                        var value = possibleValues.Descendant(l).PropertyValueByName("Value");
                                        if (value == c.Value)
                                        {
                                            c.PossibleValues.Insert(0, new CharacteristicValues { ID = id, Value = value });
                                        }
                                        else
                                        {
                                            c.PossibleValues.Add(new CharacteristicValues { ID = id, Value = value });
                                        }
                                    }
                                }
                                c.ID = index++;
                                if (c.Value.Length > 0 && c.PossibleValues.Count > 0)
                                {
                                    c.SelectedCharvalue = c.PossibleValues.FirstOrDefault(x => x.Value == c.Value);
                                }
                                c.isModified = false;
                                if (c.DataType == "TIME")
                                {
                                     c.Value = (c.Value.Length > 0) ? String.Format("{0}:{1}", c.Value.Substring(0, 2), c.Value.Substring(2, 2)) : "00:00";
                                }
                                if (c.DataType == "DATE")
                                {
                                    if (c.Value.Length > 0 && c.Value.IndexOf(".") < 0)
                                    {
                                        c.Value = DateTime.ParseExact(c.Value.Substring(0, 8), "yyyyMMdd", CultureInfo.InvariantCulture).ToShortDateString();
                                    }
                                }
                                obj.characteristics.Add(c);
                            }

                            if (obj.characteristics.Any())
                            {
                                FunctionLocations.Add(obj);
                            }
                        }
                    }
                }
                dataGrid = GetTemplateChild("PART_Grid") as DataGrid;
                cvs = new CollectionViewSource();
                cvs.Filter += cvs_Filter;
                cvs.Source = FunctionLocations.SelectMany(c => c.characteristics).OrderBy(x => x.ItemNumber).ToList();

                ListCollectionView lcw = cvs.View as ListCollectionView;
                dataGrid.ItemsSource = cvs.View;
                dataGrid.Items.MoveCurrentToFirst();
                dataGrid.UpdateLayout();
                dataGrid.ScrollIntoView(dataGrid.SelectedItem);
                hasUnsavedChanged = false;
                var btnResetFilter = GetTemplateChild("PART_btnResetFilter") as Button;
                btnResetFilter.Click += btnResetFilter_Click;
                firePropertyChanged("FunctionLocations");
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex);
            }
        }

        private void txt_TextChanged(object sender, TextChangedEventArgs e)
        {
            var item = (Characteristic)dataGrid.SelectedItem;
            var btnSave = GetTemplateChild("PART_btnSave") as Button;
            btnSave.IsEnabled = true;
            item.isModified = true;
            hasUnsavedChanged = true;           
        }

        private void txt_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox txt = (TextBox)sender;
            var item = (Characteristic)dataGrid.SelectedItem;
            item.Value = txt.Text;                     
        }
        private void txt_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var item = (Characteristic)dataGrid.SelectedItem;
            var allowedDecimal = Convert.ToInt32(item.ZNumDec);
            var dataType = item.DataType;
            if (allowedDecimal > 0)
            {
                var txt = (sender as TextBox).Text;
                var totalLength = Convert.ToInt32(item.ZNumChar);

                if (!StringHelper.IsNumericTextAllowed(e.Text))
                {
                    if (currentTextLength > 0)
                    {
                        txt = txt + e.Text;
                        if (txt.IndexOf(",") > 0)
                        {
                            var separatorCount = txt.Count(x => x == ',');
                            if (separatorCount < 2)
                            {
                                var split = txt.Split(',');
                                e.Handled = (split[0].Length > (totalLength - allowedDecimal)) || split[1].Length > allowedDecimal;
                            }
                            else
                                e.Handled = true;
                        }
                        else
                        {
                            e.Handled = txt.Length > (totalLength - allowedDecimal);
                        }
                    }
                    currentTextLength++;
                }

            }
            else
            {
                e.Handled = (dataType.ToUpper() == "NUM") ? StringHelper.IsNumericTextAllowed(e.Text) : false;
            }
        }
        private void cmbPossibleValues_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dataGrid = GetTemplateChild("PART_Grid") as DataGrid;
            var item = (Characteristic)dataGrid.SelectedItem;
            item.Value = ((CharacteristicValues)((sender as ComboBox).SelectedItem)).Value;
            var btnSave = GetTemplateChild("PART_btnSave") as Button;
            btnSave.IsEnabled = true;
            item.isModified = true;
            hasUnsavedChanged = true;
            //dataGrid.CommitEdit();
        }
        private void date_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            dataGrid = GetTemplateChild("PART_Grid") as DataGrid;
            var item = (Characteristic)dataGrid.SelectedItem;
            item.Value = (sender as DatePicker).SelectedDate.Value.ToString("dd.MM.yyy");
            var btnSave = GetTemplateChild("PART_btnSave") as Button;
            btnSave.IsEnabled = true;
            item.isModified = true;
            hasUnsavedChanged = true;
            //dataGrid.CommitEdit();
        }
        private void masktext_LostFocus(object sender, RoutedEventArgs e)
        {
            var maskedTimeTextBox = (sender as MaskedTextBox);
            dataGrid = GetTemplateChild("PART_Grid") as DataGrid;
            var item = (Characteristic)dataGrid.SelectedItem;
            item.Value = maskedTimeTextBox.Text.Replace("_", "");
            var btnSave = GetTemplateChild("PART_btnSave") as Button;
            btnSave.IsEnabled = true;
            item.isModified = true;
            hasUnsavedChanged = true;
            //dataGrid.CommitEdit();            
        }
        private void masktext_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var isNumeric = StringHelper.IsNumericTextAllowed(e.Text);
            if (!isNumeric)
            {
                var txt = (sender as TextBox).Text;
                txt = txt.Replace("_", "").Replace(":", "") + e.Text;
                if (txt.Length == 3)
                {
                    e.Handled = Convert.ToInt32(e.Text) > 5;
                }
                else if (txt.Length == 2)
                {
                    if (txt[0] == '2')
                    {
                        e.Handled = Convert.ToInt32(e.Text) > 3;
                    }
                }

            }
            else
            {
                e.Handled = true;
            }
        }
        private void btnResetFilter_Click(object sender, RoutedEventArgs e)
        {
            dataGrid = GetTemplateChild("PART_Grid") as DataGrid;
            cvs = new CollectionViewSource();
            cvs.Source = FunctionLocations.SelectMany(c => c.characteristics).ToList();
            ListCollectionView lcw = cvs.View as ListCollectionView;
            dataGrid.ItemsSource = cvs.View;
            var cmb = GetTemplateChild("PART_Combo") as ComboBox;
            cvs.Filter += cvs_Filter;
            cmb.SelectedValue = -1;
        }
        void cvs_Filter(object sender, FilterEventArgs e)
        {
            Characteristic t = e.Item as Characteristic;
            if (t != null)
            // If filter is turned on, filter completed items.
            {
                if (selectedFL == null) { e.Accepted = true; return; }
                if (t.FunctionLoc == selectedFL)
                    e.Accepted = true;
                else
                    e.Accepted = false;
            }
        }
        public ICommand SaveCommand
        {
            get
            {
                return new CommandHandler(saveCharValue, true);
            }
        }
        public ICommand EditCommand
        {
            get
            {
                return new CommandHandler(editCharValue, true);
            }
        }

        private void editCharValue(object obj)
        {
            dataGrid = GetTemplateChild("PART_Grid") as DataGrid;
            dataGrid.UpdateLayout();
            dataGrid.ScrollIntoView(dataGrid.SelectedItem);
            dataGrid.BeginEdit();
            foreach (var txt in DataGridHelper.FindVisualChild<TextBox>(dataGrid, "PART_Textbox"))
            {
                txt.TextChanged += txt_TextChanged;
                txt.LostFocus += txt_LostFocus;
                txt.PreviewTextInput += txt_PreviewTextInput;
                var item = (Characteristic)dataGrid.SelectedItem;
                var allowedDecimal = Convert.ToInt32(item.ZNumDec);
                var dataType = item.DataType;
                if (allowedDecimal > 0)
                {
                    txt.MaxLength = txt.MaxLength + 1;
                }
                txt.IsEnabled = true;
            }
            foreach (var combo in DataGridHelper.FindVisualChild<ComboBox>(dataGrid, "PART_cmbPossibleValues"))
            {
                combo.SelectionChanged += cmbPossibleValues_SelectionChanged;
                combo.IsEnabled = true;
            }
            foreach (var date in DataGridHelper.FindVisualChild<DatePicker>(dataGrid, "PART_Date"))
            {
                date.SelectedDateChanged += date_SelectedDateChanged;
                date.IsEnabled = true;
            }
            foreach (var masktext in DataGridHelper.FindVisualChild<MaskedTextBox>(dataGrid, "PART_maskedTimeTextBox"))
            {
                masktext.LostFocus += masktext_LostFocus;
                masktext.PreviewTextInput += masktext_PreviewTextInput;
                masktext.IsEnabled = true;
            }
        }

        private void saveCharValue(object target)
        {
            var CharValueToSave = FunctionLocations.SelectMany(c => c.characteristics).Where(d => d.isModified);
            foreach (var characteristic in CharValueToSave)
            {
                this.SetExtensionString("ObjectID", characteristic.ObjectID);
                this.SetExtensionString("ClassificationID ", characteristic.ClassificationID);
                this.SetExtensionString("CharacteristicUniqueID", characteristic.CharacteristicUniqueID);
                var value = characteristic.Value;
                if (value.Length > 0)
                {
                    if (Convert.ToInt32(characteristic.ZNumDec) > 0)
                    {
                        value = value.Replace(",", ".");
                    }
                    else if (characteristic.DataType == "TIME")
                    {
                        value = value.Replace(":", "") + "00";
                    }
                }
                this.SetExtensionString("Value", value);
                context.ExecuteAgentryAction("ZCharacteristicValueAdd");
            }
            hasUnsavedChanged = false;
            var btnSave = GetTemplateChild("PART_btnSave") as Button;
            btnSave.IsEnabled = false;
        }

    }
}
