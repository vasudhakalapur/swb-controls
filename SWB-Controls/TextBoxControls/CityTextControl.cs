﻿using SWB_Controls.ButtonControls;
using SWB_Controls.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SWB_Controls.TextBoxControls
{
    public class CityTextControl : BaseAgentryCustomControl
    {
        public static AgentryClientSDK.IAgentryControlViewModelStringDisplay context { get; set; }

        public static TextBox txtControl {get; set;}        

        public static string CityTextControlValue { get; set; }

        static CityTextControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CityTextControl), new FrameworkPropertyMetadata(typeof(CityTextControl)));
            PureVirtualHandler.RegisterHandler();
        }
        public void SetAgentryValue()
        {
            context = DataContext as AgentryClientSDK.IAgentryControlViewModelStringDisplay;
            
            txtControl = GetTemplateChild("PART_Textbox") as TextBox;
            txtControl.Text = context.GetAgentryString("ZCity");
            context.StringValue = txtControl.Text;
            CityTextControlValue = txtControl.Text;
            txtControl.LostFocus += textbox_LostFocus;
        }

        private void textbox_LostFocus(object sender, RoutedEventArgs e)
        {
            var textbox = sender as TextBox;
            context.StringValue = textbox.Text;
            if (AddressButtonControl.IsAddressSelected != null && AddressButtonControl.IsAddressSelected == false)            
                CityTextControlValue = textbox.Text;
        }
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            SetAgentryValue();
        }
    }
}
