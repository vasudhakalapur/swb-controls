﻿using SWB_Controls.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using SWB_Controls.DropdownControls;


namespace SWB_Controls.TextBoxControls
{
    public class RepairTextControl40Chars : BaseAgentryCustomControl
    {
        public AgentryClientSDK.IAgentryControlViewModelStringDisplay context { get; set; }
        public static TextBox textbox { get; set; }

        static RepairTextControl40Chars()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(RepairTextControl40Chars), new FrameworkPropertyMetadata(typeof(RepairTextControl40Chars)));
            PureVirtualHandler.RegisterHandler();
        }
        public void SetAgentryValue()
        {
            context = DataContext as AgentryClientSDK.IAgentryControlViewModelStringDisplay;
            textbox = GetTemplateChild("PART_Textbox") as TextBox;
          
            textbox.Text = context.StringValue;
            textbox.LostFocus += textbox_LostFocus;
            if (RepairHoursDropdownControl.cmbHours.SelectedIndex == 0 && Convert.ToInt32(RepairMinutesDropdownControl.txtMinutes.Text) == 0)
                textbox.IsEnabled = false;
        }

        private void textbox_LostFocus(object sender, RoutedEventArgs e)
        {
            var textbox = sender as TextBox;
            context.StringValue = textbox.Text;
        }
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            SetAgentryValue();
        }
    }
}
