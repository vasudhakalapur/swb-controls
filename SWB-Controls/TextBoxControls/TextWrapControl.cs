﻿using SWB_Controls.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SWB_Controls.TextBoxControls
{
    public class TextWrapControl : BaseAgentryCustomControl
    {
        public AgentryClientSDK.IAgentryControlViewModelStringDisplay context { get; set; }
        static TextWrapControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TextWrapControl), new FrameworkPropertyMetadata(typeof(TextWrapControl)));
            PureVirtualHandler.RegisterHandler();
        }
        public void SetAgentryValue()
        {
            context = DataContext as AgentryClientSDK.IAgentryControlViewModelStringDisplay;
            TextBox tbMultiLine = GetTemplateChild("PART_tbMultiLine") as TextBox;
            tbMultiLine.Text = context.GetAgentryString("NewNotes");
            context.StringValue = tbMultiLine.Text;
            tbMultiLine.LostFocus += tbMultiLine_LostFocus;
        }

        private void tbMultiLine_LostFocus(object sender, RoutedEventArgs e)
        {
            var tbMultiLine = sender as TextBox;
            context.StringValue = tbMultiLine.Text;
        }
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            SetAgentryValue();
        }
    }
}
