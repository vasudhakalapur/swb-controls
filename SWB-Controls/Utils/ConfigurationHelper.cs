﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWB_Controls.Utils
{
    public class ConfigurationHelper
    {
        public static string GetConfigFile
        {
            get { return Environment.CurrentDirectory + "\\CustomControls\\App.config"; }
        }
        public static Configuration GetConfig
        {
            get
            {
                ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
                fileMap.ExeConfigFilename = GetConfigFile;
                return ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
            }
        }
        public static KeyValueConfigurationCollection GetConfigSettings
        {
            get
            {
                return GetConfig.AppSettings.Settings;
            }
        }
        public static void SaveConfig(Configuration config)
        {
            config.Save();
        }
    }
}
