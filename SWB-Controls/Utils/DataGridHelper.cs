﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace SWB_Controls.Utils
{
    public static class DataGridHelper
    {
        #region Rows
        //Gets the DataGridRow based on given index
        public static DataGridRow GetRow(DataGrid dataGrid, int index)
        {
            DataGridRow row = (DataGridRow)dataGrid.ItemContainerGenerator.ContainerFromIndex(index);
            if (row == null)
            {
                dataGrid.ScrollIntoView(dataGrid.Items[index]);
                row = (DataGridRow)dataGrid.ItemContainerGenerator.ContainerFromIndex(index);
            }
            return row;
        } 
      

        public static IEnumerable<DataGridRow> GetRows(DataGrid dataGrid)
        {
            var itemSource = dataGrid.ItemsSource;
            if (null == itemSource) yield return null;
            foreach (var item in itemSource)
            {
                var row = dataGrid.ItemContainerGenerator.ContainerFromItem(item) as DataGridRow;
                if (row != null) yield return row;
                else
                {
                    dataGrid.UpdateLayout();
                    dataGrid.ScrollIntoView(item);
                    row = dataGrid.ItemContainerGenerator.ContainerFromItem(item) as DataGridRow;
                    if (row != null) yield return row;
                }
            }
        }

        public static DataGridRow GetEditingRow(DataGrid dataGrid)
        {
            var sIndex = dataGrid.SelectedIndex;
            if (sIndex >= 0)
            {
                var selected =(DataGridRow) dataGrid.ItemContainerGenerator.ContainerFromIndex(sIndex);
                if (selected.IsEditing) return selected;
            }

            for (int i = 0; i < dataGrid.Items.Count; i++)
            {
                if (i == sIndex) continue;
                var item = (DataGridRow)dataGrid.ItemContainerGenerator.ContainerFromIndex(i);
                if (item.IsEditing) return item;
            }
            return null;
        }
        #endregion

        #region Cells
        public static DataGridCell GetCell(DataGrid dataGrid, int row, int column)
        {
            DataGridRow rowContainer = GetRow(dataGrid, row);
            if (rowContainer != null)
            {
                DataGridCellsPresenter presenter = GetVisualChild<DataGridCellsPresenter>(rowContainer);
                DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(column);
                if (cell == null)
                {
                    dataGrid.ScrollIntoView(rowContainer, dataGrid.Columns[column]);
                    cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(column);
                }
                return cell;
            }
            return null;
        }
#endregion

        #region GetRowHeader
        public static DataGridRowHeader GetRowHeader(DataGrid dataGrid, int index)
        {
            return GetRowHeader(GetRow(dataGrid, index));
        }
        public static DataGridRowHeader GetRowHeader(DataGridRow row)
        {
            if (row != null)
            {
                return GetVisualChild<DataGridRowHeader>(row);
            }
            return null;
        }
        #endregion

        #region GetColumnHeader
        public static DataGridColumnHeader GetColumnHeader(DataGrid dataGrid, int index)
        {
            DataGridColumnHeadersPresenter presenter = GetVisualChild<DataGridColumnHeadersPresenter>(dataGrid);
            if (presenter != null)
            {
                return (DataGridColumnHeader)presenter.ItemContainerGenerator.ContainerFromIndex(index);
            }
            return null;
        }
        #endregion

        public static bool IsEditing(DataGrid dataGrid)
        {
            return GetEditingRow(dataGrid) != null;
        }

        public static childItem FindVisualChild<childItem>(DependencyObject obj) where childItem : DependencyObject
        {
            for (int i = 0; i < System.Windows.Media.VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = System.Windows.Media.VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is childItem)
                    return (childItem)child;
                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }
        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject obj) where T : DependencyObject
        {
            if (obj != null)
            {
                for (int i = 0; i < System.Windows.Media.VisualTreeHelper.GetChildrenCount(obj); i++)
                { 
                    DependencyObject child = System.Windows.Media.VisualTreeHelper.GetChild(obj, i);
                    if (child != null && child is T)
                        yield return (T)child;
                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }
        public static IEnumerable<T> FindVisualChild<T>(DependencyObject obj, string childName) where T : DependencyObject
        {
            if (obj != null)
            {
                for (int i = 0; i < System.Windows.Media.VisualTreeHelper.GetChildrenCount(obj); i++)
                {
                    DependencyObject child = System.Windows.Media.VisualTreeHelper.GetChild(obj, i);
                    if (child != null && child is T)
                        yield return (T)child;
                    foreach (T childOfChild in FindVisualChild<T>(child, childName))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }
        public static T FindVisualChildren<T>(DependencyObject obj, string childName) where T : DependencyObject
        {
            if (obj != null)
            {
                T foundChild = null;
                for (int i = 0; i < System.Windows.Media.VisualTreeHelper.GetChildrenCount(obj); i++)
                {
                    var child = System.Windows.Media.VisualTreeHelper.GetChild(obj, i);
                    T childType = child as T;
                    if (childType == null)
                    {
                        foundChild = FindVisualChildren<T>(child, childName);
                        if (foundChild != null) break;
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(childName))
                        {
                            var frameworkElement = child as FrameworkElement;
                            if (frameworkElement != null && frameworkElement.Name == childName)
                            {
                                foundChild = (T)child;
                                break;
                            }
                            else
                            {
                                foundChild = FindVisualChildren<T>(child, childName);
                                if (foundChild != null) break;
                            }
                        }
                        else
                        {
                            foundChild = FindVisualChildren<T>(child, childName);
                            if (foundChild != null) break;
                        }
                    }                   
                }
                return foundChild;
            }
            return null;
        }

        #region GetVisualChild

        public static T GetVisualChild<T>(Visual parent) where T : Visual
        {
            T child = default(T);

            int numVisuals = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < numVisuals; i++)
            {
                Visual v = (Visual)VisualTreeHelper.GetChild(parent, i);
                child = v as T;
                if (child == null)
                {
                    child = GetVisualChild<T>(v);
                }
                if (child != null)
                {
                    break;
                }
            }

            return child;
        }

        #endregion GetVisualChild

        #region FindVisualParent

        public static T FindVisualParent<T>(UIElement element) where T : UIElement
        {
            UIElement parent = element;
            while (parent != null)
            {
                T correctlyTyped = parent as T;
                if (correctlyTyped != null)
                {
                    return correctlyTyped;
                }

                parent = VisualTreeHelper.GetParent(parent) as UIElement;
            }

            return null;
        }

        #endregion FindVisualParent

        #region FindPartByName

        public static DependencyObject FindPartByName(DependencyObject ele, string name)
        {
            DependencyObject result;
            if (ele == null)
            {
                return null;
            }
            if (name.Equals(ele.GetValue(FrameworkElement.NameProperty)))
            {
                return ele;
            }

            int numVisuals = VisualTreeHelper.GetChildrenCount(ele);
            for (int i = 0; i < numVisuals; i++)
            {
                DependencyObject vis = VisualTreeHelper.GetChild(ele, i);
                if ((result = FindPartByName(vis, name)) != null)
                {
                    return result;
                }
            }
            return null;
        }

        #endregion FindPartByName
    }
}
