﻿using SWB_Controls.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SWB_Controls.BusinessObjects
{
    public class LaborTimeDetails
    {
        public int Index { get; set; }
        public string ID { get; set; }
        public string Status { get; set; }
        public string WorkDate { get; set; }
        public string PersonNr { get; set; }
        public string _woTime { get; set; }
        public string WorkTime
        {
            get
            {
                return StringHelper.BuildTime(_woTime);
            }
            set { _woTime = value; }
        }
        public string Wonr { get; set; }
        public string WoComment { get; set; }
        public string MiscComment { get; set; }
        public bool IsAdditionalWorkDone { get; set; }
        public Visibility ClockVisible { get { return (IsAdditionalWorkDone) ? Visibility.Visible : Visibility.Collapsed; } }
        public bool IsLocal { get; set; }
        public Visibility DeleteButtonVisible { get { return (IsLocal) ? Visibility.Visible : Visibility.Collapsed; } }

    }
}
