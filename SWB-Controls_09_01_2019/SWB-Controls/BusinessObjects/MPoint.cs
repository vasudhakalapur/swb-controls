﻿using AgentryClientSDK;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using SWB_Controls.Extensions;

namespace SWB_Controls.BusinessObjects
{
    public class MPoint : INotifyPropertyChanged
    {
        public IAgentryData agentrydata { get; set; }

        public bool IsCodeSufficient { get; set; }
        public string Catalog { get; set; }
        public string CodeGroup { get; set; }
        public string Description { get; set; }
        public string ID { get; set; }
        public string PrevCodeDesc { get; set; }
        public string PrevReadingDate { get; set; }
        public string UOM { get; set; }
        public bool isModified { get; set; }
        public bool isPreviousReading { get; set; }
        public bool hasPendingMeasDoc { get; set; } //if MeasDoc is pending, it can be deleted.
        public Visibility deleteButtonVisible { get { return hasPendingMeasDoc ? Visibility.Visible : Visibility.Collapsed; } }
        public Visibility FlagVisible { get { return isPreviousReading ? Visibility.Visible : Visibility.Collapsed; } }
        public Visibility addDocumentOrShortTextButtonVisible { get { return hasPendingMeasDoc | isModified ? Visibility.Visible : Visibility.Collapsed; } }
        public Visibility AddDocumentExistsVisible { get { return isDocumentAttached ? Visibility.Visible : Visibility.Collapsed; } }
        public Visibility AddDocumentVisible { get { return !isDocumentAttached ? Visibility.Visible : Visibility.Collapsed; } }
        public Visibility AddShorttextExistsVisible { get { return hasShorttext | hasNotes ? Visibility.Visible : Visibility.Collapsed; } }
        public Visibility AddShorttextVisible { get { return !(hasShorttext | hasNotes) ? Visibility.Visible : Visibility.Collapsed; } }

        public string FlagPrevCodeDesc { get; set; }
        public string FlagPrevReadingDate { get; set; }
        public bool IsLowerRange { get; set; }
        public bool IsUpperRange { get; set; }
        public decimal LowerRange { get; set; }
        public decimal UpperRange { get; set; }

        public string Shorttext { get; set; }
        public string Notes { get; set; }

        public string ShorttextAndNotes { get { return String.Format("{0}\r\n\r\n{1}", Shorttext, (Notes.Length > 40 ? Notes.Substring(0,40) + "..." : Notes)); } }

        public bool hasShorttext { get { return !String.IsNullOrEmpty(Shorttext); } }
        public bool hasNotes { get { return !String.IsNullOrEmpty(Notes); } }

        public bool isDocumentAttached { get; set; }
        public bool isDefaultValueTextbox { get; set; }

        private string reading;
        public string Reading
        {
            get
            {
                return reading;
            }
            set
            {
                reading = value;
                isModified = true;
                firePropertChangedEvent("addDocumentOrShortTextButtonVisible");
            }
        }
        public string ValuationCode { get; set; }
        public DateTime ReadingDate { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public void firePropertChangedEvent(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(property));
            }
        }

        public string FormattedPrevReading
        {
            get
            {
                if (!String.IsNullOrWhiteSpace(PrevCodeDesc)) return String.Format("{0} ({1})", PrevCodeDesc, PrevReadingDate);
                return String.Empty;
            }
        }
        public string FormattedFlagPrevReading
        {
            get
            {
                if (!String.IsNullOrWhiteSpace(FlagPrevCodeDesc)) return String.Format("{0} : {1}", FlagPrevReadingDate, FlagPrevCodeDesc);
                return String.Empty;
            }
        }
        public void refreshShorttextandDocumentProperties()
        {
            Shorttext = agentrydata.PropertyValueByName("ReadingShortText");
            Notes = agentrydata.PropertyValueByName("Notes");
            isDocumentAttached = agentrydata.PropertyValueAsBool("ZIsDocumentAttached");

            if (Shorttext.Length > 0)
            {
                Error = string.Empty;
                firePropertChangedEvent("Error");
            }

            firePropertChangedEvent("Shorttext");
            firePropertChangedEvent("Notes");
            firePropertChangedEvent("AddDocumentVisible");
            firePropertChangedEvent("AddDocumentExistsVisible");
            firePropertChangedEvent("AddShorttextVisible");
            firePropertChangedEvent("AddShorttextExistsVisible");

        }

        public void refreshIsReadProperties()
        {
            IsRead = agentrydata.PropertyValueAsBool("ZIsRead");
            firePropertChangedEvent("IsRead");
            hasPendingMeasDoc = IsRead;
            firePropertChangedEvent("deleteButtonVisible");
            firePropertChangedEvent("addDocumentOrShortTextButtonVisible");
        }

        public TechnicalObject TechnicalObject { get; set; }

        private ObservableCollection<CodeGroup> codes;
        public ObservableCollection<CodeGroup> Codes
        {
            get
            {
                if (codes == null)
                {
                    try
                    {
                        var codeTemplate = BusinessObjects.CodeGroup.CodesByCatalogAndCodeGrp.Where(c => c.Key == String.Format("{0}-{1}", Catalog, CodeGroup)).FirstOrDefault();
                        var codes2 = codeTemplate != null ? codeTemplate.Select(c => c.CopyForMPoint(this)) : null;
                        codes = codes2 != null ? new ObservableCollection<CodeGroup>(codes2) : null;
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine("Error retrieivng codes for MPoint" + e);
                        return new ObservableCollection<CodeGroup>();
                    }
                }
                return codes;

            }
        }

        private CodeGroup _selectedCode;

        public string DefaultValue { get; set; }

        public CodeGroup selectedCode
        {
            get
            {
                return _selectedCode;
            }
            set
            {
                this.isModified = true;
                _selectedCode = value;
                ValuationCode = _selectedCode.Code;
                firePropertChangedEvent("addDocumentOrShortTextButtonVisible");
            }
        }

        public bool IsRead { get; set; }

        public string Error { get; set; }

        public MPoint(TechnicalObject to)
        {
            TechnicalObject = to;
        }
        internal void resetReading()
        {
            if (!String.IsNullOrEmpty(DefaultValue))
            {
                Reading = DefaultValue;
                ValuationCode = agentrydata.PropertyValueByName("ValuationCode");

                //Set code in case ValuationCode-Field is set:
                if (!String.IsNullOrEmpty(ValuationCode))
                {
                    var defmp = Codes.Where(c => c.Code == ValuationCode).FirstOrDefault();
                    if (defmp != null)
                    {
                        selectedCode = Codes.Where(c => c.Code == ValuationCode).FirstOrDefault();
                    }
                }
                hasPendingMeasDoc = false;
            }
            else
            {
                Reading = "";
                _selectedCode = null;
                hasPendingMeasDoc = false;
                isModified = false;
                firePropertChangedEvent("addDocumentOrShortTextButtonVisible");
            }
            ShowError(false);
            firePropertChangedEvent("deleteButtonVisible");
            firePropertChangedEvent("Reading");
            firePropertChangedEvent("selectedCode");
        }

        internal void markAsMeasDocSaved()
        {
            hasPendingMeasDoc = IsRead;
            firePropertChangedEvent("deleteButtonVisible");
            firePropertChangedEvent("addDocumentOrShortTextButtonVisible");
        }

        internal void ShowError(bool isVisible)
        {
            Error = isVisible ? "Geben Sie einen Kurztext" : string.Empty;
            firePropertChangedEvent("Error");
        }
    }
}
