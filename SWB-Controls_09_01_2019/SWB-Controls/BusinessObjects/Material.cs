﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWB_Controls.BusinessObjects
{
    public class Material
    {
        public int Index { get; set; }
        public string Number { get; set; }
        public string Quantity { get; set; }
        public string UOM { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
    }
}
