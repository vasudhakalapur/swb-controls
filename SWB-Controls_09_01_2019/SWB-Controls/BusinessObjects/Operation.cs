﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SWB_Controls.BusinessObjects
{
    public class Operation
    {
        public int Index { get; set; }
        public string Number { get; set; }
        public string WorkCenter { get; set; }
        public string Description { get; set; }
        public string PersName { get; set; }
        public bool IsPersSupervisor { get; set; }
        public Visibility ShowHat { get { return IsPersSupervisor ? Visibility.Visible : Visibility.Collapsed; } }
        public string TelNum { get; set; }
    }
}
