﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWB_Controls.BusinessObjects
{
    public class TechnicalObject
    {
        static int MAX_TP_DESCRIPTION_LENGTH = 16;
        static int MAX_STREET_ADRESS_LENGTH = 9;
        static int MAX_HOUSNO_LENGTH = 3;
        public string TechObjDesc { get; set; }
        public string Description { get { return buildTechObjDesc(TechObjDesc, Street, HouseNo, SortField, ParentDescription); } }

        public string ParentDescription { get; set; }
        public string Street { get; set; }
        public string HouseNo { get; set; }
        public string SortField { get; set; }
        public string DescriptionLong { get; set; }

        public string FullDescription { get { return Description + " - " + DescriptionLong; } }

        public string ID { get; set; }
        public String ObjectID { get; set; }
        public String ObjectType { get; set; }
        public List<MPoint> mpoints = new List<MPoint>();

        public static string buildTechObjDesc(string TechObjDesc, string Street, string HouseNo, out string longVersion)
        {
            longVersion = String.Format("{0} {1} {2}", TechObjDesc, Street, HouseNo);


            return String.Format("{0} | {1} {2}",
                TechObjDesc.Substring(0, TechObjDesc.Length > MAX_TP_DESCRIPTION_LENGTH ? MAX_TP_DESCRIPTION_LENGTH : TechObjDesc.Length),
                Street.Substring(0, Street.Length > MAX_STREET_ADRESS_LENGTH ? MAX_STREET_ADRESS_LENGTH : Street.Length),
                HouseNo.Substring(0, HouseNo.Length > MAX_HOUSNO_LENGTH ? MAX_HOUSNO_LENGTH : HouseNo.Length)
                );
        }

        public static string buildTechObjDesc(string TechObjDesc, string Street, string HouseNo, string SortField, string ParentDescription)
        {
            var sortfield = (SortField.Length > 0) ? String.Format("| {0}", SortField) : string.Empty;
            var address = (Street.Length > 0 || HouseNo.Length > 0) ? String.Format("| {0} {1}", Street.Substring(0, Street.Length > MAX_STREET_ADRESS_LENGTH ? MAX_STREET_ADRESS_LENGTH : Street.Length), HouseNo.Substring(0, HouseNo.Length > MAX_HOUSNO_LENGTH ? MAX_HOUSNO_LENGTH : HouseNo.Length)) : string.Empty;

            return String.Format("{0} | {1} {2} {3}",
                TechObjDesc.Substring(0, TechObjDesc.Length > MAX_TP_DESCRIPTION_LENGTH ? MAX_TP_DESCRIPTION_LENGTH : TechObjDesc.Length),
                address,
                sortfield,
                ParentDescription
                );
        }

        public static string buildAddressDesc(string Pincode, string City, string Street, string HouseNo)
        {
            return String.Format("{0}{1}{2}{3}", Pincode.Length > 0 ? Pincode + " " : string.Empty, City.Length > 0 ? City + " " : string.Empty, Street.Length > 0 ? Street + " " : string.Empty, HouseNo);
        }

        public static string buildShortAddressDesc(string Pincode, string City, string Street)
        {
            return String.Format("{0}{1}{2}", Pincode.Length > 0 ? Pincode + " " : string.Empty, City.Length > 0 ? City + " " : string.Empty, Street.Length > 0 ? Street + " " : string.Empty);
        }
    }
}
