﻿using SWB_Controls.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace SWB_Controls.DatePickerControls
{
    public class AllDatePickerControl : BaseAgentryCustomControl
    {
        public AgentryClientSDK.IAgentryControlViewModelStringDisplay context { get; set; }

        static AllDatePickerControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(AllDatePickerControl), new FrameworkPropertyMetadata(typeof(AllDatePickerControl)));
            PureVirtualHandler.RegisterHandler();
        }
        public void SetAgentryValue()
        {
            context = DataContext as AgentryClientSDK.IAgentryControlViewModelStringDisplay;
            var datePicker = GetTemplateChild("PART_Date") as DatePicker;

            var onsiteDate = context.GetAgentryString("ZOnsiteDate");
            if (!string.IsNullOrEmpty(onsiteDate))
            {
                datePicker.Text = onsiteDate;
            }
           
            if (datePicker.SelectedDate != null)
                context.StringValue = datePicker.SelectedDate.Value.ToString("MM/dd/yyyy");

            datePicker.SelectedDateChanged += datePicker_SelectedDateChanged;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("de-DE");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("de-DE");
        }

        private void datePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            var datePicker = sender as DatePicker;
            DateTime? date = datePicker.SelectedDate;
            context.StringValue = (date != null) ? date.Value.ToString("MM/dd/yyyy") : "";
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            SetAgentryValue();
        }
    }
}
