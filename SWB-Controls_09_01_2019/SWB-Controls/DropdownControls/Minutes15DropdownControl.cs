﻿using SWB_Controls.BusinessObjects;
using SWB_Controls.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SWB_Controls.DropdownControls
{
    public class Minutes15DropdownControl : BaseAgentryCustomControl
    {
        public AgentryClientSDK.IAgentryControlViewModelStringDisplay context { get; set; }
        static Minutes15DropdownControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Minutes15DropdownControl), new FrameworkPropertyMetadata(typeof(Minutes15DropdownControl)));
            PureVirtualHandler.RegisterHandler();
        }

        public void SetAgentryValue()
        {
            context = DataContext as AgentryClientSDK.IAgentryControlViewModelStringDisplay;
            ComboBox cmb = GetTemplateChild("PART_Minutes") as ComboBox;

            //GetItemSourceForRange arguments
            //minValue = 0 (first value / lowest value)
            //maxValue = 55 (last value / max value adding 1)
            //incrementor = 6 (jumping value and listing multiples of incrementor within the range)
            cmb.ItemsSource = DropDown.GetItemSourceForRange(0, 55, 15, true);
            var onsiteMins = context.GetAgentryString("ZOnsiteMins");
            var createdMins = context.GetAgentryString("ZCreatedMins");
            if (!string.IsNullOrEmpty(onsiteMins))
                cmb.SelectedItem = onsiteMins;
            else if (!string.IsNullOrEmpty(createdMins))
            {
                var mins = Convert.ToInt32(createdMins);
                if (mins % 15 != 0)
                {
                    if (mins >= 0 && mins < 15)
                    {
                        cmb.SelectedIndex = 0;
                    }
                    else if (mins >= 15 && mins < 30)
                    {
                        cmb.SelectedIndex = 1;
                    }
                    else if (mins >= 30 && mins < 45)
                    {
                        cmb.SelectedIndex = 2;
                    }
                    else if (mins >= 45)
                    {
                        cmb.SelectedIndex = 3;
                    }
                }
                else
                {
                    cmb.SelectedItem = createdMins;
                }
            }
            else
                cmb.SelectedIndex = 0;

            if (cmb.SelectedItem == null)
                cmb.SelectedIndex = 0;

            context.StringValue = cmb.SelectedItem.ToString();
            cmb.SelectionChanged += cmb_SelectionChanged;
        }

        private void cmb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var cmb = sender as ComboBox;
            context.StringValue = cmb.SelectedItem.ToString();
        }
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            SetAgentryValue();
        }
    }
}
