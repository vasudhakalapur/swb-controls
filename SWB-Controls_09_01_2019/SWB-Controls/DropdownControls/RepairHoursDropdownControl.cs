﻿using SWB_Controls.BusinessObjects;
using SWB_Controls.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using SWB_Controls.TextBoxControls;

namespace SWB_Controls.DropdownControls
{
    public class RepairHoursDropdownControl : BaseAgentryCustomControl
    {
        public AgentryClientSDK.IAgentryControlViewModelStringDisplay context { get; set; }
        public static ComboBox cmbHours { get; set; }

        static RepairHoursDropdownControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(RepairHoursDropdownControl), new FrameworkPropertyMetadata(typeof(RepairHoursDropdownControl)));
            PureVirtualHandler.RegisterHandler();
        }
        public void SetAgentryValue()
        {
            context = DataContext as AgentryClientSDK.IAgentryControlViewModelStringDisplay;
            cmbHours = GetTemplateChild("PART_Hours") as ComboBox;
            //GetItemSourceForRange arguments
            //minValue = 0 (first value / lowest value)
            //maxValue = 13 (last value / max value adding 1)
            //incrementor = 0 (0 means no incrementor condition for list)
            cmbHours.ItemsSource = DropDown.GetItemSourceForRange(0, 13, 0, false);
            cmbHours.SelectedIndex = 0;
            cmbHours.SelectionChanged += cmb_SelectionChanged;
        }
        private void cmb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var cmb = sender as ComboBox;
            context.StringValue = cmb.SelectedItem.ToString();
            if (cmb.SelectedIndex == 0 && Convert.ToInt32(RepairMinutesDropdownControl.txtMinutes.Text) == 0)
                RepairTextControl40Chars.textbox.IsEnabled = false;
            else
                RepairTextControl40Chars.textbox.IsEnabled = true;
        }
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            SetAgentryValue();
        }
    }
}
