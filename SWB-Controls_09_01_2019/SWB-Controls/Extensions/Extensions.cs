﻿using AgentryClientSDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWB_Controls.Extensions
{
    public static class Extensions
    {
        public static string PropertyValueByName(this IAgentryData item, string property)
        {
            var prop = item.Properties().Where(c => c.InternalName == property).FirstOrDefault();
            return prop == null ? "Property not found" : prop.ToString();
        }

        public static DateTime PropertyValueAsDateTimeByName(this IAgentryData item, string property)
        {
            var prop = item.Properties().Where(c => c.InternalName == property).FirstOrDefault();
            //adding one more check as prop.ToString() throws error if prop is null
            return (prop != null) ? (String.IsNullOrEmpty(prop.ToString()) ? DateTime.MinValue : prop.ToDateTime()) : DateTime.MinValue;
        }

        public static bool PropertyValueAsBool(this IAgentryData item, string property)
        {
            var prop = item.Properties().Where(c => c.InternalName == property).FirstOrDefault();
            return prop == null ? false : prop.ToBoolean();
        }

        public static IAgentryData CollectionByName(this IAgentryData item, string property)
        {
            for (int i = 0; i < item.DescendantCount; i++)
            {
                var desc = item.Descendant(i);
                if (desc.InternalName == property)
                {
                    return desc;
                }
            }
            return null;
            }
    }
}
