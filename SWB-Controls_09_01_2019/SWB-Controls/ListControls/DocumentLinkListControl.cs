﻿using AgentryClientSDK;
using SWB_Controls.BusinessObjects;
using SWB_Controls.Extensions;
using SWB_Controls.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Markup;

namespace SWB_Controls.ListControls
{
    public class DocumentLinkListControl : BaseAgentryCustomControl
    {
         public CollectionViewSource cvs;
        public static DataGrid grid { get; set; }
        private System.Timers.Timer refreshTimer;
        public IAgentryControlViewModelCollectionDisplay vm;
        static DocumentLinkListControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(DocumentLinkListControl), new FrameworkPropertyMetadata(typeof(DocumentLinkListControl)));
            PureVirtualHandler.RegisterHandler();
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            grid = GetTemplateChild("PART_GridDocumentList") as DataGrid;
            if (grid != null)
            {
                grid.Loaded -= grid_Loaded;
                grid.Loaded += grid_Loaded;
                grid.MouseDoubleClick -= grid_MouseDoubleClick;
                grid.MouseDoubleClick += grid_MouseDoubleClick;
                grid.DataContext = this;
            }
        }

        private void grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {         
            var path = ((AgentryClientSDK.IAgentryControlViewModelCollectionDisplay)DataContext).GetAgentryString("ZFilePath");
                

            if (path.Length > 0)
            {
                Process proc = new Process();
                proc.StartInfo = new ProcessStartInfo { FileName = path };
                proc.Start();
            }        
        }

        private DocumentLink _selectedItem;
        public DocumentLink SelectedDocument
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                _selectedItem = value;
                if (value != null)
                {
                    ((AgentryClientSDK.IAgentryControlViewModelCollectionDisplay)DataContext).SelectItem(value.Index);
                }
            }
        }
        private void grid_Loaded(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Document Grid loaded triggered");
            List<DocumentLink> docLinks = new List<DocumentLink>();
            vm = DataContext as AgentryClientSDK.IAgentryControlViewModelCollectionDisplay;
            vm.PropertyChanged += dc_PropertyChanged;
            int index = 0;
            foreach (AgentryClientSDK.IAgentryData item in vm)
            {
                docLinks.Add(new DocumentLink
                              {                                
                                  Number = item.PropertyValueByName("ID"),
                                  Name = item.PropertyValueByName("Name"),
                                  RefObject = String.Format("{0}:{1}", item.PropertyValueByName("PhysicalObjectType"), item.PropertyValueByName("ReferenceGUID")),
                                  IsLocal = item.PropertyValueByName("ID").ToString().ToLower().StartsWith("local_"),
                                  Index = index++
                              });               

            }

            cvs = new CollectionViewSource();
            cvs.Source = docLinks;
            grid.ItemsSource = cvs.View;
            grid.Items.MoveCurrentToFirst();
            grid.SelectedIndex = 0;
            SelectedDocument = (DocumentLink)grid.SelectedItem;  
        }

        private void dc_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (refreshTimer == null)
            {
                refreshTimer = new System.Timers.Timer(100);
                refreshTimer.Elapsed += t_Elapsed;
                refreshTimer.Start();
            }
        }
        void t_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                IAgentryControlViewModelCollectionDisplay dc = null;
                this.Dispatcher.Invoke(new Action(() => dc = this.DataContext as IAgentryControlViewModelCollectionDisplay));
                this.Dispatcher.Invoke(new Action(() => { grid_Loaded(null, new RoutedEventArgs()); }));
                refreshTimer.Stop();
                refreshTimer = null;
            }
            catch
            {
                //TODO 
                //Do nothing for now.
            }
        }
        public ICommand DeleteDecCommand
        {
            get
            {
                return new CommandHandler(deleteDc, true);
            }
        }

        private void deleteDc(object obj)
        {
            vm.ExecuteAgentryAction("ZDocumentLinkClearAutoWithMessage");
        }
    }
}
