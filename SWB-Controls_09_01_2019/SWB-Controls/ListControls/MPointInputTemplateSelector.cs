﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Reflection;
using SWB_Controls.BusinessObjects;

namespace SWB_Controls.ListControls
{
    public class MPointInputTemplateSelector : DataTemplateSelector
    {

        public DataTemplate NumericTemplate { get; set; }
        public DataTemplate CodegroupTemplate { get; set; }
        public DataTemplate DropdownTemplate { get; set; }
        
        public string PropertyToEvaluate  { get; set; }
        public string PropertyValueToAlter  { get; set; }


        public override DataTemplate SelectTemplate(object item, System.Windows.DependencyObject container)
        {
            MPoint dataUnit = item as MPoint;

            if (dataUnit == null) return this.NumericTemplate;

            //lets see what template we need to select according to the specified property value
            if (dataUnit.IsCodeSufficient && dataUnit.Codes.Count() > 3)
            {
                return this.DropdownTemplate;
            }
            else if (dataUnit.IsCodeSufficient)
            {
                return this.CodegroupTemplate;
            }
            else
            {
                return this.NumericTemplate;
            }

        }
    }

}