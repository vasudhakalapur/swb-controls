﻿using SWB_Controls.BusinessObjects;
using SWB_Controls.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Timers;
using AgentryClientSDK;
using System.Windows.Markup;
using SWB_Controls.Utils;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Controls.Primitives;
using System.Data;
using System.Windows.Media;

namespace SWB_Controls.ListControls
{
    public class MPointListControl : BaseAgentryCustomControl
    {
        public AgentryClientSDK.IAgentryControlViewModelCollectionDisplay context { get; set; }
        public DataGrid dataGrid { get; set; }
        public CollectionViewSource cvs;
        public bool isDirty = false;
        private System.Timers.Timer refreshTimer;
        public bool hasUnsavedChanged
        {
            set { this.SetExtensionString("HasUnsavedChanges", ((value) ? "true" : "")); }
        }

        public static string SelectedObjectID { get; set; }

        public string _filter;
        public string Filter
        {
            get
            {
                return _filter;
            }
            set
            {
                _filter = value;
                cvs.View.Refresh();
            }
        }
        private TechnicalObject _selectedTO;
        public TechnicalObject selectedTO
        {
            get
            {
                return _selectedTO;
            }
            set
            {
                _selectedTO = value;
                if (_selectedTO != null)
                {
                    this.SetExtensionString("ZTechObjectID", _selectedTO.ID);
                    this.SetExtensionString("ZTechObjectType", _selectedTO.ObjectType);
                    SelectedObjectID = _selectedTO.ObjectID;
                }
                else
                {
                    this.SetExtensionString("ZTechObjectID", "");
                    this.SetExtensionString("ZTechObjectType", "");
                    SelectedObjectID = string.Empty;
                }
                cvs.View.Refresh();
            }
        }

        public List<TechnicalObject> TechnicalObjects { get; set; }
        public bool IsEventAssigned { get; set; }
        public bool IsWONGHA { get; set; }

        static MPointListControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(MPointListControl), new FrameworkPropertyMetadata(typeof(MPointListControl)));
        }

        public override void OnApplyTemplate()
        {

            base.OnApplyTemplate();

            var baseGrid = GetTemplateChild("PART_BaseGrid") as Grid;
            if (baseGrid != null)
            {
                baseGrid.DataContext = this;
            }
            DataGrid grid = GetTemplateChild("PART_Grid") as DataGrid;
            if (grid != null)
            {
                grid.Loaded += grid_Loaded;
                //grid.Unloaded += grid_Unloaded;
                grid.SelectionChanged += grid_SelectionChanged;
            }
            this.SetExtensionString("ZHasError", "0");
        }

        void grid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!IsWONGHA)
            {
                if (!IsEventAssigned)
                {
                    if (dataGrid.Items.Count > 0)
                    {
                        dataGrid.UpdateLayout();
                        if (dataGrid.SelectedItem != null)
                            dataGrid.ScrollIntoView(dataGrid.SelectedItem);

                        AssignControlEvents();
                        IsEventAssigned = true;
                    }
                }
            }
            else
            {
                if (dataGrid.Items.Count > 0)
                {
                    dataGrid.UpdateLayout();
                    if (dataGrid.SelectedItem != null)
                        dataGrid.ScrollIntoView(dataGrid.SelectedItem);

                    AssignControlEvents();
                }
            }
        }

        //private void grid_Unloaded(object sender, RoutedEventArgs e)
        //{
        //    MessageBox.Show("Unloaded");
        //}

        void grid_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                TechnicalObjects = new List<TechnicalObject>();
                IsEventAssigned = false;
                context = DataContext as AgentryClientSDK.IAgentryControlViewModelCollectionDisplay;
                context.PropertyChanged += context_PropertyChanged;
                var defaultFilter = context.GetAgentryString("ZObjectIDForMeasuringPointFilter").Length > 0 ? context.GetAgentryString("ZObjectIDForMeasuringPointFilter") : SelectedObjectID; ;
                var woType = context.GetAgentryString("Type");
                IsWONGHA = woType.Equals("NGHA");

                foreach (AgentryClientSDK.IAgentryData item in context)
                {

                    string wonum = item.Ancestor.PropertyValueByName("WONum");

                    TechnicalObject obj = new TechnicalObject()
                    {
                        ID = item.PropertyValueByName("ZTechnicalObject"),
                        ObjectID = item.PropertyValueByName("ObjectID")
                    };
                    obj.TechObjDesc = item.PropertyValueByName("ZTechnicalObjectDesc");
                    obj.DescriptionLong = item.PropertyValueByName("ZTechnicalObject");
                    obj.ObjectType = item.PropertyValueByName("ZType");
                    var mpoints = item.CollectionByName("MeasuringPoints");
                    obj.Street = item.PropertyValueByName("ZStreet");
                    obj.HouseNo = item.PropertyValueByName("ZHouseNum");
                    obj.SortField = item.PropertyValueByName("ZSortField");
                    obj.ParentDescription = item.PropertyValueByName("ZParentTechnicalObjectDesc");

                    for (int i = 0; i < mpoints.DescendantCount; i++)
                    {
                        MPoint mp = new MPoint(obj);
                        mp.agentrydata = mpoints.Descendant(i);
                        mp.Description = mpoints.Descendant(i).PropertyValueByName("Description");
                        mp.ID = mpoints.Descendant(i).PropertyValueByName("Point");
                        mp.Catalog = mpoints.Descendant(i).PropertyValueByName("CatalogType");
                        mp.CodeGroup = mpoints.Descendant(i).PropertyValueByName("CodeGroup");
                        mp.IsCodeSufficient = mpoints.Descendant(i).PropertyValueByName("CodeGroup") != "";
                        mp.UOM = mpoints.Descendant(i).PropertyValueByName("UOM");
                        //SWB-74 - Changing property from ShortText to ReadingShortText
                        mp.Shorttext = mpoints.Descendant(i).PropertyValueByName("ReadingShortText");
                        mp.Notes = mpoints.Descendant(i).PropertyValueByName("Notes");
                        mp.isDocumentAttached = mpoints.Descendant(i).PropertyValueAsBool("ZIsDocumentAttached");
                        mp.ReadingDate = mpoints.Descendant(i).PropertyValueAsDateTimeByName("ReadingDate");
                        mp.DefaultValue = mpoints.Descendant(i).PropertyValueByName("ZDefaultValue");
                        mp.IsLowerRange = mpoints.Descendant(i).PropertyValueAsBool("IsLowerRange");
                        mp.IsUpperRange = mpoints.Descendant(i).PropertyValueAsBool("IsUpperRange");
                        if (mp.IsLowerRange)
                        {
                            mp.LowerRange = Convert.ToDecimal(mpoints.Descendant(i).PropertyValueByName("LowerRange"));
                        }
                        if (mp.IsUpperRange)
                        {
                            mp.UpperRange = Convert.ToDecimal(mpoints.Descendant(i).PropertyValueByName("UpperRange"));
                        }
                        //Set Reading / Valuation Code
                        mp.IsRead = mpoints.Descendant(i).PropertyValueAsBool("ZIsRead");
                        if (!mp.IsRead) // Reading has not been taken - use default value
                        {
                            if (!String.IsNullOrEmpty(mp.DefaultValue))
                            {
                                if (mp.IsCodeSufficient)
                                {
                                    if (mp.Codes.Where(c => c.Code == mp.DefaultValue).FirstOrDefault() != null)
                                    {
                                        mp.ValuationCode = mp.DefaultValue;
                                        mp.isModified = true;
                                    }
                                    else
                                    {
                                        System.Diagnostics.Debug.WriteLine("DefaultCode not found " + mp.DefaultValue + " for MPoint " + mp.Description);
                                    }
                                }
                                else
                                {
                                    mp.Reading = (!String.IsNullOrEmpty(mp.DefaultValue)) ? mp.DefaultValue.Replace(".", ",") : string.Empty;
                                    mp.isModified = true;
                                }
                            }

                            //Set code in case ValuationCode-Field is set:
                            if (!String.IsNullOrWhiteSpace(mp.ValuationCode))
                            {
                                var defmp = mp.Codes.Where(c => c.Code == mp.ValuationCode).FirstOrDefault();
                                if (defmp != null)
                                {
                                    mp.selectedCode = defmp;
                                    mp.isModified = true; //on save default value will be saved.
                                }
                                else
                                {
                                    System.Diagnostics.Debug.WriteLine("DefaultCode not found " + mp.DefaultValue + " for MPoint " + mp.Description);
                                }
                            }
                        }
                        else
                        {
                            mp.Reading = (!String.IsNullOrEmpty(mpoints.Descendant(i).PropertyValueByName("Reading"))) ? mpoints.Descendant(i).PropertyValueByName("Reading").Replace(".", ",") : string.Empty;
                            mp.isModified = false; //setting is explicitly to false here, to avoid saving last value again.
                            mp.ValuationCode = mpoints.Descendant(i).PropertyValueByName("ValuationCode");
                            mp.hasPendingMeasDoc = true;

                            //Set code in case ValuationCode-Field is set:
                            if (!String.IsNullOrWhiteSpace(mp.ValuationCode))
                            {
                                var defmp = mp.Codes.Where(c => c.Code == mp.ValuationCode).FirstOrDefault();
                                if (defmp != null)
                                {

                                    mp.selectedCode = mp.Codes.Where(c => c.Code == mp.ValuationCode).FirstOrDefault();
                                    mp.isModified = false; //setting is explicitly to false here, to avoid saving last value again.
                                }
                            }
                        }
                        mp.isPreviousReading = false;
                        var histDocsCollection = mpoints.Descendant(i).CollectionByName("ZHistoryMeasurementDocuments");
                        for (int j = 0; j < histDocsCollection.DescendantCount; j++)
                        {
                            var doc = histDocsCollection.Descendant(j);
                            if (doc.PropertyValueByName("ZWONum") == wonum)
                            {
                                mp.FlagPrevCodeDesc = doc.PropertyValueByName("ValuationCode");
                                mp.FlagPrevReadingDate = doc.PropertyValueByName("ReadingDate");
                                if (!IsWONGHA && !String.IsNullOrEmpty(mp.FlagPrevCodeDesc))
                                {
                                    mp.isPreviousReading = true;
                                }
                            }
                            else
                            {
                                mp.PrevCodeDesc = doc.PropertyValueByName("ValuationCode");
                                mp.PrevReadingDate = doc.PropertyValueByName("ReadingDate");
                            }
                        }
                        obj.mpoints.Add(mp);

                    }
                    if (obj.mpoints.Any())
                    {
                        TechnicalObjects.Add(obj);
                    }
                }

                dataGrid = GetTemplateChild("PART_Grid") as DataGrid;

                cvs = new CollectionViewSource();
                cvs.Filter += cvs_Filter;
                cvs.Source = TechnicalObjects.SelectMany(c => c.mpoints).ToList();

                if (defaultFilter != null && defaultFilter != "")
                {
                    selectedTO = TechnicalObjects.SingleOrDefault(x => x.ObjectID == defaultFilter.ToString());
                    this.firePropertyChanged("selectedTO");
                    //if (selectedTO != null)
                    //{
                    //    var cmb = GetTemplateChild("PART_Combo") as ComboBox;
                    //    cmb.SelectedValue = defaultFilter.ToString();
                    //}
                }

                cvs.GroupDescriptions.Add(new PropertyGroupDescription("TechnicalObject.FullDescription"));

                ListCollectionView lcw = cvs.View as ListCollectionView;
                dataGrid.ItemsSource = cvs.View;

                this.SetExtensionString("ZTechObjectID", "");
                this.SetExtensionString("ZTechObjectType", "");
                if (dataGrid.Items.Count > 0)
                {
                    dataGrid.Items.MoveCurrentToFirst();
                }
                if (IsWONGHA)
                {
                    var filterFramework = GetTemplateChild("PART_Filter") as StackPanel;
                    filterFramework.Visibility = Visibility.Collapsed;

                    dataGrid.Columns.SingleOrDefault(x => x.Header.Equals("Letzter Meßwert")).Visibility = Visibility.Collapsed;
                    dataGrid.Columns.SingleOrDefault(x => x.Header.Equals("Bezugsobjekt")).Visibility = Visibility.Collapsed;

                    //dataGrid.Columns[dataGrid.Columns.Count - 1].Visibility = Visibility.Visible;

                    var baseGrid = GetTemplateChild("PART_BaseGrid") as Grid;
                    baseGrid.RowDefinitions[0].Height = new GridLength(0, GridUnitType.Star);
                }

                var btnResetFilter = GetTemplateChild("PART_btnResetFilter") as Button;
                btnResetFilter.Click += btnResetFilter_Click;

                firePropertyChanged("TechnicalObjects");

            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex);
            }
        }

        void AssignControlEvents()
        {
            foreach (var txt in DataGridHelper.FindVisualChild<TextBox>(dataGrid, "PART_Textbox"))
            {
                txt.PreviewTextInput += txt_PreviewTextInput;
                txt.LostFocus += txt_LostFocus;
                //txt.TextChanged += txt_TextChanged;
            }
            foreach (var radio in DataGridHelper.FindVisualChild<RadioButton>(dataGrid, "PART_Radio"))
            {
                radio.Checked += radio_Checked;
            }
            foreach (var combo in DataGridHelper.FindVisualChild<ComboBox>(dataGrid, "PART_Combo"))
            {
                combo.SelectionChanged += combo_SelectionChanged;
            }
        }

        void context_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("MPoint List Control Property Changed" + e.PropertyName);
            if (e.PropertyName == "SupportsFiltering" && dataGrid != null)
            {
                if (dataGrid.SelectedItem != null && dataGrid.Items.Count > 0)
                {
                    //Refresh this item.
                    MPoint point = dataGrid.SelectedItem as MPoint;
                    point.refreshShorttextandDocumentProperties();
                    this.SetExtensionString("ZHasError", "0");
                }
            }
        }

        private void btnResetFilter_Click(object sender, RoutedEventArgs e)
        {
            selectedTO = null;
            this.firePropertyChanged("selectedTO");
            IsEventAssigned = false;
            if (dataGrid.Items.Count > 0)
            {
                dataGrid.Items.MoveCurrentToFirst();
            }
        }

        private void combo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            hasUnsavedChanged = true;
        }

        private void radio_Checked(object sender, RoutedEventArgs e)
        {
            hasUnsavedChanged = true;
        }

        //private void txt_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    hasUnsavedChanged = true;
        //}

        private void txt_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = StringHelper.IsNumericCommaTextAllowed(e.Text);
        }

        private void txt_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox txt = (TextBox)sender;
            if (txt.Text.Length == 0 && txt.Background != Brushes.Red)
                return;
            else if (txt.Text.Length == 0 && txt.Background == Brushes.Red)
            {
                txt.Background = Brushes.White;
                txt.Foreground = Brushes.Black;
                return;
            }
            var grid = GetTemplateChild("PART_Grid") as DataGrid;
            var item = (MPoint)grid.SelectedItem;
            if (item.IsLowerRange && item.IsUpperRange)
            {
                if (!StringHelper.IsValueWithinRange(txt.Text, item.LowerRange, item.UpperRange))
                {
                    txt.Background = Brushes.Red;
                    txt.Foreground = Brushes.White;
                }
                else
                {
                    txt.Background = Brushes.White;
                    txt.Foreground = Brushes.Black;
                }
            }
            else if (item.IsLowerRange && !item.IsUpperRange)
            {
                if (!StringHelper.IsValueInRange(txt.Text, true, item.LowerRange))
                {
                    txt.Background = Brushes.Red;
                    txt.Foreground = Brushes.White;
                }
                else
                {
                    txt.Background = Brushes.White;
                    txt.Foreground = Brushes.Black;
                }
            }
            else if (!item.IsLowerRange && item.IsUpperRange)
            {
                if (!StringHelper.IsValueInRange(txt.Text, false, item.UpperRange))
                {
                    txt.Background = Brushes.Red;
                    txt.Foreground = Brushes.White;
                }
                else
                {
                    txt.Background = Brushes.White;
                    txt.Foreground = Brushes.Black;
                }
            }
            hasUnsavedChanged = true;
        }

        void cvs_Filter(object sender, FilterEventArgs e)
        {
            MPoint t = e.Item as MPoint;
            if (t != null)
            // If filter is turned on, filter completed items.
            {
                if (selectedTO == null) { e.Accepted = true; return; }
                if (t.TechnicalObject == selectedTO)
                {
                    e.Accepted = true;
                    IsEventAssigned = false;
                }
                else
                    e.Accepted = false;
            }
        }

        #region commands
        public ICommand NavigateToAddShortTextCommand
        {
            get
            {
                return new CommandHandler(navigateToAddShortText, true);
            }
        }

        public ICommand NavigateToDocumentCommand
        {
            get
            {
                return new CommandHandler(navigateToAddDocument, true);
            }
        }

        public ICommand DeleteMeasDocCommand
        {
            get
            {
                return new CommandHandler(deleteMeasDoc, true);
            }
        }

        private void navigateToAddShortText(object target)
        {
            MPoint mpoint = target as MPoint;
            addShortText(mpoint.TechnicalObject.ObjectID, mpoint.ID);
        }

        void addShortText(string ObjectID, string Point)
        {
            this.SetExtensionString("ObjectID", ObjectID);
            this.SetExtensionString("Point", Point);
            context.ExecuteAgentryAction("ZMeasuringPointAddShortText");
            hasUnsavedChanged = true;
        }

        private void navigateToAddDocument(object target)
        {
            MPoint mpoint = target as MPoint;
            addDocument(mpoint.TechnicalObject.ObjectID, mpoint.ID);
        }

        void addDocument(string ObjectID, string Point)
        {
            this.SetExtensionString("ObjectID", ObjectID);
            this.SetExtensionString("Point", Point);
            context.ExecuteAgentryAction("ZDocumentAddForMeasurementDoc");
            hasUnsavedChanged = true;
        }

        private void deleteMeasDoc(object target)
        {
            MPoint mpoint = target as MPoint;
            this.SetExtensionString("ObjectID", mpoint.TechnicalObject.ObjectID);
            this.SetExtensionString("Point", mpoint.ID);
            context.ExecuteAgentryAction("ZMeasurementDocClear");
            //if (IsWONGHA)
            //{
            //    if (refreshTimer == null)
            //    {
            //        refreshTimer = new System.Timers.Timer();
            //        refreshTimer.Elapsed += (sender, e) => time_Elapsed(mpoint.ID, mpoint);
            //        refreshTimer.Interval = 500;
            //        refreshTimer.Start();
            //    }
            //}
            //else
            //{
            mpoint.resetReading();
            //}
        }

        void time_Elapsed(string id, MPoint mpoint)
        {
            try
            {
                IAgentryControlViewModelCollectionDisplay dc = null;
                this.Dispatcher.Invoke(new Action(() => dc = this.DataContext as IAgentryControlViewModelCollectionDisplay));

                foreach (AgentryClientSDK.IAgentryData item in dc)
                {
                    var mpoints = item.CollectionByName("MeasuringPoints");
                    for (int i = 0; i < mpoints.DescendantCount; i++)
                    {
                        if (id == mpoints.Descendant(i).PropertyValueByName("Point"))
                        {
                            mpoint.IsRead = mpoints.Descendant(i).PropertyValueAsBool("ZIsRead");
                            if (!mpoint.IsRead)
                            {
                                mpoint.resetReading();
                            }
                            break;
                        }
                    }
                }
                refreshTimer.Stop();
                refreshTimer = null;
            }
            catch
            {
                //TODO 
                //Do nothing for now.
            }
        }
        public ICommand SaveCommand
        {
            get
            {
                return new CommandHandler(saveMPoints, true);
            }
        }

        private void saveMPoints(object target)
        {
            dataGrid = GetTemplateChild("PART_Grid") as DataGrid;
            dataGrid.UpdateLayout();
            if (dataGrid.SelectedItem != null)
            {
                dataGrid.ScrollIntoView(dataGrid.SelectedItem);
            }
            foreach (var txt in DataGridHelper.FindVisualChild<TextBox>(dataGrid, "PART_Textbox"))
            {
                if (txt.Background == Brushes.Red)
                {
                    isDirty = true;
                }
            }
            if (!isDirty)
            {
                hasUnsavedChanged = false;
                var PointToSave = selectedTO != null ? TechnicalObjects.SelectMany(c => c.mpoints).Where(d => d.isModified && d.TechnicalObject == selectedTO) : TechnicalObjects.SelectMany(c => c.mpoints).Where(d => d.isModified);
                foreach (var item in PointToSave)
                {
                    if (IsWONGHA)
                    {

                        if (item.IsCodeSufficient && item.ValuationCode != item.DefaultValue && item.Shorttext.Length == 0)
                        {
                            item.ShowError(true);
                            hasUnsavedChanged = true;
                        }
                        else if (!item.IsCodeSufficient && item.Reading != item.DefaultValue && item.Shorttext.Length == 0)
                        {
                            item.ShowError(true);
                            hasUnsavedChanged = true;
                        }
                        this.SetExtensionString("ObjectID", item.TechnicalObject.ObjectID);
                        this.SetExtensionString("Point", item.ID);
                        this.SetExtensionString("ValuationCode", item.ValuationCode);
                        this.SetExtensionString("Reading", (!String.IsNullOrEmpty(item.Reading)) ? item.Reading.Replace(",", ".") : string.Empty);
                        context.ExecuteAgentryAction("ZMeasuringPointAdd");

                    }
                    else
                    {
                        this.SetExtensionString("ObjectID", item.TechnicalObject.ObjectID);
                        this.SetExtensionString("Point", item.ID);
                        this.SetExtensionString("ValuationCode", item.ValuationCode);
                        this.SetExtensionString("Reading", (!String.IsNullOrEmpty(item.Reading)) ? item.Reading.Replace(",", ".") : string.Empty);
                        context.ExecuteAgentryAction("ZMeasuringPointAdd");
                    }
                }


                Debug.WriteLine("Saved " + PointToSave.Count() + " M Points");
                foreach (var item in this.extensionKeys)
                {
                    Debug.WriteLine("key [" + item.Key + "] = " + item.Value);
                }
                if (refreshTimer == null)
                {
                    refreshTimer = new System.Timers.Timer();
                    refreshTimer.Elapsed += (sender, e) => t_Elapsed(selectedTO);
                    refreshTimer.Interval = 500;
                    refreshTimer.Start();
                }
            }
            else
            {
                MessageBox.Show("Bitte beheben Sie die Fehler, um fortzufahren");
            }
        }

        void t_Elapsed(TechnicalObject selectedObject)
        {
            try
            {
                var count = 0;
                IAgentryControlViewModelCollectionDisplay dc = null;
                this.Dispatcher.Invoke(new Action(() => dc = this.DataContext as IAgentryControlViewModelCollectionDisplay));

                foreach (AgentryClientSDK.IAgentryData item in dc)
                {
                    var objectID = item.PropertyValueByName("ObjectID");
                    var mpoints = item.CollectionByName("MeasuringPoints");
                    if (selectedObject != null && selectedObject.ObjectID == objectID)
                    {
                        for (int i = 0; i < mpoints.DescendantCount; i++)
                        {
                            ((MPoint)dataGrid.Items[count]).IsRead = mpoints.Descendant(i).PropertyValueAsBool("ZIsRead");
                            ((MPoint)dataGrid.Items[count]).markAsMeasDocSaved();
                            count++;
                        }
                        break;
                    }
                    else if (selectedObject == null)
                    {
                        for (int i = 0; i < mpoints.DescendantCount; i++)
                        {
                            ((MPoint)dataGrid.Items[count]).IsRead = mpoints.Descendant(i).PropertyValueAsBool("ZIsRead");
                            ((MPoint)dataGrid.Items[count]).markAsMeasDocSaved();
                            count++;
                        }
                    }
                }
                refreshTimer.Stop();
                refreshTimer = null;
            }
            catch
            {
                //TODO 
                //Do nothing for now.
            }
        }


        #endregion
    }

}
