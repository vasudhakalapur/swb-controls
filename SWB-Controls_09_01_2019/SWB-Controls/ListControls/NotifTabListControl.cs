﻿using AgentryClientSDK;
using SWB_Controls.BusinessObjects;
using SWB_Controls.Extensions;
using SWB_Controls.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;

namespace SWB_Controls.ListControls
{
    public class NotifTabListControl: BaseAgentryCustomControl
    {
        
        public CollectionViewSource cvs;
        public static DataGrid grid { get; set; }
        private System.Timers.Timer refreshTimer;
        static string selectedNotif = "";
        private bool ignoreSelectionChanges = false; //this will be set to true during intialization of grid to  avoid saving wrong selection.
        public Notification _selectedItem;
        public Notification SelectedNotif
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                _selectedItem = value;
                if (ignoreSelectionChanges) return;
                if (_selectedItem != null)
                {
                    selectedNotif = _selectedItem.NotificationNumber;
                    ((AgentryClientSDK.IAgentryControlViewModelCollectionDisplay)DataContext).SelectItem(value.Index);
                }
            }
        }
        static NotifTabListControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(NotifTabListControl), new FrameworkPropertyMetadata(typeof(NotifTabListControl)));
            PureVirtualHandler.RegisterHandler();
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            grid = GetTemplateChild("PART_GridNotifList") as DataGrid;
            if (grid != null)
            {
                grid.Loaded -= grid_Loaded;
                grid.Loaded += grid_Loaded;
                grid.DataContext = this;
            }
        }

        private void grid_Loaded(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Notif Grid loaded triggered");
            ignoreSelectionChanges = true;
            List<Notification> notifications = new List<Notification>();
            AgentryClientSDK.IAgentryControlViewModelCollectionDisplay vm = DataContext as AgentryClientSDK.IAgentryControlViewModelCollectionDisplay;
            vm.PropertyChanged += dc_PropertyChanged;
            int index = 0;
            foreach (AgentryClientSDK.IAgentryData item in vm)
            {
                DateTime notifDateTime;
                var success = DateTime.TryParse(item.PropertyValueByName("ZCreatedDate"), out notifDateTime);
                notifications.Add(new Notification
                              {
                                  context = vm,
                                  WorkOrderNumber = item.PropertyValueByName("NotifWO"),
                                  NotificationNumber = item.PropertyValueByName("NotifNum"),
                                  StartAsDate = (success) ? notifDateTime : DateTime.MinValue,
                                  NotifLocal = item.PropertyValueByName("NotifLocal"),
                                  NotifIsClosed = item.PropertyValueAsBool("ZIsClosed"),
                                  Description = item.PropertyValueByName("Description"),
                                  Techobj = GetTechnicalObjectDesc(WorkOrderListControl.woContext, item.PropertyValueByName("FuncLoc"), item.PropertyValueByName("EquipmentID")),
                                  Index = index++
                              });
            }

            cvs = new CollectionViewSource();
            cvs.Source = notifications;
            cvs.SortDescriptions.Add(new SortDescription("CreatedDate", ListSortDirection.Descending));
            grid.ItemsSource = cvs.View;
            if (selectedNotif != "")
            {
                var item = notifications.SingleOrDefault(x => x.NotificationNumber == selectedNotif);
                if (item != null)
                {
                    System.Diagnostics.Debug.WriteLine("Restore of selectedNotif from" + selectedNotif);
                    grid.SelectedItem = item;
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("Could not find selectedNotif from" + selectedNotif + "selecting first row instead");
                    grid.SelectedIndex = 0;
                }
            }
            else
            {
                grid.Items.MoveCurrentToFirst();
                grid.SelectedIndex = 0;
            }
            ignoreSelectionChanges = false;
            SelectedNotif = (Notification)grid.SelectedItem;
            grid.UpdateLayout();
            grid.ScrollIntoView(grid.SelectedIndex);
            foreach (var chk in DataGridHelper.FindVisualChild<CheckBox>(grid, "PART_Checkbox"))
            {
                chk.Checked += chk_Checked;
                chk.Unchecked += chk_Unchecked;
            }
        }

        void chk_Unchecked(object sender, RoutedEventArgs e)
        {
            (DataContext as AgentryClientSDK.IAgentryControlViewModelCollectionDisplay).ExecuteAgentryAction("ZNotificationOpen");
        }

        void chk_Checked(object sender, RoutedEventArgs e)
        {
            (DataContext as AgentryClientSDK.IAgentryControlViewModelCollectionDisplay).ExecuteAgentryAction("ZNotificationClose");
        }

        private void dc_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (refreshTimer == null)
            {
                refreshTimer = new System.Timers.Timer(100);
                refreshTimer.Elapsed += t_Elapsed;
                refreshTimer.Start();
            }
        }
        void t_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                IAgentryControlViewModelCollectionDisplay dc = null;
                this.Dispatcher.Invoke(new Action(() => dc = this.DataContext as IAgentryControlViewModelCollectionDisplay));
                this.Dispatcher.Invoke(new Action(() => { grid_Loaded(null, new RoutedEventArgs()); }));
                refreshTimer.Stop();
                refreshTimer = null;
            }
            catch
            {
                //TODO 
                //Do nothing for now.
            }
        }

        private string GetTechnicalObjectDesc(AgentryClientSDK.IAgentryData item, string functionLoc, string equiID)
        {
            for (int i = 0; i < item.DescendantCount; i++)
            {
                var descendant = item.Descendant(i);
                if (descendant.InternalName == "ObjectList")
                {
                    if (descendant.DescendantCount > 0)
                    {                       
                        for (int j = 0; j < descendant.DescendantCount; j++)
                        {
                            var funcLoc = descendant.Descendant(j);
                            if (!String.IsNullOrEmpty(functionLoc))
                            {
                                if (funcLoc.PropertyValueByName("ZTechnicalObject") == functionLoc && funcLoc.PropertyValueByName("ZType") == "TP")
                                    return funcLoc.PropertyValueByName("ZTechnicalObjectDesc");
                            
                            }
                            else if (!String.IsNullOrEmpty(equiID))
                            {
                                if (funcLoc.PropertyValueByName("ZTechnicalObject") == equiID && funcLoc.PropertyValueByName("ZType") == "EQ")
                                    return funcLoc.PropertyValueByName("ZTechnicalObjectDesc");
                            }
                        }
                    }
                }
            }
            return string.Empty;
        }
    }
}
