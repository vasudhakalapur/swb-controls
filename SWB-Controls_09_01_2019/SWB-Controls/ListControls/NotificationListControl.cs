﻿using SWB_Controls.BusinessObjects;
using SWB_Controls.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;
using SWB_Controls.Extensions;
using System.ComponentModel;
using System.Timers;
using AgentryClientSDK;
using System.Windows.Input;
using System.Collections;

namespace SWB_Controls.ListControls
{
    public class NotificationListControl : BaseAgentryCustomControl
    {

        public AgentryClientSDK.IAgentryControlViewModelCollectionDisplay context { get; set; }
        static string selectedNotif = "";
        static NotificationListControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(NotificationListControl), new FrameworkPropertyMetadata(typeof(NotificationListControl)));
            injectDictionary();
            PureVirtualHandler.RegisterHandler();
        }

        private static void injectDictionary()
        {
            var ResName = "SWB-Controls;component/Resources/Theme.xaml";
            var uri = new Uri(ResName, UriKind.Relative);
            var streamResourceInfo = Application.GetResourceStream(uri);

            using (var resStream = streamResourceInfo.Stream)
            {
                ResourceDictionary myResDic = (ResourceDictionary)XamlReader.Load(resStream);
                Application.Current.Resources.MergedDictionaries.Add(myResDic);
            }
        }
        private Timer refreshTimer;
        public Notification currentNotification;
        public CollectionViewSource cvs;
        private bool ignoreSelectionChanges = false; //this will be set to true during intialization of grid to  avoid saving wrong selection.
        private Notification _selectedItem;
        public Notification SelectedNotification
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                _selectedItem = value;

                if (ignoreSelectionChanges) return;

                if (_selectedItem != null)
                {
                    selectedNotif = _selectedItem.NotificationNumber;
                    ((AgentryClientSDK.IAgentryControlViewModelCollectionDisplay)DataContext).SelectItem(value.Index);
                }
            }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            DataGrid grid = GetTemplateChild("PART_Grid") as DataGrid;
            if (grid != null)
            {
                grid.Loaded += grid_Loaded;
                grid.Sorting += grid_Sorting;
                grid.DataContext = this;
            }
        }

        void grid_Sorting(object sender, DataGridSortingEventArgs e)
        {
            DataGridColumn column = e.Column;

            if (column.SortMemberPath == "CreatedDate")
            {
                e.Handled = true;
                var direction = (column.SortDirection != ListSortDirection.Ascending) ? ListSortDirection.Ascending : ListSortDirection.Descending;
                column.SortDirection = direction;

                var lcv = cvs.View as ListCollectionView;
                IComparer comparer = (direction == ListSortDirection.Ascending) ? (IComparer)new NotificationDateComparer_Ascending() : (IComparer)new NotificationDateComparer_Descending();
                lcv.CustomSort = comparer;
            }

        }
      
        void grid_Loaded(object sender, RoutedEventArgs e)
        {
            ignoreSelectionChanges = true;
            List<Notification> notifications = new List<Notification>();
            context = DataContext as AgentryClientSDK.IAgentryControlViewModelCollectionDisplay;
            context.PropertyChanged += dc_PropertyChanged;
            CodeGroup.initCodeGroups(context);
            int index = 0;
            foreach (AgentryClientSDK.IAgentryData item in context)
            {
                DateTime notifDateTime;
                var success = DateTime.TryParse(item.PropertyValueByName("ZCreatedDate"), out notifDateTime);
                var notification =
                new Notification(this)
                {
                    context = context,
                    WorkOrderNumber = item.PropertyValueByName("NotifWO"),
                    NotificationNumber = item.PropertyValueByName("NotifNum"),
                    Priority = item.PropertyValueByName("Priority"),
                    StartAsDate = (success) ? notifDateTime : DateTime.MinValue,
                    NotifLocal = item.PropertyValueByName("NotifLocal"),
                    NotifIsClosed = item.PropertyValueAsBool("ZIsClosed"),
                    Description = item.PropertyValueByName("Description"),
                    Techobj = (item.PropertyValueByName("NotifLocal") == "L") ? BuildLocalAddress(item) : BuildSAPAddress(item),
                    Index = index++
                };

                notifications.Add(notification);
            }

            DataGrid grid = GetTemplateChild("PART_Grid") as DataGrid;

            cvs = new CollectionViewSource();

            cvs.SortDescriptions.Add(new SortDescription("StartAsDate", ListSortDirection.Descending));
            cvs.SortDescriptions.Add(new SortDescription("Priority", ListSortDirection.Ascending));
            cvs.Source = notifications;

            ListCollectionView lcw = cvs.View as ListCollectionView;
            lcw.CustomSort = new NotificationComparer();

            grid.ItemsSource = cvs.View;
            if (selectedNotif != "")
            {
                var item = notifications.SingleOrDefault(x => x.NotificationNumber == selectedNotif);
                if (item != null)
                {
                    System.Diagnostics.Debug.WriteLine("Restore of selectedNotif from" + selectedNotif);
                    grid.SelectedItem = item;
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("Could not find selectedNotif from" + selectedNotif + "selecting first row instead");
                    grid.SelectedIndex = 0;
                }
            }
            else
            {
                grid.Items.MoveCurrentToFirst();
                grid.SelectedIndex = 0;
            }
            ignoreSelectionChanges = false;
            SelectedNotification = (Notification)grid.SelectedItem;
            grid.UpdateLayout();
            grid.ScrollIntoView(grid.SelectedIndex);
            foreach (var chk in DataGridHelper.FindVisualChild<CheckBox>(grid, "PART_Checkbox"))
            {
                chk.Checked += chk_Checked;
                chk.Unchecked += chk_Unchecked;
            }
        }
        void chk_Unchecked(object sender, RoutedEventArgs e)
        {
            (DataContext as AgentryClientSDK.IAgentryControlViewModelCollectionDisplay).ExecuteAgentryAction("ZNotificationOpen");
        }

        void chk_Checked(object sender, RoutedEventArgs e)
        {
            (DataContext as AgentryClientSDK.IAgentryControlViewModelCollectionDisplay).ExecuteAgentryAction("ZNotificationClose");
        }
        private string BuildLocalAddress(IAgentryData agentryData)
        {
            string City = agentryData.PropertyValueByName("ZCity");
            string Street = agentryData.PropertyValueByName("ZStreet");
            string HouseNo = agentryData.PropertyValueByName("ZHouseNum");
            string desc = string.Empty;
            var funcLoc = GetTechnicalObjectDesc((DataContext as AgentryClientSDK.IAgentryControlViewModelCollectionDisplay).First().Ancestor, agentryData.PropertyValueByName("FuncLoc"), agentryData.PropertyValueByName("EquipmentID"));
            if (funcLoc != null)
            {
                desc = funcLoc.PropertyValueByName("ZTechnicalObjectDesc");
            }
            return String.Format("{0} | {1} {2} {3}", desc, City, Street, HouseNo);
        }
        private string BuildSAPAddress(IAgentryData agentryData)
        {
            var funcLoc = GetTechnicalObjectDesc((DataContext as AgentryClientSDK.IAgentryControlViewModelCollectionDisplay).First().Ancestor, agentryData.PropertyValueByName("FuncLoc"), agentryData.PropertyValueByName("EquipmentID"));
            if (funcLoc != null)
            {
                var desc = funcLoc.PropertyValueByName("ZTechnicalObjectDesc");
                string Street = funcLoc.PropertyValueByName("ZStreet");
                string HouseNo = funcLoc.PropertyValueByName("ZHouseNum");
                return String.Format("{0} | {1} {2}", desc, Street, HouseNo);
            }
            else
                return string.Empty;
        }
        
        private void dc_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (refreshTimer == null)
            {
                refreshTimer = new System.Timers.Timer(50);
                refreshTimer.Elapsed += t_Elapsed;
                refreshTimer.Start();
            }
        }
        void t_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                IAgentryControlViewModelCollectionDisplay dc = null;
                this.Dispatcher.Invoke(new Action(() => dc = this.DataContext as IAgentryControlViewModelCollectionDisplay));
                this.Dispatcher.Invoke(new Action(() => { grid_Loaded(null, new RoutedEventArgs()); }));
                refreshTimer.Stop();
                refreshTimer = null;
            }
            catch
            {
                //TODO 
                //Do nothing for now.
            }
        }
        private IAgentryData GetTechnicalObjectDesc(AgentryClientSDK.IAgentryData item, string functionLoc, string equiID)
        {
            for (int i = 0; i < item.DescendantCount; i++)
            {
                var descendant = item.Descendant(i);
                if (descendant.InternalName == "ObjectList")
                {
                    if (descendant.DescendantCount > 0)
                    {
                        for (int j = 0; j < descendant.DescendantCount; j++)
                        {
                            var funcLoc = descendant.Descendant(j);
                            if (!String.IsNullOrEmpty(functionLoc))
                            {
                                if (funcLoc.PropertyValueByName("ZTechnicalObject") == functionLoc && funcLoc.PropertyValueByName("ZType") == "TP")
                                    return funcLoc;

                            }
                            else if (!String.IsNullOrEmpty(equiID))
                            {
                                if (funcLoc.PropertyValueByName("ZTechnicalObject") == equiID && funcLoc.PropertyValueByName("ZType") == "EQ")
                                    return funcLoc;
                            }
                        }
                    }
                }
            }
            return null;
        }
        public ICommand DeleteNotifCommand
        {
            get
            {
                return new CommandHandler(deleteNotif, true);
            }
        }
        public ICommand EditNotifCommand
        {
            get
            {
                return new CommandHandler(editNotif, true);
            }
        }

        private void editNotif(object obj)
        {
            context.ExecuteAgentryAction("ZNavigateToNotificationEditScreen");
        }

        private void deleteNotif(object obj)
        {
            Notification notif = obj as Notification;
            context.ExecuteAgentryAction("ZNotificationClear");
        }       
    }
}
