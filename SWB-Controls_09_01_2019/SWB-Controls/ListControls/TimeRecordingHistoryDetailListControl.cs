﻿using SWB_Controls.BusinessObjects;
using SWB_Controls.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Timers;
using AgentryClientSDK;
using System.Windows.Markup;
using SWB_Controls.Utils;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Threading;
using System.Windows.Threading;

namespace SWB_Controls.ListControls
{
    public class TimeRecordingHistoryDetailListControl : BaseAgentryCustomControl
    {
        public AgentryClientSDK.IAgentryControlViewModelCollectionDisplay context { get; set; }
        static TimeRecordingHistoryDetailListControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TimeRecordingHistoryDetailListControl), new FrameworkPropertyMetadata(typeof(TimeRecordingHistoryDetailListControl)));
            injectDictionary();
            PureVirtualHandler.RegisterHandler();
        }

        private static void injectDictionary()
        {
            var ResName = "SWB-Controls;component/Resources/Theme.xaml";
            var uri = new Uri(ResName, UriKind.Relative);

            var streamResourceInfo = Application.GetResourceStream(uri);

            using (var resStream = streamResourceInfo.Stream)
            {
                ResourceDictionary myResDic = (ResourceDictionary)XamlReader.Load(resStream);
                Application.Current.Resources.MergedDictionaries.Add(myResDic);
            }
        }

        static Dictionary<string, ListSortDirection> dictSort = new Dictionary<string, ListSortDirection>();
        public CollectionViewSource cvs;
        private LaborTimeDetails _selectedTimeRecord;
        public LaborTimeDetails SelectedTimeRecord
        {
            get
            {
                return _selectedTimeRecord;
            }
            set
            {
                _selectedTimeRecord = value;

                if (_selectedTimeRecord != null)
                {
                    ((AgentryClientSDK.IAgentryControlViewModelCollectionDisplay)DataContext).SelectItem(value.Index);
                    System.Diagnostics.Debug.WriteLine("storing time detail selectedID to " + _selectedTimeRecord.ID);
                }
            }
        }

        private ObservableCollection<LaborTimeDetails> records;
        private DataGrid grid;
        private System.Timers.Timer refreshTimer;

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            grid = GetTemplateChild("PART_GridDailyOverviewDetailsList") as DataGrid;
            if (grid != null)
            {
                grid.Loaded += grid_Loaded;
                //grid.SelectionChanged += (obj, e) => Dispatcher.BeginInvoke(DispatcherPriority.Render, new Action(() => grid.UnselectAll()));
                grid.DataContext = this;
            }
          
        }

      
        private void grid_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("Time Recording History Details Grid load triggered");
                
                records = new ObservableCollection<LaborTimeDetails>();
                context = DataContext as AgentryClientSDK.IAgentryControlViewModelCollectionDisplay;

                context.PropertyChanged += dc_PropertyChanged;

                int index = 0;
                foreach (AgentryClientSDK.IAgentryData item in context)
                {
                    var record =
                    new LaborTimeDetails
                    {   
                        WorkDate = item.PropertyValueByName("TimeRecordingDate"),
                        PersonNr = item.PropertyValueByName("PersonNum"),
                        WorkTime = item.PropertyValueByName("WoTimes"),
                        Wonr = item.PropertyValueByName("WoNum"),
                        WoComment = item.PropertyValueByName("WoText"),
                        MiscComment = item.PropertyValueByName("ConfirmationText"),
                        IsLocal = item.PropertyValueAsBool("IsLocal"),
                        ID = item.PropertyValueByName("Key"),
                        Index = index++
                    };
                    records.Add(record);
                }

                cvs = new CollectionViewSource();
                cvs.Source = records;

                grid.ItemsSource = cvs.View;           
                grid.SelectedIndex = 0;

                SelectedTimeRecord = (LaborTimeDetails)grid.SelectedItem;
                grid.UpdateLayout();
                grid.ScrollIntoView(grid.SelectedIndex);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Grid Load Exception : " + ex.Message);
            }
        }

        private void dc_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (refreshTimer == null)
            {
                refreshTimer = new System.Timers.Timer(100);
                refreshTimer.Elapsed += t_Elapsed;
                refreshTimer.Start();
            }
        }
        void t_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                IAgentryControlViewModelCollectionDisplay dc = null;
                this.Dispatcher.Invoke(new Action(() => dc = this.DataContext as IAgentryControlViewModelCollectionDisplay));
                this.Dispatcher.Invoke(new Action(() => { grid_Loaded(null, new RoutedEventArgs()); }));
                refreshTimer.Stop();
                refreshTimer = null;
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Timer Exception : " + ex.Message);
            }
        }

        public ICommand DeleteTimeCommand
        {
            get
            {
                return new CommandHandler(deleteTime, true);
            }
        }

        private void deleteTime(object obj)
        {
            LaborTimeDetails notif = obj as LaborTimeDetails;
            context.ExecuteAgentryAction("ZTimeRecordingClearFromTimeOverview");
        }
    }
}
