﻿using SWB_Controls.BusinessObjects;
using SWB_Controls.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Timers;
using AgentryClientSDK;
using System.Windows.Markup;
using SWB_Controls.Utils;
using System.ComponentModel;
using System.Collections.ObjectModel;
using SWB_Controls.DropdownControls;

namespace SWB_Controls.ListControls
{
    public class TimeRecordingListControl : BaseAgentryCustomControl
    {
        static TimeRecordingListControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TimeRecordingListControl), new FrameworkPropertyMetadata(typeof(TimeRecordingListControl)));
            injectDictionary();
            PureVirtualHandler.RegisterHandler();
        }

        private static void injectDictionary()
        {
            var ResName = "SWB-Controls;component/Resources/Theme.xaml";
            var uri = new Uri(ResName, UriKind.Relative);

            var streamResourceInfo = Application.GetResourceStream(uri);

            using (var resStream = streamResourceInfo.Stream)
            {
                ResourceDictionary myResDic = (ResourceDictionary)XamlReader.Load(resStream);
                Application.Current.Resources.MergedDictionaries.Add(myResDic);
            }
        }

        static Dictionary<string, ListSortDirection> dictSort = new Dictionary<string, ListSortDirection>();
        public CollectionViewSource cvs;
        private LaborTimeDetails _selectedItem;
        public LaborTimeDetails SelectedRecord
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                _selectedItem = value;

                if (_selectedItem != null)
                {
                    ((AgentryClientSDK.IAgentryControlViewModelCollectionDisplay)DataContext).SelectItem(value.Index);
                }
            }
        }

        private ObservableCollection<LaborTimeDetails> records;
        private DataGrid grid;
        private System.Timers.Timer refreshTimer;

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            grid = GetTemplateChild("PART_GridTimeRecordingList") as DataGrid;
            if (grid != null)
            {
                grid.Loaded += grid_Loaded;
                grid.DataContext = this;
            }
          
        }

      
        private void grid_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("Time Recording Grid load triggered");
                
                records = new ObservableCollection<LaborTimeDetails>();
                AgentryClientSDK.IAgentryControlViewModelCollectionDisplay vm = DataContext as AgentryClientSDK.IAgentryControlViewModelCollectionDisplay;

                vm.PropertyChanged += dc_PropertyChanged;

                int index = 0;
                foreach (AgentryClientSDK.IAgentryData item in vm)
                {
                    var record =
                    new LaborTimeDetails
                    {   
                        WorkDate = item.PropertyValueByName("ZDate"),
                        PersonNr = item.PropertyValueByName("ZPersonNum"),
                        WorkTime = item.PropertyValueByName("ZDuration"),
                        MiscComment = item.PropertyValueByName("ZDurationText"),
                        Status = item.PropertyValueByName("ZStatus"),
                        IsAdditionalWorkDone = item.PropertyValueAsBool("ZAdditionalWorkDone"),
                        IsLocal = (item.PropertyValueByName("LaborLocal") != null) ? item.PropertyValueByName("LaborLocal").ToString().ToUpper().Equals("L") : false,
                        Index = index++
                    };
                    records.Add(record);
                }

                cvs = new CollectionViewSource();
                cvs.Source = records;

                grid.ItemsSource = cvs.View;           
                grid.SelectedIndex = 0;

                SelectedRecord = (LaborTimeDetails)grid.SelectedItem;
                if (grid.Items.Count > 0)
                {
                    grid.UpdateLayout();
                    grid.ScrollIntoView(grid.SelectedIndex);
                }
                if (MinutesDropdownControl.context.StringValue != MinutesDropdownControl.txtMinutes.Text)
                {
                    MinutesDropdownControl.context.StringValue = MinutesDropdownControl.txtMinutes.Text;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Grid Load Exception : " + ex.Message);
            }
        }

        private void dc_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (refreshTimer == null)
            {
                refreshTimer = new System.Timers.Timer(100);
                refreshTimer.Elapsed += t_Elapsed;
                refreshTimer.Start();
            }
        }
        void t_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                IAgentryControlViewModelCollectionDisplay dc = null;
                this.Dispatcher.Invoke(new Action(() => dc = this.DataContext as IAgentryControlViewModelCollectionDisplay));
                this.Dispatcher.Invoke(new Action(() => { grid_Loaded(null, new RoutedEventArgs()); }));
                refreshTimer.Stop();
                refreshTimer = null;
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Timer Exception : " + ex.Message);
            }
        }

        public ICommand DeleteTimeRecordCommand
        {
            get
            {
                return new CommandHandler(deleteTime, true);
            }
        }

        private void deleteTime(object obj)
        {
            ((AgentryClientSDK.IAgentryControlViewModelCollectionDisplay)DataContext).ExecuteAgentryAction("ZTimeRecordingClear");
        }
    }
}
