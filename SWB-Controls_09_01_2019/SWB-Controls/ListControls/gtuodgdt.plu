﻿using SWB_Controls.BusinessObjects;
using SWB_Controls.Extensions;
using SWB_Controls.TextBoxControls;
using SWB_Controls.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace SWB_Controls.ListControls
{
    public class CharValueListControl : BaseAgentryCustomControl
    {
        public AgentryClientSDK.IAgentryControlViewModelCollectionDisplay context { get; set; }
        static DataGrid dataGrid { get; set; }
        static Characteristic dataGridSelectedItem { get { return (Characteristic)dataGrid.SelectedItem; } }
        public CollectionViewSource cvs;
        public bool isDirty = false;
        public int currentTextLength = 0;
        public bool hasUnsavedChanged
        {
            set { this.SetExtensionString("HasUnsavedChanges", ((value) ? "true" : "")); }
        }
        public string _filter;
        public string Filter
        {
            get
            {
                return _filter;
            }
            set
            {
                _filter = value;
                cvs.View.Refresh();
            }
        }
        private FunctionLocation _selectedFL;
        public FunctionLocation selectedFL
        {
            get
            {
                return _selectedFL;
            }
            set
            {
                _selectedFL = value;
                cvs.View.Refresh();
            }
        }

        public List<FunctionLocation> FunctionLocations { get; set; }
        public string SelectedCharValue { get; set; }

        static CharValueListControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CharValueListControl), new FrameworkPropertyMetadata(typeof(CharValueListControl)));
        }

        public override void OnApplyTemplate()
        {

            base.OnApplyTemplate();

            var baseGrid = GetTemplateChild("PART_BaseGrid") as Grid;
            if (baseGrid != null)
            {
                baseGrid.DataContext = this;
            }
            DataGrid grid = GetTemplateChild("PART_Grid") as DataGrid;
            if (grid != null)
            {
                grid.Loaded += grid_Loaded;
                grid.CellEditEnding += grid_CellEditEnding;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("de-DE");
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("de-DE");
            }
        }

        private void grid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            dataGrid = GetTemplateChild("PART_Grid") as DataGrid;
            var item = (Characteristic)dataGrid.SelectedItem;
            if (dataGrid.CurrentColumn != null)
            {
                var cell = DataGridHelper.GetCell(dataGrid, dataGrid.SelectedIndex, dataGrid.CurrentColumn.DisplayIndex);
                var dependentCell = (ContentPresenter)cell.Content;
                var allowedDecimal = Convert.ToInt32(item.ZNumDec);
                var hasError = false;
                if (item.IsNegative && item.Value.IndexOf("-") != 0)
                {
                    hasError = true;
                }
                if (item.DataType == "NUM" && allowedDecimal > 0 && !hasError)
                {
                    var txt = item.Value;
                    if (item.Value.IndexOf("-") > -1)
                    {
                        var negativeCount = txt.Count(x => x == '-');
                        if (negativeCount < 2)
                            txt = txt.Substring(1);
                        else
                            hasError = true;
                    }

                    var totalLength = Convert.ToInt32(item.ZNumChar);
                    if (txt.IndexOf(",") > -1)
                    {
                        var separatorCount = txt.Count(x => x == ',');
                        if (separatorCount < 2)
                        {
                            var split = txt.Split(',');
                            hasError = (split[0].Length > (totalLength - allowedDecimal)) || split[1].Length > allowedDecimal;
                        }
                        else
                            hasError = true;
                    }
                    else
                    {
                        hasError = txt.Length > (totalLength - allowedDecimal);
                    }
                }
                if (hasError)
                {
                    var textbox = (TextBox)(DataGridHelper.FindVisualChildren<TextBox>(dependentCell, "PART_Textbox"));
                    if (textbox != null)
                    {
                        textbox.Background = Brushes.Red;
                        textbox.Foreground = Brushes.White;
                    }
                    e.Cancel = true;
                }
            }

        }


        private void grid_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                FunctionLocations = new List<FunctionLocation>();

                context = DataContext as AgentryClientSDK.IAgentryControlViewModelCollectionDisplay;
                var index = 0;
                foreach (AgentryClientSDK.IAgentryData item in context)
                {
                    var classificationCollection = item.CollectionByName("Classifications");
                    var objID = item.PropertyValueByName("ObjectID");
                    var objDesc = item.PropertyValueByName("ZTechnicalObjectDesc");
                    if (classificationCollection != null && classificationCollection.DescendantCount > 0)
                    {
                        for (int i = 0; i < classificationCollection.DescendantCount; i++)
                        {
                            FunctionLocation obj = new FunctionLocation()
                            {
                                ObjectID = objID
                            };
                            var crName = classificationCollection.Descendant(i).PropertyValueByName("Name");
                            var crID = classificationCollection.Descendant(i).PropertyValueByName("ID");
                            var characteristicCollection = classificationCollection.Descendant(i).CollectionByName("Characteristics");
                            obj.ID = crID;
                            obj.Description = objDesc;
                            obj.DescriptionLong = crName;
                            for (int j = 0; j < characteristicCollection.DescendantCount; j++)
                            {
                                Characteristic c = new Characteristic(obj);
                                c.ObjectID = objID;
                                c.ClassificationID = crID;
                                c.CharacteristicUniqueID = characteristicCollection.Descendant(j).PropertyValueByName("ID");
                                c.Name = characteristicCollection.Descendant(j).PropertyValueByName("Description");
                                c.IsReadOnly = characteristicCollection.Descendant(j).PropertyValueAsBool("ZIsReadOnly");
                                c.ZNumChar = characteristicCollection.Descendant(j).PropertyValueByName("ZNumChar");
                                c.ZNumDec = characteristicCollection.Descendant(j).PropertyValueByName("ZNumDec");
                                c.DataType = characteristicCollection.Descendant(j).PropertyValueByName("DataType");
                                c.ItemNumber = Convert.ToInt32(characteristicCollection.Descendant(j).PropertyValueByName("ItemNumber"));
                                c.IsNegative = characteristicCollection.Descendant(j).PropertyValueByName("Sign").ToString().ToLower() == "x";
                                var valueCollection = characteristicCollection.Descendant(j).CollectionByName("CharacteristicValues");
                                var possibleValues = characteristicCollection.Descendant(j).CollectionByName("ZPossibleCharacteristicValues");
                                if (valueCollection != null && valueCollection.DescendantCount > 0)
                                {
                                    if (valueCollection.DescendantCount == 1)
                                    {
                                        c.Value = valueCollection.Descendant(0).PropertyValueByName("Value");
                                        c.CharacteristicValueID = valueCollection.Descendant(0).PropertyValueByName("ID");
                                    }
                                    else
                                    {
                                        for (int k = 0; k < valueCollection.DescendantCount; k++)
                                        {
                                            var isLocalValue = valueCollection.Descendant(k).PropertyValueAsBool("ZIsLocalValue");
                                            if (isLocalValue)
                                            {
                                                c.Value = valueCollection.Descendant(k).PropertyValueByName("Value");
                                                c.CharacteristicValueID = valueCollection.Descendant(k).PropertyValueByName("ID");
                                                break;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    c.Value = string.Empty;
                                    c.CharacteristicValueID = string.Empty;
                                }
                                if (possibleValues != null && possibleValues.DescendantCount > 0)
                                {
                                    for (int l = 0; l < possibleValues.DescendantCount; l++)
                                    {
                                        var id = possibleValues.Descendant(l).PropertyValueByName("ID");
                                        var value = possibleValues.Descendant(l).PropertyValueByName("Value");
                                        if (value == c.Value)
                                        {
                                            c.PossibleValues.Insert(0, new CharacteristicValues { ID = id, Value = value });
                                        }
                                        else
                                        {
                                            c.PossibleValues.Add(new CharacteristicValues { ID = id, Value = value });
                                        }
                                    }
                                }
                                c.ID = index++;
                                if (c.PossibleValues.Count > 0)
                                {
                                    c.SelectedCharvalue = c.PossibleValues.First();
                                }
                                c.isModified = false;
                                if (c.DataType == "TIME")
                                {
                                    c.Value = (c.Value.Length > 0) ? String.Format("{0}:{1}", c.Value.Substring(0, 2), c.Value.Substring(2, 2)) : "00:00";
                                }
                                if (c.DataType == "DATE")
                                {
                                    if (c.Value.Length > 0 && c.Value.IndexOf(".") < 0)
                                    {
                                        c.Value = DateTime.ParseExact(c.Value.Substring(0, 8), "yyyyMMdd", CultureInfo.InvariantCulture).ToShortDateString();
                                    }
                                }
                                obj.characteristics.Add(c);
                            }

                            if (obj.characteristics.Any())
                            {
                                FunctionLocations.Add(obj);
                            }
                        }
                    }
                }
                dataGrid = GetTemplateChild("PART_Grid") as DataGrid;
                cvs = new CollectionViewSource();
                cvs.Filter += cvs_Filter;
                cvs.Source = FunctionLocations.SelectMany(c => c.characteristics).OrderBy(x => x.ItemNumber).ToList();

                cvs.GroupDescriptions.Add(new PropertyGroupDescription("FunctionLoc.FullDescription"));

                ListCollectionView lcw = cvs.View as ListCollectionView;
                dataGrid.ItemsSource = cvs.View;

                dataGrid.Items.MoveCurrentToFirst();
                dataGrid.UpdateLayout();
                dataGrid.ScrollIntoView(dataGrid.SelectedItem);

                hasUnsavedChanged = false;
                var btnResetFilter = GetTemplateChild("PART_btnResetFilter") as Button;
                btnResetFilter.Click += btnResetFilter_Click;
                firePropertyChanged("FunctionLocations");
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex);
            }
        }
        private void EnableSaveButton()
        {
            var btnSave = GetTemplateChild("PART_btnSave") as Button;
            btnSave.IsEnabled = true;
        }
        private void DisableSaveButton()
        {
            var btnSave = GetTemplateChild("PART_btnSave") as Button;
            btnSave.IsEnabled = false;
        }
        private void txt_TextChanged(object sender, TextChangedEventArgs e)
        {
            dataGridSelectedItem.isModified = true;
            hasUnsavedChanged = true;
            EnableSaveButton();
        }

        private void txt_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox txt = (TextBox)sender;
            dataGridSelectedItem.Value = txt.Text;
            dataGrid.CommitEdit();
        }
        private void txt_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var item = dataGridSelectedItem;
            var allowedDecimal = Convert.ToInt32(item.ZNumDec);
            var dataType = item.DataType;
            var txt = (sender as TextBox).Text;
            if (allowedDecimal > 0)
            {
                if (dataType.ToUpper() == "NUM")
                {
                    if (item.IsNegative && e.Text.Equals("-"))
                    {
                        e.Handled = false;
                    }
                    else
                    {
                        if (!StringHelper.IsNumericCommaTextAllowed(e.Text))
                        {
                            txt = txt + e.Text;
                            if (txt.IndexOf(",") > 0)
                            {
                                var separatorCount = txt.Count(x => x == ',');
                                e.Handled = separatorCount > 1;
                            }
                        }
                        else
                        {
                            e.Handled = true;
                        }
                    }
                }

            }
            else
            {
                if (dataType.ToUpper() == "NUM")
                {
                    if (item.IsNegative && e.Text.Equals("-"))
                    {
                        e.Handled = false;
                    }
                    else
                    {
                        e.Handled = StringHelper.IsNumericTextAllowed(e.Text);
                    }
                }
                else
                {
                    e.Handled = false;
                }
            }

        }
        private void cmbPossibleValues_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dataGridSelectedItem.SelectedCharvalue = (CharacteristicValues)((sender as ComboBox).SelectedItem);
            hasUnsavedChanged = true;
            EnableSaveButton();
            dataGrid.CommitEdit();
        }
        private void date_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            dataGridSelectedItem.Value = (sender as DatePicker).SelectedDate.Value.ToString("dd.MM.yyy");
            dataGridSelectedItem.isModified = true;
            hasUnsavedChanged = true;
            EnableSaveButton();
            dataGrid.CommitEdit();
        }
        private void masktext_LostFocus(object sender, RoutedEventArgs e)
        {
            var maskedTimeTextBox = (sender as TextBox);
            dataGridSelectedItem.Value = maskedTimeTextBox.Text.Replace("_", "");
            dataGridSelectedItem.isModified = true;
            hasUnsavedChanged = true;
            EnableSaveButton();
            dataGrid.CommitEdit();
        }
        private void masktext_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var isNumeric = StringHelper.IsNumericTextAllowed(e.Text);
            if (!isNumeric)
            {
                var txt = (sender as TextBox).Text.Replace("_", "").Replace(":", "") + e.Text;

                if (txt.Length == 3)
                {
                    e.Handled = Convert.ToInt32(e.Text) > 5;
                }
                else if (txt.Length == 2)
                {
                    if (txt[0] == '2')
                    {
                        e.Handled = Convert.ToInt32(e.Text) > 3;
                    }
                }

            }
            else
            {
                e.Handled = true;
            }
        }

        private void btnResetFilter_Click(object sender, RoutedEventArgs e)
        {
            selectedFL = null;
            this.firePropertyChanged("selectedFL");
        }
        void cvs_Filter(object sender, FilterEventArgs e)
        {
            Characteristic t = e.Item as Characteristic;
            if (t != null)
            // If filter is turned on, filter completed items.
            {
                if (selectedFL == null) { e.Accepted = true; return; }
                if (t.FunctionLoc == selectedFL)
                    e.Accepted = true;
                else
                    e.Accepted = false;
            }
        }
        public ICommand SaveCommand
        {
            get
            {
                return new CommandHandler(saveCharValue, true);
            }
        }
        public ICommand EditCommand
        {
            get
            {
                return new CommandHandler(editCharValue, true);
            }
        }

        private void editCharValue(object obj)
        {
            var item = dataGridSelectedItem;
            dataGrid.UpdateLayout();
            dataGrid.ScrollIntoView(dataGrid.SelectedItem);
            dataGrid.BeginEdit();
            var cell = DataGridHelper.GetCell(dataGrid, dataGrid.SelectedIndex, dataGrid.CurrentColumn.DisplayIndex);
            var dependentCell = (ContentPresenter)cell.Content;
            switch (item.DataType)
            {
                case "TIME":
                    var masktext = (MaskedTextBox)(DataGridHelper.FindVisualChildren<MaskedTextBox>(dependentCell, "PART_maskedTimeTextBox"));
                    if (masktext != null)
                    {
                        masktext.LostFocus -= masktext_LostFocus;
                        masktext.LostFocus += masktext_LostFocus;

                        masktext.PreviewTextInput -= masktext_PreviewTextInput;
                        masktext.PreviewTextInput += masktext_PreviewTextInput;

                        masktext.IsEnabled = true;
                    }
                    break;
                case "DATE":
                    var date = (DatePicker)(DataGridHelper.FindVisualChildren<DatePicker>(dependentCell, "PART_Date"));
                    if (date != null)
                    {
                        date.SelectedDateChanged -= date_SelectedDateChanged;
                        date.SelectedDateChanged += date_SelectedDateChanged;

                        date.IsEnabled = true;
                    }
                    break;
                case "CHAR":
                case "NUM":
                    if (item.PossibleValues.Count() > 0)
                    {
                        var combo = (ComboBox)(DataGridHelper.FindVisualChildren<ComboBox>(dependentCell, "PART_cmbPossibleValues"));
                        if (combo != null)
                        {
                            combo.SelectionChanged -= cmbPossibleValues_SelectionChanged;
                            combo.SelectionChanged += cmbPossibleValues_SelectionChanged;
                            combo.IsEnabled = true;
                        }
                    }
                    else
                    {
                        var txt = (TextBox)(DataGridHelper.FindVisualChildren<TextBox>(dependentCell, "PART_Textbox"));
                        if (txt != null)
                        {
                            txt.TextChanged -= txt_TextChanged;
                            txt.TextChanged += txt_TextChanged;

                            txt.LostFocus -= txt_LostFocus;
                            txt.LostFocus += txt_LostFocus;

                            txt.PreviewTextInput -= txt_PreviewTextInput;
                            txt.PreviewTextInput += txt_PreviewTextInput;

                            var allowedDecimal = Convert.ToInt32(item.ZNumDec);
                            var dataType = item.DataType;
                            if (allowedDecimal > 0)
                            {
                                txt.MaxLength += 1;
                            }
                            if (item.IsNegative)
                            {
                                txt.MaxLength += 1;
                            }
                            txt.IsEnabled = true;
                        }
                    }
                    break;

            }

        }

        private void saveCharValue(object target)
        {
            dataGrid.UpdateLayout();
            dataGrid.ScrollIntoView(dataGrid.SelectedItem);
            foreach (var txt in DataGridHelper.FindVisualChild<TextBox>(dataGrid, "PART_Textbox"))
            {
                if (txt.Background == Brushes.Red)
                {
                    isDirty = true;
                }
            } 
            
            if (!isDirty)
            {
                var CharValueToSave = FunctionLocations.SelectMany(c => c.characteristics).Where(d => d.isModified);
                foreach (var characteristic in CharValueToSave)
                {
                    this.SetExtensionString("ObjectID", characteristic.ObjectID);
                    this.SetExtensionString("ClassificationID ", characteristic.ClassificationID);
                    this.SetExtensionString("CharacteristicUniqueID", characteristic.CharacteristicUniqueID);
                    var value = characteristic.Value;
                    if (value.Length > 0)
                    {
                        if (Convert.ToInt32(characteristic.ZNumDec) > 0)
                        {
                            value = value.Replace(",", ".");
                        }
                        else if (characteristic.DataType == "TIME")
                        {
                            value = value.Replace(":", "") + "00";
                        }
                    }
                    this.SetExtensionString("Value", value);
                    context.ExecuteAgentryAction("ZCharacteristicValueAdd");
                }
                hasUnsavedChanged = false;
                DisableSaveButton();
            }
            else
            {
                MessageBox.Show("Bitte beheben Sie die Fehler, um fortzufahren");
            }
        }

    }
}
