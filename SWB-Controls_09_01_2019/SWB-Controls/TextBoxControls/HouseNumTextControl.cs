﻿using SWB_Controls.ButtonControls;
using SWB_Controls.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SWB_Controls.TextBoxControls
{
    public class HouseNumTextControl : BaseAgentryCustomControl
    {
         public static AgentryClientSDK.IAgentryControlViewModelStringDisplay context { get; set; }
         public static TextBox txtControl { get; set; }
         public static string HouseNumTextControlValue { get; set; }

         static HouseNumTextControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(HouseNumTextControl), new FrameworkPropertyMetadata(typeof(HouseNumTextControl)));
            PureVirtualHandler.RegisterHandler();
        }
        public void SetAgentryValue()
        {
            context = DataContext as AgentryClientSDK.IAgentryControlViewModelStringDisplay;
            txtControl = GetTemplateChild("PART_Textbox") as TextBox;
            txtControl.Text = context.GetAgentryString("ZHouseNum");
            context.StringValue = txtControl.Text;
            HouseNumTextControlValue = txtControl.Text;
            txtControl.LostFocus += textbox_LostFocus;
        }

        private void textbox_LostFocus(object sender, RoutedEventArgs e)
        {
            var textbox = sender as TextBox;
            context.StringValue = textbox.Text;
            if (AddressButtonControl.IsAddressSelected != null && AddressButtonControl.IsAddressSelected == false)
                HouseNumTextControlValue = textbox.Text;
        }
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            SetAgentryValue();
        }
    }
}
