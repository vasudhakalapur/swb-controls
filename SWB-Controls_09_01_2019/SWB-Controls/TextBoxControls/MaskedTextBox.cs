﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace SWB_Controls.TextBoxControls
{
    public class MaskedTextBox : TextBox
    {
        private System.ComponentModel.MaskedTextProvider _mprovider = null;

        public string Mask
        {
            get { if (_mprovider != null) return _mprovider.Mask; else return ""; }
            set 
            { 
                _mprovider = new System.ComponentModel.MaskedTextProvider(value);
                this.Text = _mprovider.ToDisplayString();
            }
        }

        private bool PreviousInsertState = false;
        private bool _InsertIsON = false;
        private bool _stayInFocusUntilValid = true;

        public bool StayInFocusUntilValid
        {
            get { return _stayInFocusUntilValid; }
            set { _stayInFocusUntilValid = true; }
        }

        private bool _newTextIsOk = false;
        public bool NewTextIsOk
        {
            get { return _newTextIsOk; }
            set { _newTextIsOk = value; }
        }

        private bool _ignoreSpace = true;
        public bool IgnoreSpace
        {
            get { return _ignoreSpace; }
            set { _ignoreSpace = value; }
        }

        protected override void OnPreviewKeyDown(System.Windows.Input.KeyEventArgs e)
        {
            if (this.SelectionLength > 1)
            {
                this.SelectionLength = 0;
                e.Handled = true;
            }
            if (e.Key == Key.Insert || e.Key == Key.Delete || (e.Key == Key.Space && _ignoreSpace))
            {
                e.Handled = true;
            }
            else if (e.Key == Key.Back)
            {
                if (this.CaretIndex > 0)
                {
                    if (this.Text[this.CaretIndex - 1].Equals(':'))
                    {
                        e.Handled = true;
                        this.CaretIndex = this.CaretIndex - 1;
                    }
                    else
                    {
                        switch (this.CaretIndex - 1)
                        {
                            case 0 :
                                this.Text = "__:__";
                                break;
                            case 1:
                                this.Text = this.Text.Substring(0,1) + "_:__";
                                break;
                            case 3:
                                this.Text = this.Text.Substring(0,3) + "__";
                                break;
                            case 4:
                                this.Text = this.Text.Substring(0, 4) + "_";
                                break;
                        }
                    }
                }
            }
            base.OnPreviewKeyDown(e);
        }

        protected override void OnPreviewTextInput(TextCompositionEventArgs e)
        {
            System.ComponentModel.MaskedTextResultHint hint;
            int TestPosition;
            if (e.Text.Length == 1)
                this._newTextIsOk = _mprovider.VerifyChar(e.Text[0], this.CaretIndex, out hint);
            else
                this._newTextIsOk = _mprovider.VerifyString(e.Text, out TestPosition, out hint);
            base.OnPreviewTextInput(e);
        }

        protected override void OnTextInput(TextCompositionEventArgs e)
        {
            string previousText = this.Text;
            if (NewTextIsOk)
            {
                base.OnTextInput(e);
                if (_mprovider.VerifyString(this.Text) == false) this.Text = previousText;
                while (!_mprovider.IsEditPosition(this.CaretIndex) && _mprovider.Length > this.CaretIndex) this.CaretIndex++;
            }
            else
                e.Handled = true;
        }

        protected override void OnGotFocus(System.Windows.RoutedEventArgs e)
        {
            base.OnGotFocus(e);
            if (!_InsertIsON)
            {
                PressKey(Key.Insert);
                _InsertIsON = true;
            }
        }

        private void PressKey(Key key)
        {
            var eInsertBack = new KeyEventArgs(Keyboard.PrimaryDevice, Keyboard.PrimaryDevice.ActiveSource, 0, key);
            eInsertBack.RoutedEvent = KeyDownEvent;
            InputManager.Current.ProcessInput(eInsertBack);
        }

        protected override void OnPreviewLostKeyboardFocus(KeyboardFocusChangedEventArgs e)
        {
            if (StayInFocusUntilValid)
            {
                _mprovider.Clear();
                _mprovider.Add(this.Text);
                if (!_mprovider.MaskFull) e.Handled = true;
            }
            base.OnPreviewLostKeyboardFocus(e);
        }

        protected override void OnLostFocus(System.Windows.RoutedEventArgs e)
        {
            base.OnLostFocus(e);
            if (PreviousInsertState != System.Windows.Input.Keyboard.PrimaryDevice.IsKeyToggled(System.Windows.Input.Key.Insert))
                PressKey(Key.Insert);
        }
    }
}
