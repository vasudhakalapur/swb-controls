﻿using SWB_Controls.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SWB_Controls.TextBoxControls
{
    public class PersNumTextControl : BaseAgentryCustomControl
    {
        public AgentryClientSDK.IAgentryControlViewModelStringDisplay context { get; set; }
        private static AutoCompleteTextBox txtAuto { get; set; }
        private bool isNewSettingAdded;

        static PersNumTextControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PersNumTextControl), new FrameworkPropertyMetadata(typeof(PersNumTextControl)));
            PureVirtualHandler.RegisterHandler();
        }
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            SetAgentryValue();
        }
        public void SetAgentryValue()
        {
            context = DataContext as AgentryClientSDK.IAgentryControlViewModelStringDisplay;
            txtAuto = GetTemplateChild("txtAuto") as AutoCompleteTextBox;
            txtAuto.textBox.LostFocus += textBox_LostFocus;
            txtAuto.textBox.TextChanged += textBox_TextChanged;
            var contextPersNum = context.GetAgentryString("ZCurrentPersonalNummer");

            SetConfigurationValue(contextPersNum);
            CreatePersNumList();
            txtAuto.Text = contextPersNum;
            context.StringValue = txtAuto.Text;
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {           
            context.StringValue = txtAuto.textBox.Text;
        }

        void textBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!txtAuto.insertText && !txtAuto.comboBox.IsFocused)
            {
                SetConfigurationValue(txtAuto.textBox.Text);
                CreatePersNumList();
            }
        }      

        private void CreatePersNumList()
        {
            txtAuto.RemoveAllItems();
            foreach (var str in SWB_Controls.Properties.Settings.Default.PersonnelNumberHistory.Split(';').Distinct())
            {                
                txtAuto.AddItem(new AutoCompleteEntry(str.Trim(), str.Trim()));
            }
        }

        private void SetConfigurationValue(string val)
        {
            if (!SWB_Controls.Properties.Settings.Default.PersonnelNumberHistory.ToLower().Equals(val.ToLower()))
            {
                if (SWB_Controls.Properties.Settings.Default.PersonnelNumberHistory.Length > 0)
                {
                    SWB_Controls.Properties.Settings.Default.PersonnelNumberHistory = SWB_Controls.Properties.Settings.Default.PersonnelNumberHistory + ";" + val;
                }
                else
                {
                    SWB_Controls.Properties.Settings.Default.PersonnelNumberHistory = val;
                }
                SWB_Controls.Properties.Settings.Default.Save();
                isNewSettingAdded = true;
            }
            else
            {
                isNewSettingAdded = false;
            }
        }
    }
}
