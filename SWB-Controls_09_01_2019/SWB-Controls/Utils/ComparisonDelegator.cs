﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SWB_Controls.Utils
{
    class ComparisonDelegator<T> : IComparer<T>, IComparer
    {
        private Comparison<T> comparison;

        public ComparisonDelegator(Comparison<T> comparison) { this.comparison = comparison; }

        public int Compare(T x, T y) { return comparison(x, y); }

        public int Compare(object x, object y) { return comparison((T)x, (T)y); }
    }
}
