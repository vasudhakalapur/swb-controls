﻿using SWB_Controls.BusinessObjects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace SWB_Controls.Utils
{
    public class PagingCollectionView : CollectionView
    {
        private IList _innerList;
        private readonly IList _innerOriginalList;
        private readonly int _itemsPerPage;

        private int _currentPage = 1;

        public PagingCollectionView(IList innerList, int itemsPerPage)
            : base(innerList)
        {
            this._innerList = innerList;
            this._innerOriginalList = innerList;
            this._itemsPerPage = itemsPerPage;
        }

        public override int Count
        {
            get
            {
                if (this._currentPage < this.PageCount)
                    return this._itemsPerPage;

                var itemsLeft = this._innerList.Count % this._itemsPerPage;
                return (itemsLeft == 0) ? this._itemsPerPage : itemsLeft;
            }
        }
        //Only for WorkorderChecklist
        private string mFilterText;
        public string FilterText
        {
            get
            {
                return mFilterText;
            }
            set
            {

                mFilterText = value;
                this.OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs("FilterText"));
                this._innerList = (mFilterText.Length > 0) ? ((IList<WorkOrderCheck>)this._innerOriginalList).Where(t => t.Description.ToLower().Contains(mFilterText) ||
                                                                t.Start.ToString().ToLower().Contains(mFilterText) || t.Wonr.ToLower().Contains(mFilterText) ||
                                                                t.Techobj.ToLower().Contains(mFilterText)).ToList() : this._innerOriginalList;
                this.Refresh();
            }
        }

        public Visibility ShowPrevButton
        {
            get { return (CurrentPage == 1) ? Visibility.Collapsed : Visibility.Visible; }
        }
        public Visibility ShowNextButton
        {
            get { return (CurrentPage == PageCount) ? Visibility.Collapsed : Visibility.Visible; }
        }
        public int CurrentPage
        {
            get { return this._currentPage; }
            set
            {
                this._currentPage = value;
                this.OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs("CurrentPage"));
            }
        }

        public int ItemsPerPage { get { return this._itemsPerPage; } }

        public int PageCount
        {
            get { return (this._innerList.Count + this._itemsPerPage - 1) / this._itemsPerPage; }
        }

        private int EndIndex
        {
            get
            {
                var end = this._currentPage * this._itemsPerPage - 1;
                return (end > this._innerList.Count) ? this._innerList.Count : end;
            }
        }

        private int StartIndex
        {
            get { return (this._currentPage - 1) * this._itemsPerPage; }
        }

        public override object GetItemAt(int index)
        {
            if (index >= 0 && this._innerList.Count > 0)
            {
                var offset = index % (this._itemsPerPage);
                return this._innerList[this.StartIndex + offset];
            }
            return new object();
        }

        public void MoveToNextPage()
        {
            if (this._currentPage < this.PageCount)
            {
                this.CurrentPage += 1;
            }
            this.Refresh();
        }
        public void MoveToPreviousPage()
        {
            if (this._currentPage > 1)
            {
                this.CurrentPage -= 1;
            }
            this.Refresh();
        }
    }
}
