﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SWB_Controls.Utils
{
    public class PureVirtualHandler
    {
        private const string DllName =  "msvcr100.dll";


        public static bool init = false;
        public static  PureCallHandler hand;
        public static void RegisterHandler()
        {
            if (!init)
            {
                hand = CallHandler;
                PureVirtualHandler._set_purecall_handler(hand);
            init = true;
            }
        }

        public delegate void PureCallHandler();

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        private static extern PureCallHandler _set_purecall_handler(PureCallHandler handler);
       

        private static void CallHandler()
        {
          
        }

        




    }
}
